# A shipper owns a barge of carrying capacity 1500 m3. He has seven regular 
# customers who load and unload practically at the same places. For each client, 
# the available number of lots, the lot size, the price per lot and transportation 
# cost per m3 are given. The problem is to determine the number of lots transported 
# for each client so as to maximize the profit.

from pyomo.environ import *

model = AbstractModel("Barge Loading")

#Sets and parameters

#set of clients
model.clients = Set()

#number of available lots per client
model.avail_lots = Param(model.clients, within = NonNegativeReals)

#lot size per client
model.lot_size = Param(model.clients, within = NonNegativeReals)

#price per lot for each client
model.price_lot = Param(model.clients, within = NonNegativeReals)

#transportation cost per m3 for each client
model.transp_cost = Param(model.clients, within = NonNegativeReals)

#capacity of the barge
model.capacity = Param(within = NonNegativeReals)

#profit per lot for each client
model.profit = Param(model.clients, within = NonNegativeReals)

#profit for each client
def profit_client(model, c):
    return model.price_lot[c] - model.transp_cost[c]*model.lot_size[c]
model.profit_client = Param(model.clients, initialize = profit_client)

#Variables

#variables representing the number of lots transported for each client
model.lots_transp = Var(model.clients, within = NonNegativeIntegers)

#Objective

#maximizing the total profit
def max_profit(model):
    return sum(model.profit_client[c]*model.lots_transp[c] for c in model.clients)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#capacity of the barge satisfied
def cap_satisfied(model):
    return sum(model.lot_size[c]*model.lots_transp[c] for c in model.clients) <= model.capacity
model.cap_satisfied = Constraint(rule = cap_satisfied)

#every amount transported per client should be at as the availability for each client
def avail_satisfied(model, c):
    return model.lots_transp[c] <= model.avail_lots[c]
model.avail_satisfied = Constraint(model.clients, rule = avail_satisfied)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("barge_loading.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.clients:
    if value(instance.lots_transp[i]) > 0:
        print(f'The number of lots transported for client {i} is {value(instance.lots_transp[i])}')
print(f'The maximum total profit is {value(instance.max_profit)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.clients:
        if value(instance.lots_transp[i]) > 0:
            output.write(f'The number of lots transported for client {i} is {value(instance.lots_transp[i])}\n\n')
    output.write(f'The maximum total profit is {value(instance.max_profit)}')
    output.close()
