# A number of power stations are needed to satisfy electricity demands
# over a day. The day (24-hour) is divided into $5$ different time
# periods. Three type of generating unit are available at the following
# quantities: 12 of type 1, 10 of type 2 and 5 of type 3. A
# minimum and maximum work level is given for each generator. Also, a
# hourly cost of using each generator at minimum level is provided. In
# addition, there is an extra hourly cost for each mw at which a unit is
# used above the minimum level. There is a cost associated to starting a
# generator. If there is an increase by 15% in demand, there should be
# sufficient generators working to make it possible.

# In addition to the thermal generators, a reservoir powers two hydro
# generators. When a hydro generator is running, it operates at a fixed
# level and the depth of the reservoir decreases. A fixed start-up cost
# and a running cost per hour for each hydro generator are given. The
# reservoir level must be between 15 and 20m and at midnight the level
# should be 16m. It is required 3000MWh electricity for each meter
# increased in the reservoir level.

from pyomo.environ import *

model = AbstractModel("Hydro Power")

#Sets and parameters

#set of time periods
model.times = Set()

#amount of hours in each period of time
model.hours_period = Param(model.times, within = NonNegativeReals)

#electricity demands for each time period
model.elec_demand = Param(model.times, within = NonNegativeReals)

#set of type of generators
model.gen_types = Set()

#set of hydro types
model.hydro_types = Set()

#minimum level each generator type works
model.min_level = Param(model.gen_types, within = NonNegativeReals)

#maximum level each generator type works
model.max_level = Param(model.gen_types, within = NonNegativeReals)

#cost per hour working at minimum
model.cost_at_min = Param(model.gen_types, within = NonNegativeReals)

#cost per hour per megawatt above minimum
model.cost_above_min = Param(model.gen_types, within = NonNegativeReals)

#cost for starting a generator of each type
model.start_cost = Param(model.gen_types, within = NonNegativeReals)

#cost per hour per megawatt above minimum level multiplied by the number of hours in the period
def c(model, i, j):
    return model.cost_above_min[i]*model.hours_period[j]
model.c = Param(model.gen_types, model.times, initialize = c)

#costs per hour operating at minimum level multiplied by the number of hours in the period
def total_cost_at_min_per_period(model, i, j):
    return model.cost_at_min[i]*model.hours_period[j]
model.total_cost_at_min_per_period = Param(model.gen_types, model.times, initialize = total_cost_at_min_per_period)

#cost per hour of hydro i
model.K = Param(model.hydro_types, within = NonNegativeReals)

#start-up cost of hydro i
model.G = Param(model.hydro_types, within = NonNegativeReals)

#operating level of hydro i
model.L = Param(model.hydro_types, within = NonNegativeReals)

#height reduction per hour caused by hydro j
model.R = Param(model.hydro_types, within = NonNegativeReals)

#Variables

#number of generators of a type working in a period
def ub(model, i, j):
    if i == 1:
        return RangeSet(0,12)
    elif i == 2:
        return RangeSet(0,10)
    else:
        return RangeSet(0,5)
model.n = Var(model.gen_types, model.times, within = ub)

#number of generators of a type started up in a period
model.s = Var(model.gen_types, model.times, within = ub)

#total output rate from generators of a type in a period
model.x = Var(model.gen_types, model.times, within = NonNegativeReals)

#hydro of type i working in period j
model.h = Var(model.hydro_types, model.times, within = Binary)

#hydro of type i started in period j
model.t = Var(model.hydro_types, model.times, within = Binary)

#height of reservoir at the beginning of period j
model.l = Var(model.times, within = NonNegativeReals)

#number of megawatts pumping in period j
model.p = Var(model.times, within = NonNegativeReals)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(sum(model.c[i,j]*(model.x[i,j] - model.min_level[i]*model.n[i,j]) for i in model.gen_types) for j in model.times) + sum(sum(model.total_cost_at_min_per_period[i,j]*model.n[i,j] for i in model.gen_types) for j in model.times) + sum(sum(model.start_cost[i]*model.s[i,j] for i in model.gen_types) for j in model.times) + sum(sum(model.K[i]*model.hours_period[j]*model.h[i,j] for i in model.hydro_types) for j in model.times) + sum(sum(model.G[i]*model.t[i,j] for i in model.hydro_types) for j in model.times)
model.min_cost = Objective(rule = min_cost)

#Constraints

#Demand must be met in each period
def demand_sat(model, j):
    return sum(model.x[i,j] for i in model.gen_types) + sum(model.L[i]*model.h[i,j] for i in model.hydro_types) - model.p[j] >= model.elec_demand[j]
model.demand_sat = Constraint(model.times, rule = demand_sat)

#Constraints satisfying lower bound on the output 
def lb_output(model, i, j):
    return model.x[i,j] >= model.min_level[i]*model.n[i,j]
model.lb_output = Constraint(model.gen_types, model.times, rule = lb_output)

#Constraints satisfying upper bound on the output 
def ub_output(model, i, j):
    return model.x[i,j] <= model.max_level[i]*model.n[i,j]
model.ub_output = Constraint(model.gen_types, model.times, rule = ub_output)

#The extra guaranteed load requirement(15%) must be able to be met without starting up any more generators
def extra_load_sat(model, j):
    return sum(model.max_level[i]*model.n[i,j] for i in model.gen_types) >= 1.15*model.elec_demand[j] - sum(model.L[i] for i in model.hydro_types)
model.extra_load_sat = Constraint(model.times, rule = extra_load_sat)

##The number of generators started in period j must equal the increase in number
def numb_gens_started(model, i, j):
    if j == 1:
        return model.s[i,1] >= model.n[i,1] - model.n[i,5]
    else:
        return model.s[i,j] >= model.n[i,j] - model.n[i,j-1]
model.numb_gens_started = Constraint(model.gen_types, model.times, rule = numb_gens_started)

#The number of hydros started in period j must equal the increase in number 
def no_hydros_started(model, i, j):
    if j == 1:
        return model.t[i,1] >= model.h[i,1] - model.h[i,5]
    else:
        return model.t[i,j] >= model.h[i,j] - model.h[i,j-1]
model.no_hydros_started = Constraint(model.hydro_types, model.times, rule = no_hydros_started)

#Reservoir level changes resulting from pumping and generation
def reservoir_level(model, j):
    if j == 1:
        return model.l[1] - model.l[5] - (model.hours_period[1]*model.p[1]) / 3000 + sum(model.hours_period[1]*model.R[i]*model.h[i,1] for i in model.hydro_types) == 0
    else:
        return model.l[j] - model.l[j-1] - (model.hours_period[j]*model.p[j]) / 3000 + sum(model.hours_period[j]*model.R[i]*model.h[i,j] for i in model.hydro_types) == 0
model.reservoir_level = Constraint(model.times, rule = reservoir_level)

#upper bound on the operating level
def ub_operating_level(model, j):
    return model.l[j] <= 20
model.ub_operating_level = Constraint(model.times, rule = ub_operating_level)

#lower bound on the operating level
def lb_operating_level(model, j):
    return 15 <= model.l[j]
model.lb_operating_level = Constraint(model.times, rule = lb_operating_level)

#Reservoir level at midnight each night
def res_level_midnight(model, j):
    if j == 1:
        return model.l[1] == 16
    else:
        return Constraint.Skip
model.res_level_midnight = Constraint(model.times, rule = res_level_midnight)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("hydro_power.dat")
results = solver.solve(instance)

for i in instance.gen_types:
    for j in instance.times:
        if value(instance.n[i,j]) > 0:
            print("Number of generating units of type %s working in period %s is: %d " %(i,j,value(instance.n[i,j])))
for i in instance.gen_types:
    for j in instance.times:
        if value(instance.s[i,j]) > 0:
            print("Number of generators of type %s started up in period %s is: %d " %(i,j, value(instance.s[i,j])))
for i in instance.gen_types:
    for j in instance.times:
        if value(instance.x[i,j]) > 0:
            print("Total output rate from generators of type %s in period %s is: %f " %(i,j, value(instance.x[i,j])))
for i in instance.hydro_types:
    for j in instance.times:
        if value(instance.h[i,j]) > 0:
            print("Hydro type %s is working in period %s " %(i,j))
for i in instance.hydro_types:
    for j in instance.times:
        if value(instance.t[i,j]) > 0:
            print("Hydro type %s started in period %s " %(i,j))
for j in instance.times:
    if value(instance.l[j]) > 0:
        print("Height of reservoir at beginning of period %s " %j)
for j in instance.times:
    if value(instance.p[j]) > 0:
        print("Number of megawatts of pumping in period %s " %j)
print("The minimum total cost is: %f" %value(instance.min_cost))

        
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.n[i,j]) > 0:
                output.write("Number of generating units of type %s working in period %s is: %d \n\n" %(i,j,value(instance.n[i,j])))
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.s[i,j]) > 0:
                output.write("Number of generators of type %s started up in period %s is: %d \n\n" %(i,j, value(instance.s[i,j])))
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.x[i,j]) > 0:
                output.write("Total output rate from generators of type %s in period %s is: %f \n\n" %(i,j, value(instance.x[i,j])))
    for i in instance.hydro_types:
        for j in instance.times:
            if value(instance.h[i,j]) > 0:
                output.write("Hydro type %s is working in period %s \n\n" %(i,j))
    for i in instance.hydro_types:
        for j in instance.times:
            if value(instance.t[i,j]) > 0:
                output.write("Hydro type %s started in period %s \n\n" %(i,j))
    for j in instance.times:
        if value(instance.l[j]) > 0:
            output.write("Height of reservoir at beginning of period %s \n\n" %j)
    for j in instance.times:
        if value(instance.p[j]) > 0:
            output.write("Number of megawatts of pumping in period %s \n\n" %j)
    output.write("The minimum total cost is: %f" %value(instance.min_cost))
    output.close()
