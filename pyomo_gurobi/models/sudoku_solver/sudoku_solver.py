# Sudoku puzzle is a known problem which can be modelled in several ways
# including Mixed Integer Linear Programming. The latter method is used
# below to formulate the sudoku problem and then solved by
# OptiSolver. This problem is a feasibility problem since we are
# interested in a solution only. Sudoku is a 9x9 square composed by
# 9 3x3 subsquares. Each subsquare, row, column must contain each
# number from 1 to 9 exactly once. The original square is partially
# filled.

from pyomo.environ import *

model = ConcreteModel("Sudoku Solver")

#starting board
model.board = [(1,1,5), (1,2,3), (1,5,7), (2,1,6), (2,4,1), (2,5,9), (2,6,5), (3,2,9), (3,3,8), (3,8,6), (4,1,8), (4,5,6), (4,9,3), (5,1,4), (5,4,8), (5,6,3), (5,9,1), (6,1,7), (6,5,2), (6,9,6), (7,2,6), (7,7,2), (7,8,8), (8,4,4), (8,5,1), (8,6,9), (8,9,5), (9,5,8)]


#Sets and parameters

#set of rows
model.rows = RangeSet(1,9)

#set of columns
model.columns = RangeSet(1,9)

#set of values
model.value = RangeSet(1,9)

#set of subsquares
model.subsquares = RangeSet(1,9)

model.subsq = Set(model.subsquares, dimen = 2)

model.subsq[1] = [(i,j) for i in range(1,4) for j in range(1,4)]
model.subsq[2] = [(i,j) for i in range(1,4) for j in range(4,7)]
model.subsq[3] = [(i,j) for i in range(1,4) for j in range(7,10)]

model.subsq[4] = [(i,j) for i in range(4,7) for j in range(1,4)]
model.subsq[5] = [(i,j) for i in range(4,7) for j in range(4,7)]
model.subsq[6] = [(i,j) for i in range(4,7) for j in range(7,10)]

model.subsq[7] = [(i,j) for i in range(7,10) for j in range(1,4)]
model.subsq[8] = [(i,j) for i in range(7,10) for j in range(4,7)]
model.subsq[9] = [(i,j) for i in range(7,10) for j in range(7,10)]


#Variables

#binary variable indicating whether a value is assigned to a certain cell
model.assign = Var(model.rows, model.columns, model.value, within = Binary)

#for values already in the starting board fix the corresponding binaries at 1
for (r,c,v) in model.board:
    model.assign[r,c,v].fix(1)
    
        
#Objective

#since this is a feasibility problem we max/min a constant
model.obj = Objective(expr = 1)


#Constraints

#there should be exactly one number in each row
def row_val(model, r, v):
    return sum(model.assign[r,c,v] for c in model.columns) == 1
model.row_val = Constraint(model.rows, model.value, rule = row_val)


#there should be exactly one number in each column
def col_val(model, c, v):
    return sum(model.assign[r,c,v] for r in model.rows) == 1
model.col_val = Constraint(model.columns, model.value, rule = col_val)


#there should be exactly one number in each subsquare
def subsq_val(model, s, v):
    return sum(model.assign[r,c,v] for (r,c) in model.subsq[s]) == 1
model.subsq_val = Constraint(model.subsquares, model.value, rule = subsq_val)


#there should be exactly one number in each cell
def cell_val(model, r, c):
    return sum(model.assign[r,c,v] for v in model.value) == 1
model.cell_val = Constraint(model.rows, model.columns, rule = cell_val)


#function to add Integer Cuts
def Int_cuts(model):
    if not hasattr(model, "IntegerCuts"):
        model.IntegerCuts = ConstraintList()

    cut_expr = 0.0
    for r in model.rows:
        for c in model.columns:
            for v in model.value:
                if not model.assign[r,c,v].fixed:
                    if value(model.assign[r,c,v]) > 0.5:
                        cut_expr += (1 - model.assign[r,c,v])
                    else:
                        cut_expr += model.assign[r,c,v]
    model.IntegerCuts.add(cut_expr >= 1)


#function to print the current solution
def print_sol(model):
    for r in model.rows:
        print(' '.join(str(v) for c in model.columns for v in model.value if value(model.assign[r,c,v] > 0.5)))


sol_count = 0
while 1:
    with SolverFactory('gurobi_direct') as opt:
        results = opt.solve(model)
        if results.solver.termination_condition != TerminationCondition.optimal:
            print("All board solutions have been found")
            break

    sol_count += 1

    Int_cuts(model)

    print('Solution %d' % sol_count)
    print_sol(model)
    





