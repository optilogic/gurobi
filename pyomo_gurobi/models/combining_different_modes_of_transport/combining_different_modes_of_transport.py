# A load of 20 tonnes needs to be transported on a route passing through five cities, with a 
# choice of three different modes of transport: rail, road, and air. In any of the three 
# intermediate cities it is possible to change the mode of transport but the load uses a 
# single mode of transport between two consecutive cities. The cost of transport in $ per tonne 
# between the pairs of cities as well as the costs for changing the mode of transport in $ per tonne 
# are given. The problem is to determine the transport load so as to minimize the cost.

from pyomo.environ import *

model = AbstractModel("Combining different modes of transport")

#Sets and parameters

#set of city pairs
model.city_pairs = Set()

#set of all city pairs but the last
model.all_but_last = Set()

#set of transportation modes
model.transp_modes = Set()

#cost per city pair for each transporation mode
model.transp_cost = Param(model.transp_modes, model.city_pairs, within = NonNegativeReals)

#cost for changing mode of transport
model.change_transp_mode = Param(model.transp_modes, model.transp_modes, within = NonNegativeReals)

#Variables

#binary variable x_mc which is 1 if mode m is used for city_pair c
model.x_mc = Var(model.transp_modes, model.city_pairs, within = Binary)

#binary variable y_mnc which is 1 if there is a change from mode m to mode n during transition from c to c + 1
model.y_mnc = Var(model.transp_modes, model.transp_modes, model.all_but_last, within = Binary)

#Objective

#minimizing the total transportation cost
def min_cost(model):
    return sum(sum(model.transp_cost[m,c]*model.x_mc[m,c] for c in model.city_pairs) for m in model.transp_modes) + sum(sum(sum(model.change_transp_mode[m,n]*model.y_mnc[m,n,c] for c in model.all_but_last) for n in model.transp_modes) for m in model.transp_modes)
model.min_cost = Objective(rule = min_cost)

#Constraints

#A single mode of transport has to be used on each city pair
def single_mode_pair(model, c):
    return sum(model.x_mc[m,c] for m in model.transp_modes) == 1
model.single_mode_pair = Constraint(model.city_pairs, rule = single_mode_pair)

#A single change of the mode of transport may take place at every intermediate city
def single_change(model, c):
    return sum(model.y_mnc[m,n,c] for m in model.transp_modes for n in model.transp_modes) == 1
model.single_change = Constraint(model.all_but_last, rule = single_change)

#implication constraints: if there was a change of transportation mode from m to n while transiting from c to c + 1 then transportation mode m is used for city pair c and if transportation mode m is not used for city pair c then there was no change of mode transiting from c to c + 1 
#def imply_1(model, m, n, c):
#    return model.x_mc[m,c] >= model.y_mnc[m,n,c], model.x_mc[n,c + 1] >= model.y_mnc[m,n,c]
#model.imply_1 = Constraint(model.transp_modes, model.transp_modes, model.all_but_last, rule = imply_1)

#similar explanation as above
#def imply_2(model, m, n, c):
#    return model.x_mc[n,c + 1] >= model.y_mnc[m,n,c]
#model.imply_2 = Constraint(model.transp_modes, model.transp_modes, model.all_but_last, rule = imply_2)


#an alternative(weaker) formulation of the last two set of constraints is the following:
def implication(model, m, n, c):
    return model.x_mc[m,c] + model.x_mc[n, c + 1] >= 2*model.y_mnc[m,n,c]
model.implication = Constraint(model.transp_modes, model.transp_modes, model.all_but_last, rule = implication)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("combining_different_modes_of_transport.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.transp_modes:
    for j in instance.city_pairs:
        if value(instance.x_mc[i,j]) > 0:
            print(f'Transportation mode {i} is used for city-pair of type {j}')
for i in instance.transp_modes:
    for j in instance.transp_modes:
        for c in instance.all_but_last:
            if i != j and value(instance.y_mnc[i,j,c]) > 0:
                print(f'During transition from city-pair {c} to city-pair {c+1} there is a change in transportation mode from {i} to {j}')
            elif i == j and value(instance.y_mnc[i,j,c]) > 0:
                 print(f'During transition from city-pair {c} to city-pair {c+1} there is no change of transport')
print(f'The minimum of the total transportation cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.transp_modes:
        for j in instance.city_pairs:
            if value(instance.x_mc[i,j]) > 0:
                output.write(f'Transportation mode {i} is used for city-pair of type {j}\n\n')
    for i in instance.transp_modes:
        for j in instance.transp_modes:
            for c in instance.all_but_last:
                if i != j and value(instance.y_mnc[i,j,c]) > 0:
                    output.write(f'During transition from city-pair {c} to city-pair {c+1} there is a change in transportation mode from {i} to {j}\n\n')
                elif i == j and value(instance.y_mnc[i,j,c]) > 0:
                    output.write(f'During transition from city-pair {c} to city-pair {c+1} there is no change of transport\n\n')
    output.write(f'The minimum of the total transportation cost is {value(instance.min_cost)}')
    output.close()
