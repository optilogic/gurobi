# Let S1 and S2 be two RNA strings of length n constructed from
# the nucleotides A, U, C, G. We wish to build a Mixed Integer Linear
# Programming model which will predict the interaction of the given RNA
# sequences. For each possible pair within the first or the second
# sequence there is a weight/score associated to it. There are several
# constraint to be satisfied.

# First, each nucleotide in each sequence must pair to at most one other
# nucleotide in both sequences.

# Secondly, there cannot be crossing among any two pairs of nucleotides whether 
# it is within the same sequence or on two different sequences.

# Lastly, pairs must form stacks to the right or to the left. 

# The objective is to maximize the total pairing score while satisfying
# the above constraints. This problem is formulated below as a binary
# program.

from pyomo.environ import *

model = AbstractModel("RNA-RNA interaction prediction")

#Sets and parameters

#set of ordered nucleotides for first RNA sequence
model.RNA1 = Set(ordered = True)

#set of ordered nucleotides for second RNA sequence
model.RNA2 = Set(ordered = True)

#set of pairs between the two sequences
def pairs(model):
    return ((i,j) for i in model.RNA1 for j in model.RNA2)
model.pairs = Set(dimen = 2, initialize = pairs)

#weight on each possible pair in the cartesian product of two sequences
model.weight_pairs = Param(model.pairs, within = NonNegativeReals)

#weight on each possible matching pair within RNA1
model.weight1 = Param(model.RNA1, model.RNA1, within = NonNegativeReals)

#weight on each possible matching pair within RNA2
model.weight2 = Param(model.RNA2, model.RNA2, within = NonNegativeReals)

#hybridization probability
model.hybridization = Param(within = NonNegativeReals)

#Variables

#binary variable indicating whether a pair in the first sequence is matched
model.match1 = Var(model.RNA1, model.RNA1, within = Binary)

#binary variable indicating whether a pair in the second sequence is matched
model.match2 = Var(model.RNA2, model.RNA2, within = Binary)

#binary variable indicating whether a pair with one nucleotide from the first sequence and the other from the second sequence
model.match3 = Var(model.RNA1, model.RNA2, within = Binary)

#Objective

#maximizing the number of pairings
def max_pairs(model):
    x = 0
    for i in range(1,4):
        for j in range(i+1, 5):
            x += model.weight1[i,j]*model.match1[i,j]
    y = 0
    for i in range(1,4):
        for j in range(i+1,5):
            y += model.weight2[i,j]*model.match2[i,j]
    z = 0
    for i in range(1,5):
        for j in range(1,5):
            z += model.weight_pairs[i,j]*model.match3[i,j]
    return x + y + model.hybridization*z
model.max_pairs = Objective(rule = max_pairs, sense = maximize)


#Constraints

#every nucleotide in the first sequence can be paired to at most one nucleotide from either itself or second sequence
def each_base_1(model, i):
    return sum(model.match1[j,i] for j in range(1, i)) + sum(model.match1[i,k] for k in range(i+1,5)) + sum(model.match3[i,l] for l in range(1,5)) <= 1
model.each_base_1 = Constraint(model.RNA1, rule = each_base_1)

#every nucleotide in the second sequence can be paired to at most one nucleotide from either itself or first sequence
def each_base_2(model, i):
    return sum(model.match2[j,i] for j in range(1, i)) + sum(model.match2[i,k] for k in range(i+1,5)) + sum(model.match3[l,i] for l in range(1,5)) <= 1
model.each_base_2 = Constraint(model.RNA2, rule = each_base_2)

#no crossing allowed among every two pairs in the first sequence
def no_cross_1(model, i, k, j, l):
    if i < k and k < j and j < l:
        return model.match1[i,j] + model.match1[k,l] <= 1
    else:
        return Constraint.Skip
model.no_cross_1 = Constraint(model.RNA1, model.RNA1, model.RNA1, model.RNA1, rule = no_cross_1)

#no crossing allowed among every two pairs in the second sequence
def no_cross_2(model, i, k, j, l):
    if i < k and k < j and j < l:
        return model.match2[i,j] + model.match2[k,l] <= 1
    else:
        return Constraint.Skip
model.no_cross_2 = Constraint(model.RNA2, model.RNA2, model.RNA2, model.RNA2, rule = no_cross_2)

#no crossing allowed among every two pairs between the sequences
def no_cross_12(model, i, k, j, l):
    if i < k and k < j and j < l:
        return model.match3[i,j] + model.match3[k,l] <= 1
    else:
        return Constraint.Skip
model.no_cross_12 = Constraint(model.RNA1, model.RNA1, model.RNA2, model.RNA2, rule = no_cross_12)

#right stacked pairs constraints
def stack_right(model, i):
    if i != 1 and i != 5:
        return sum(model.match1[i,j] for j in range(i, 5)) + (1 - sum(model.match1[i,j] for j in range(i+1, 5))) + sum(model.match1[i,j] for j in range(i+2, 5)) >= 1
    else:
        return Constraint.Skip
model.stack_right = Constraint(model.RNA1, rule = stack_right)

#left stacked pairs constraints
def stack_left(model, i):
    if i != 1 and i != 5:
        return sum(model.match1[j,i] for j in range(1, i-1)) + (1 - sum(model.match1[j,i] for j in range(1, i))) + sum(model.match1[j,i] for j in range(1, i+1)) >= 1
    else:
        return Constraint.Skip
model.stack_left = Constraint(model.RNA1, rule = stack_left)

#Linking the model with the data(creating an instance), invoking and applying the solver to solve the instance
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("rna_rna_interaction_prediction.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in range(1,4):
    for j in range(i+1, 5):
        if value(instance.match1[i,j]) > 0:
            print("In the first sequence, amino acid at position %s matches amino acid at position %s" %(i,j))
for i in range(1,4):
    for j in range(i+1, 5):
        if value(instance.match2[i,j]) > 0:
            print("In the second sequence, amino acid at position %s matches amino acid at position %s" %(i,j))
for i in range(1,5):
    for j in range(1, 5):
        if value(instance.match3[i,j]) > 0:
            print("Amino acid at position %s in the first sequence matches amino acid at position %s in the second sequence" %(i,j))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in range(1,4):
        for j in range(i+1, 5):
            if value(instance.match1[i,j]) > 0:
                output.write("In the first sequence, amino acid at position %s matches amino acid at position %s\n\n" %(i,j))
    for i in range(1,4):
        for j in range(i+1, 5):
            if value(instance.match2[i,j]) > 0:
                output.write("In the second sequence, amino acid at position %s matches amino acid at position %s\n\n" %(i,j))
    for i in range(1,5):
        for j in range(1, 5):
            if value(instance.match3[i,j]) > 0:
                output.write("Amino acid at position %s in the first sequence matches amino acid at position %s in the second sequence\n\n" %(i,j))
    output.close()
