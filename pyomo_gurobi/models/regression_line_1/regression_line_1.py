# A set of 19 two-dimensional points are given. We are interested in
# finding the equation of a line y = b x + a, so as to minimize
# the sum of absolute deviations of each observed value of y from the
# value predicted by the linear relationship.

from pyomo.environ import *

model = AbstractModel("Regression Line 1")

#Sets and parameters

#set of all points
model.points = Set()

#values of each x coordinate given
model.x = Param(model.points, within = NonNegativeReals)

#values of each y coordinate given
model.y = Param(model.points, within = NonNegativeReals)

#Variables

#constant coefficient of the line
model.a = Var(within = Reals)

#gradient of the line
model.b = Var(within = Reals)

#amounts by which the values of y[i] proposed by the linear expression
#differ from the observed values
model.u = Var(model.points, within = NonNegativeReals)

#amounts by which the values of y[i] proposed by the linear expression
#differ from the observed values
model.v = Var(model.points, within = NonNegativeReals)

#Objective

#minimizing the sum of absolute deviations of each observed value of y from the value predicted by the linear relationship
def min_deviation(model):
    return sum(model.u[i] for i in model.points) + sum(model.v[i] for i in model.points)
model.min_deviation = Objective(rule = min_deviation)

#Constraints

#constraints representing the line fitting of given points (x[i], y[i])
def line_fitting(model, i):
    return model.b*model.x[i] + model.a + model.u[i] - model.v[i] ==model.y[i]
model.line_fitting = Constraint(model.points, rule = line_fitting)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("regression_line_1.dat")
results = solver.solve(instance)
print("The predicted line coefficients are:")
print("%s = %f, %s = %f\n" %(instance.a,value(instance.a), instance.b, value(instance.b)))
print("The deviations for each given y[i] from the predicted ones are: ")
for i in instance.points:
    if value(instance.u[i]) > 0 or value(instance.v[i]) > 0:
        print("Observed y[%s] differs from the predicted y[%s] by %f" %(i, i, max(value(instance.u[i]), value(instance.v[i]))))
print("\nThe minimum sum of absolute deviations of each observed value of y from the value predicted by the linear relationship is: %f" %value(instance.min_deviation))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("The predicted line coefficients are: \n\n")
    output.write("%s = %f, %s = %f\n\n" %(instance.a,value(instance.a), instance.b, value(instance.b)))
    output.write("The deviations for each given y[i] from the predicted ones are: \n\n")
    for i in instance.points:
        if value(instance.u[i]) > 0 or value(instance.v[i]) > 0:
            output.write("Observed y[%s] differs from the predicted y[%s] by %f\n\n" %(i, i, max(value(instance.u[i]), value(instance.v[i]))))
    output.write("The minimum sum of absolute deviations of each observed value of y from the value predicted by the linear relationship is: %f" %value(instance.min_deviation))
    output.close()
