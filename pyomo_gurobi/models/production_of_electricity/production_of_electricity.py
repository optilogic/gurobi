# Power generators of four different types are available to satisfy the daily 
# electricity demands given. We consider a sliding time horizon: the period 10pm-12am 
# of day d is followed by the period 0am-6am of day d+1. The power generators of the 
# same type have a maximum capacity and may be connected to the network starting from 
# a certain minimal power output. They have a start-up cost, a fixed hourly cost for 
# working at minimal power, and an hourly cost per additional megawatt for anything 
# beyond the minimal output. A power generator can only be started or stopped at the 
# beginning of a time period. At any moment, the working power generators must be able 
# to cope with an increase by 20% of the demand forecast. The problem is to determine 
# which power generators should be used in every period in order to minimize the total daily cost.

from pyomo.environ import *

model = AbstractModel("Production of electricity")

#Sets and parameters

#set of time periods
model.time_periods = Set()

#demand per time period
model.demand = Param(model.time_periods, within = NonNegativeReals)

#length of each time period
model.length_time_period = Param(model.time_periods, within = NonNegativeIntegers)

#set of generator types
model.generators = Set()

#Number of available generators for each type
model.numb_available = Param(model.generators, within = NonNegativeReals)

#minimum output for each type of generator
model.min_output = Param(model.generators, within = NonNegativeReals)

#maximum capacity for each type of generator
model.max_capacity = Param(model.generators, within = NonNegativeReals)

#fixed cost per hour for each generator type
model.fixed_cost = Param(model.generators, within = NonNegativeReals)

#cost per hour per additional MW for each generator type
model.add_MW_cost = Param(model.generators, within = NonNegativeReals)

#start-up cost
model.start_cost = Param(model.generators, within = NonNegativeReals)

#Variables

#integer variables indicating the number of power generators of a type starting at the beginning of a time period
model.start_pt = Var(model.generators, model.time_periods, within = NonNegativeIntegers)

#integer variables indicating the number of power generators of a type working at a time period
model.work_pt = Var(model.generators, model.time_periods, within = NonNegativeIntegers)

#additional production beyond the minimum output level of generators of type p in time period t
model.add_prod_pt = Var(model.generators, model.time_periods, within = NonNegativeReals)

#Objective

#minimize the total cost
def min_cost(model):
    return sum(sum(model.start_cost[p]*model.start_pt[p,t] + model.length_time_period[t]*(model.fixed_cost[p]*model.work_pt[p,t] + model.add_MW_cost[p]*model.add_prod_pt[p,t]) for t in model.time_periods) for p in model.generators)
model.min_cost = Objective(rule = min_cost)

#Constraints

#upper bound on the number of generators of a type working at a time period
def upper_bound(model, p, t):
    return model.work_pt[p,t] <= model.numb_available[p]
model.upper_bound = Constraint(model.generators, model.time_periods, rule = upper_bound)

#limit on the additional production
def limit_add_prod(model, p, t):
    return model.add_prod_pt[p,t] <= (model.max_capacity[p] - model.min_output[p])*model.work_pt[p,t]
model.limit_add_prod = Constraint(model.generators, model.time_periods, rule = limit_add_prod)

#demand satisfaction at each time period
def demand_constraints(model, t):
    return sum(model.min_output[p]*model.work_pt[p,t] + model.add_prod_pt[p,t] for p in model.generators) >= model.demand[t]
model.demand_constraints = Constraint(model.time_periods, rule = demand_constraints)

#Without starting any new generators, those that are working must be able to produce 20% more than planned
def add_demand_satisfied(model, t):
    return sum(model.max_capacity[p]*model.work_pt[p,t] for p in model.generators) >= 1.2*model.demand[t] 
model.add_demand_satisfied = Constraint(model.time_periods, rule = add_demand_satisfied)

#the number of power generators starting to work in a period t would be at least the difference between the numbers of generators working in the current period and the precceding one
def gen_start_at_t(model, p, t):
    if t == 1:
        return model.start_pt[p,1] >= model.work_pt[p,1] - model.work_pt[p,7]
    else:
        return model.start_pt[p,t] >= model.work_pt[p,t] - model.work_pt[p,t-1]
model.gen_start_at_t = Constraint(model.generators, model.time_periods, rule = gen_start_at_t)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("production_of_electricity.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.generators:
    for j in instance.time_periods:
        if value(instance.start_pt[i,j]) > 0:
            print(f'{value(instance.start_pt[i,j])} generators of type {i} will start at the beginning of time period {j}')
for i in instance.generators:
    for j in instance.time_periods:
        if value(instance.work_pt[i,j]) > 0:
            print(f'{value(instance.start_pt[i,j])} generators of type {i} will be working during time period {j}')
for i in instance.generators:
    for j in instance.time_periods:
        if value(instance.add_prod_pt[i,j]) > 0:
            print(f'The additional output of generator {i} at time period {j} is {value(instance.add_prod_pt[i,j])}')
print(f'The minimum total cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.generators:
        for j in instance.time_periods:
            if value(instance.start_pt[i,j]) > 0:
                output.write(f'{value(instance.start_pt[i,j])} generators of type {i} will start at the beginning of time period {j}\n\n')
    for i in instance.generators:
        for j in instance.time_periods:
            if value(instance.work_pt[i,j]) > 0:
                output.write(f'{value(instance.start_pt[i,j])} generators of type {i} will be working during time period {j}\n\n')
    for i in instance.generators:
        for j in instance.time_periods:
            if value(instance.add_prod_pt[i,j]) > 0:
                output.write(f'The additional output of generator {i} at time period {j} is {value(instance.add_prod_pt[i,j])}\n\n')
    output.write(f'The minimum total cost is {value(instance.min_cost)}')
    output.close()
