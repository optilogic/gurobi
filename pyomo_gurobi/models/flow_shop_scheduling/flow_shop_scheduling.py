# A workshop that produces metal pipes on demand for the automobile industry has three 
# machines for bending the pipes, soldering the fastenings, and assembling the links. 
# The workshop has to produce six pieces, for which the durations of the processing steps 
# are given. Every workpiece first goes to bending, then to soldering, and finally to assembly 
# of the links. Once started, any operations must be carried out without interruption, but the 
# workpieces may wait between the machines. Every machine only processes one piece at a time. 
# The problem is to determine which is the sequence of workpieces that minimizes the total time 
# for completing all pieces.

from pyomo.environ import *

model = AbstractModel("Flow shop scheduling")

#Sets and parameters

#set of machines
model.machines = Set()

#set of jobs
model.jobs = Set()

#set of ranks
model.ranks = Set()

#duration of job j on machine m
model.duration = Param(model.machines, model.jobs, within = NonNegativeReals)

#Variables

#binary variables indicating the position of job j in the starting sequence
model.rank = Var(model.jobs, model.ranks, within = Binary)

#variable indicating the waiting time for the job with rank k between its processing on machines m and m + 1
model.wait = Var(model.machines, model.ranks, within = NonNegativeReals)

#variable indicating the time between the processing of the jobs with rank k and k + 1 on a machine m
model.empty = Var(model.machines, model.ranks, within = NonNegativeReals)

#the processing duration of the job of rank k on machine m
model.dur = Var(model.machines, model.ranks, within = NonNegativeReals)

#Objective

#minimizing the total time for completing all jobs
def min_total_time(model):
    return sum(sum(model.duration[m,j]*model.rank[j,1] for j in model.jobs) for m in model.machines if m != 3) + sum(model.empty[3,k] for k in model.ranks if k != 6)
model.min_total_time = Objective(rule = min_total_time)

#Constraint

#each rank corresponds to exactly one job
def one_job_per_rank(model, r):
    return sum(model.rank[j,r] for j in model.jobs) == 1
model.one_job_per_rank = Constraint(model.ranks, rule = one_job_per_rank)

#each job corresponds to exactly one rank
def one_rank_per_job(model, j):
    return sum(model.rank[j,r] for r in model.ranks) == 1
model.one_rank_per_job = Constraint(model.jobs, rule = one_rank_per_job)

#the first workpiece in the sequence can pass through all machines without any waiting times
def no_wait_m1(model, m):
    if m == 3:
        return Constraint.Skip
    else:
        return model.wait[m,1] == 0
model.no_wait_m1 = Constraint(model.machines, rule = no_wait_m1)

#The workpieces can be processed without any pause on the first machine
def no_pause_1k(model, k):
    if k == 6:
        return Constraint.Skip
    else:
        return model.empty[1,k] == 0
model.no_pause_1k = Constraint(model.ranks, rule = no_pause_1k)

#the processing duration of the job of rank k on machine m
def dur_job_k_mach_m(model, m, k):
    return model.dur[m,k] == sum(model.duration[m,j]*model.rank[j,k] for j in model.jobs)
model.dur_job_k_mach_m = Constraint(model.machines, model.ranks, rule = dur_job_k_mach_m)

#the time between the completion of job k on machine m and the start of job k + 1 on machine m + 1
def time_match(model, m, k):
    if m == 3:
        return Constraint.Skip
    elif k == 6:
        return Constraint.Skip
    else:
        return model.empty[m,k] + model.dur[m,k+1] + model.wait[m,k+1] == model.wait[m,k] + model.dur[m+1,k] + model.empty[m+1,k]
model.time_match = Constraint(model.machines, model.ranks, rule = time_match)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("flow_shop_scheduling.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.jobs:
    for j in instance.ranks:
        if value(instance.rank[i,j]) > 0:
            print(f'Job {i} is ranked {j} in the starting sequence')
for i in instance.machines:
    for j in instance.ranks:
        if i != 3 and value(instance.wait[i,j]) > 0:
            print(f'The wait time for job ranked {j} in the starting sequence between processing on machines {i} and {i+1} is {value(instance.wait[i,j])}')
for i in instance.machines:
    for j in instance.ranks:
        if j != 6 and value(instance.empty[i,j]) > 0:
            print(f'The wait time for jobs ranked {j} and {j+1} in the starting sequence to be proccessed on machine {i} is {value(instance.empty[i,j])}')
for i in instance.machines:
    for j in instance.ranks:
        if value(instance.dur[i,j]) > 0:
            print(f'Job ranked {j} in the starting sequence takes {value(instance.dur[i,j])} minutes to be proccessed on machine {i}')
print(f'The minimum total time for completing all jobs is {value(instance.min_total_time)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.jobs:
        for j in instance.ranks:
            if value(instance.rank[i,j]) > 0:
                output.write(f'Job {i} is ranked {j} in the starting sequence\n\n')
    for i in instance.machines:
        for j in instance.ranks:
            if i != 3 and value(instance.wait[i,j]) > 0:
                output.write(f'The wait time for job ranked {j} in the starting sequence between processing on machines {i} and {i+1} is {value(instance.wait[i,j])}\n\n')
    for i in instance.machines:
        for j in instance.ranks:
            if j != 6 and value(instance.empty[i,j]) > 0:
                output.write(f'The wait time for jobs ranked {j} and {j+1} in the starting sequence to be proccessed on machine {i} is {value(instance.empty[i,j])}\n\n')
    for i in instance.machines:
        for j in instance.ranks:
            if value(instance.dur[i,j]) > 0:
                output.write(f'Job ranked {j} in the starting sequence takes {value(instance.dur[i,j])} minutes to be proccessed on machine {i}\n\n')
    output.write(f'The minimum total time for completing all jobs is {value(instance.min_total_time)}')
    output.close()
