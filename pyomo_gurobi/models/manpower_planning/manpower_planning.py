# A company is undergoing a number of changes that will affect its
# manpower requirements in future years. For the next three years are
# given the estimated manpower requirements. The company wishes to
# decide its policy over the next three years with regard to
# recruitment, retraining, redundancy and short-time working. Usually, a
# large number of workers leave during their first year at the
# company. After the first year the rate goes down.

# By satisfying several other constraints on the above categories the
# company wishes to minimize the number of redundant employees each
# year.

from pyomo.environ import *

model = AbstractModel("Manpower Planning")

#Sets and parameters

#set of years
model.years = Set()

#set of employee types
model.type_employees = Set()

#current number of each type of employees
model.current_strength = Param(model.type_employees, within = NonNegativeIntegers)

#number of each type of employees needed each year
model.employee_year = Param(model.type_employees, model.years, within = NonNegativeIntegers)

#Variables

#Strength of labour force

#number of skilled workers employed in year i
model.t_sk = Var(model.years, within = NonNegativeIntegers)

#number of semi-skilled workers employed in year i
model.t_ss = Var(model.years, within = NonNegativeIntegers)

#number of unskilled workers employed in year i
model.t_us = Var(model.years, within = NonNegativeIntegers)

#Recruitment

#number of skilled workers recruited in year i
model.u_sk = Var(model.years, within = NonNegativeIntegers, bounds = (0,500))

#number of semi-skilled workers recruited in year i
model.u_ss = Var(model.years, within = NonNegativeIntegers, bounds = (0,800))

#number of unskilled workers recruited in year i
model.u_us = Var(model.years, within = NonNegativeIntegers, bounds = (0,500))

#Retraining

#number of unskilled workers retrained to semi-skilled in year i
model.v_usss = Var(model.years, within = NonNegativeIntegers, bounds = (0,200))

#number of semi-skilled workers retrained to skilled in year i
model.v_sssk = Var(model.years, within = NonNegativeIntegers)

#Downgrading

#number of skilled workers downgraded to semi-skilled in year i
model.v_skss = Var(model.years, within = NonNegativeIntegers)

#number of skilled workers downgraded to unskilled in year i
model.u_skus = Var(model.years, within = NonNegativeIntegers)

#number of semi-skilled workers downgraded to unskilled in year i
model.v_ssus = Var(model.years, within = NonNegativeIntegers)

#Redundancy

#number of skilled workers made redundant in year i
model.w_sk = Var(model.years, within = NonNegativeIntegers)

#number of semi-skilled workers made redundant in year i
model.w_ss = Var(model.years, within = NonNegativeIntegers)

#number of unskilled workers made redundant in year i
model.w_us = Var(model.years, within = NonNegativeIntegers)

#Short-time working

#number of skilled workers on short-time working in year i
model.x_sk = Var(model.years, within = NonNegativeIntegers, bounds = (0,50))

#number of semi-skilled workers on short-time working in year i
model.x_ss = Var(model.years, within = NonNegativeIntegers, bounds = (0,50))

#number of unskilled workers on short-time working in year i
model.x_us = Var(model.years, within = NonNegativeIntegers, bounds = (0,50))

#Overmanning

#number of superfluous skilled workers employed in year i
model.y_sk = Var(model.years, within = NonNegativeIntegers)

#number of superfluous semi-skilled workers employed in year i
model.y_ss = Var(model.years, within = NonNegativeIntegers)

#number of superfluous unskilled workers employed in year i
model.y_us = Var(model.years, within = NonNegativeIntegers)

#Objective

#minimizing redundancy
def min_redund(model):
    return sum((model.w_sk[i] + model.w_ss[i] + model.w_us[i]) for i in model.years)
model.min_redund = Objective(rule = min_redund) 

#Constraints

#constraints garanteeing continuity
def cont_sk(model, i):
    if i == 1:
        return model.t_sk[1] == 0.95*model.current_strength[3] + 0.9*model.u_sk[1] + 0.95*model.v_sssk[1] - model.v_skss[1] - model.u_skus[1] - model.w_sk[1]
    else:
        return model.t_sk[i] == 0.95*model.t_sk[i-1] + 0.9*model.u_sk[i] + 0.95*model.v_sssk[i] - model.v_skss[i] - model.u_skus[i] - model.w_sk[i]
model.cont_sk = Constraint(model.years, rule = cont_sk)

def cont_ss(model, i):
    if i == 1:
        return model.t_ss[1] == 0.95*model.current_strength[2] + 0.8*model.u_ss[1] + 0.95*model.v_usss[1] - model.v_sssk[1] +0.5*model.v_skss[1] - model.v_ssus[1] - model.w_ss[1]
    else:
        return model.t_ss[i] == 0.95*model.t_ss[i-1] + 0.8*model.u_ss[i] + 0.95*model.v_usss[i] - model.v_sssk[i] + 0.5*model.v_skss[i] - model.v_ssus[i] - model.w_ss[i]
model.cont_ss = Constraint(model.years, rule = cont_ss)

def cont_us(model, i):
    if i == 1:
        return model.t_us[1] == 0.9*model.current_strength[1] + 0.75*model.u_us[1] - model.v_usss[1] + 0.5*model.u_skus[1] + 0.5*model.v_ssus[1] - model.w_us[1]
    else:
        return model.t_us[i] == 0.9*model.t_us[i-1] + 0.75*model.u_us[i] - model.v_usss[i] + 0.5*model.u_skus[i] + 0.5*model.v_ssus[i] - model.w_us[i]
model.cont_us = Constraint(model.years, rule = cont_us)

#Retraining Semi-Skilled Workers

#The retraining of the semi-skilled workers to make them skilled is limited to no more than one quarter of the skilled labour force at the time 
def retrain_ss_sk(model, i):
    return model.v_sssk[i] <= 0.25*model.t_sk[i]
model.retrain_ss_sk = Constraint(model.years, rule = retrain_ss_sk)

#Overmanning

#It is possible to employ no more than 150 workers over the whole company than are needed
def ub_overman(model, i):
    return model.y_sk[i] + model.y_ss[i] + model.y_us[i] <= 150
model.ub_overman = Constraint(model.years, rule = ub_overman)

#Requirements

def req(model, e, i):
    if e == 3:
        return model.t_sk[i] - model.y_sk[i] - 0.5*model.x_sk[i] == model.employee_year[3,i]
    elif e == 2:
        return model.t_ss[i] - model.y_ss[i] - 0.5*model.x_us[i] == model.employee_year[2,i]
    else:
        return model.t_us[i] - model.y_us[i] - 0.5*model.x_us[i] == model.employee_year[1,i]
model.req = Constraint(model.type_employees, model.years, rule = req)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("manpower_planning.dat")
instance.write("Manpower.lp")
results = solver.solve(instance)

for t in instance.years:
    if value(instance.w_sk[t]) > 0:
        print("Number of skilled worker made redundant at year %s is: %f "%(t,value(instance.w_sk[t])))
for t in instance.years:
    if value(instance.w_ss[t]) > 0:
        print("Number of semi-skilled worker made redundant at year %s is: %f "%(t,value(instance.w_ss[t])))
for t in instance.years:
    if value(instance.w_us[t]) > 0:
        print("Number of unskilled worker made redundant at year %s is: %f "%(t,value(instance.w_us[t])))
print("The minimum number of redundant employees is: %f" %value(instance.min_redund))

        
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for t in instance.years:
        if value(instance.w_sk[t]) > 0:
            output.write("Number of skilled worker made redundant at year %s is: %f\n\n "%(t,value(instance.w_sk[t])))
    for t in instance.years:
        if value(instance.w_ss[t]) > 0:
            output.write("Number of semi-skilled worker made redundant at year %s is: %f\n\n "%(t,value(instance.w_ss[t])))
    for t in instance.years:
        if value(instance.w_us[t]) > 0:
            output.write("Number of unskilled worker made redundant at year %s is: %f \n\n"%(t,value(instance.w_us[t])))
    output.write("The minimum number of redundant employees is: %f" %value(instance.min_redund))
    output.close()

