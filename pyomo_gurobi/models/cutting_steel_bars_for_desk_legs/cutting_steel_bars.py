# A company produces desks of different sizes. The legs of the desks all have 
# the same diameter, with different lengths: 40 cm for the smallest ones, 60 cm 
# for medium height, and 70 cm for the largest ones. These legs are cut from steel 
# bars of 1.5 or 2 meters. The company has received an order for 108 small, 125 
# medium and 100 large desks. The problem is to determine how should this order 
# be produced so as to minimize the trim loss.

from pyomo.environ import *

model = AbstractModel("Cutting steel bars for desk legs")

#Sets and parameters

#set of desk sizes
model.sizes = Set()

#demand for each size
model.demand = Param(model.sizes, within = NonNegativeIntegers)

#set of bars
model.bars = Set()

#lengths of bars
model.bar_lengths = Param(model.bars, within = NonNegativeIntegers)

#set of patterns for bar 1
model.patterns_1 = Set()

#set of patterns for bar 2
model.patterns_2 = Set()

#set of all patterns
model.patterns = model.patterns_1 | model.patterns_2

#number of size s in pattern p
model.number_sp = Param(model.sizes, model.patterns, within = NonNegativeIntegers)

#Variables

#integer variable indicating the number of times a pattern is used
model.number_pattern_used = Var(model.patterns, within = NonNegativeIntegers)

#Objective

#minimizing the total loss of metal
def min_loss(model):
    return sum(model.bar_lengths[1]*model.number_pattern_used[p] for p in model.patterns_1) + sum(model.bar_lengths[2]*model.number_pattern_used[p] for p in model.patterns_2) - sum(4 * model.demand[s]*s for s in model.sizes)
model.min_loss = Objective(rule = min_loss) 

#Constraints

#demand satisfaction constraints
def demand_satisfaction(model, s):
    return sum(model.number_sp[s,p]*model.number_pattern_used[p] for p in model.patterns) >= 4 * model.demand[s]
model.demand_satisfaction = Constraint(model.sizes, rule = demand_satisfaction)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("cutting_steel_bars.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.patterns:
        if value(instance.number_pattern_used[i]) > 0:
            print(f'Pattern {i} is used {value(instance.number_pattern_used[i])} times')
print(f'The minimum total loss of metal is {value(instance.min_loss)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.patterns:
        if value(instance.number_pattern_used[i]) > 0:
            output.write(f'Pattern {i} is used {value(instance.number_pattern_used[i])} times\n\n')
    output.write(f'The minimum total loss of metal is {value(instance.min_loss)}')
    output.close()
