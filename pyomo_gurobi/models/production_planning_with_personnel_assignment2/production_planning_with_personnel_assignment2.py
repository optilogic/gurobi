# A company wishes to produce 4 type of products into 5 production lines. 
# In addition to the previous model there are two constraints on the number 
# of hours transferred from a line to another. While satisfying capacity on 
# each production line the company wishes to determine the quantity to produce 
# of each product type by maximizing the total profit.

from pyomo.environ import *

model = AbstractModel("Production planning with personnel assignment2")

#Sets and parameters

#set of products
model.products = Set()

#set of production lines
model.lines = Set()

#profit per unit of each product
model.profit = Param(model.products, within = NonNegativeReals)

#the time needed for product p in line l
model.duration = Param(model.products, model.lines, within = NonNegativeReals)

#capacity on each production line
model.capacity = Param(model.lines, within = NonNegativeReals)

#maximum number of transferable hours per line
model.max_transfer = Param(model.lines, within = NonNegativeReals)

#set of arcs between lines
model.arcs = Set(within = model.lines*model.lines)

#Variables

#integer variable indicating the amount of each product to be produced
model.produce = Var(model.products, within = NonNegativeIntegers)

#hours work by each line
model.hours = Var(model.lines, within = NonNegativeReals)

#number of hours transfered from line l to line k
model.transfer = Var(model.lines, model.lines, within = NonNegativeReals)

#Objective

#maximizing the profit 
def max_profit(model):
    return sum(model.produce[p]*model.profit[p] for p in model.products)
model.max_profit = Objective(rule = max_profit, sense = maximize)

#Constraints

#capacity limits of every production line
def cap_limit(model, l):
    return sum(model.duration[p,l]*model.produce[p] for p in model.products) <= model.hours[l]
model.cap_limit = Constraint(model.lines, rule = cap_limit)

#the balance between the working hours carried out on a line, the hours transfered and the maximum number of working hours on this line
def balance(model, l):
    return model.hours[l] == model.capacity[l] + sum(model.transfer[k,l] for k in model.lines if (k,l) in model.arcs) - sum(model.transfer[l,k] for k in model.lines if (l,k) in model.arcs)
model.balance = Constraint(model.lines, rule = balance)

#limit on the number of hours that may be transfered from a line to the other
def transfer_limit(model, l):
    return sum(model.transfer[l,k] for k in model.lines if (l,k) in model.arcs) <= model.max_transfer[l]
model.transfer_limit = Constraint(model.lines, rule = transfer_limit)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("production_planning_with_personnel_assignment2.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.products:
    if value(instance.produce[i]) > 0:
        print(f'Product {i} will be produced on the amount of {value(instance.produce[i])}')
for i in instance.lines:
    if value(instance.hours[i]) > 0:
        print(f'Line {i} will work {value(instance.hours[i])} hours')
for i in instance.lines:
    for j in instance.lines:
        if (i,j) in instance.arcs and value(instance.transfer[i,j]) > 0:
            print(f'{value(instance.transfer[i,j])} hours need to be transfered from line {i} to line {j}')
print(f'The maximum profit is {value(instance.max_profit)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.products:
        if value(instance.produce[i]) > 0:
            output.write(f'Product {i} will be produced on the amount of {value(instance.produce[i])}\n\n')
    for i in instance.lines:
        if value(instance.hours[i]) > 0:
            output.write(f'Line {i} will work {value(instance.hours[i])} hours\n\n')
    for i in instance.lines:
        for j in instance.lines:
            if (i,j) in instance.arcs and value(instance.transfer[i,j]) > 0:
                output.write(f'{value(instance.transfer[i,j])} hours need to be transfered from line {i} to line {j}\n\n')
    output.write(f'The maximum profit is {value(instance.max_profit)}')
    output.close()
