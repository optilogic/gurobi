# An airline has several pilots speaking different languages and each pilot is trained 
# for some aircraft types. In order to fly a plane a pilot and a co-pilot compatible 
# in terms of language and aircraft knowledge are needed. Rating of pilots in terms of 
# language knowledge and aircraft type knowledge are given. A valid flight crew consists 
# of two pilots that both have each at least 10/20 for the same language and 10/20 on the 
# same aircraft type. The problem is to determine whether all pilots can fly simultaneously.

from pyomo.environ import *

model = AbstractModel("Composing flight crews")

#Sets and parameters

#set of pilots
model.pilots = Set()

#set of arcs connecting compatible pilots
model.compatible_pairs = Set(within = model.pilots*model.pilots)

#maximum score for each compatible pair
model.max_score = Param(model.compatible_pairs, within = NonNegativeIntegers)

#Variables

#binary variable indicating whether two compatible pilots will fly together
model.fly = Var(model.compatible_pairs, within = Binary)

#Objective

#maximizing the total score
def max_total_score(model):
    return sum(model.max_score[i,j]*model.fly[i,j] for i,j in model.compatible_pairs)
model.max_total_score = Objective(rule = max_total_score, sense = maximize)

#Constraints

#every pilot i is part of at most one pair
def pilot_in_at_most_1_pair(model, k):
        return sum(model.fly[i,j] for (i,j) in model.compatible_pairs if i == k or j ==k ) <= 1 
model.pilot_in_at_most_1_pair = Constraint(model.pilots, rule = pilot_in_at_most_1_pair)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("composing_flight_crews.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.compatible_pairs:
    if value(instance.fly[i,j]) > 0:
        print(f'Pilot {i} and pilot {j} will fly together')
print(f'The maximum total score is {value(instance.max_total_score)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.compatible_pairs:
        if value(instance.fly[i,j]) > 0:
            output.write(f'Pilot {i} and pilot {j} will fly together\n\n')
    output.write(f'The maximum total score is {value(instance.max_total_score)}')
    output.close()
