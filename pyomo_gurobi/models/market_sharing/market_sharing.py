# A large company with two divisions, D1 and D2, supplies retailers
# with oil and spirit. In order to facilitate the supplying process the
# company wishes to allocate each retailer to either division D1 or
# D2. There are 30 retailers and 4 products to be supplied to
# the retailers. Each retailer i has a demand for product j, \
# d_ij. The division must be as close to 50%/50% as
# possible. The right-hand side of each constraint is chosen to be the
# floor of the mean of demands of all retailers for a product j, \
# s_j.

from pyomo.environ import *

model=AbstractModel("Market sharing")

#Sets and Parameters

#set of retailers
model.retailers = Set()

#set of products
model.products = Set()

#The demand of retailer i for product j
model.demand_ij = Param(model.products, model.retailers, within = NonNegativeIntegers)

#The info on desired market split among the two divisions D_1 and D_2 for each product
model.split = Param(model.products, within = Integers)

#Variables

#Binary variable x_j which is 1 if and only if retailer j is assigned to division 1
model.x = Var(model.retailers, within = Binary)

#Free variable s_i indicating the percentage deviation from the indicated split
model.s = Var(model.products, within = Reals)

#Substitution variable
model.s_ = Var(model.products, within = NonNegativeReals)
model.s__ = Var(model.products, within = NonNegativeReals)

#Objective

#Minimizing the percentage deviation
def percentage_deviation(model):
    return sum(model.s_[i] + model.s__[i] for i in model.products)
model.obj = Objective(rule = percentage_deviation)

#Constraints

#market split constraints
def market_split_per_product(model,i):
    return model.s_[i] - model.s__[i] + sum(model.demand_ij[i,j]*model.x[j] for j in model.retailers) == model.split[i]
model.market_split_per_product = Constraint(model.products, rule = market_split_per_product)

#representation of a free variable as a difference of two nonnegative variables
def substitution(model,i):
    return model.s[i] == model.s_[i] - model.s__[i]
model.substitution = Constraint(model.products, rule = substitution)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("market_sharing.dat")
results = solver.solve(instance)

for i in instance.retailers:
    if value(instance.x[i]) > 0:
        print("Retailer %s is assigned to division 1" %i)
    else:
        print("Retailer %s is assigned to division 2" %i)
print("The minimum percentage deviation is: %f" %value(instance.obj))

        
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.retailers:
        if value(instance.x[i]) > 0:
            output.write("Retailer %s is assigned to division 1 \n\n" %i)
        else:
            output.write("Retailer %s is assigned to division 2 \n\n" %i)
    output.write("The minimum percentage deviation is: %f" %value(instance.obj))
    output.close()
