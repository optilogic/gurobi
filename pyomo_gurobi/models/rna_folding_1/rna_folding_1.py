# Let S be a RNA string of length n constructed from the nucleotides
# A, U, C, G. We wish to build a Mixed Integer Linear Programming model
# which will predict the secondary structure of the given RNA
# sequence. There are several constraint to be satisfied.

# First, any pair in the RNA folding must be a complementary pair, i.e.,
# pairs must fall into one of the following ordered pairs (A,U), (U,A),
# (C,G), (G,C).

# Second, each nucleotide in the sequence must pair to at most one other
# nucleotide in the sequence.

# Lastly, there cannot be crossing among any two pairs.


# The objective is to maximize the number of pairings while satisfying
# the above constraints. This problem is formulated below as a binary
# program.

from pyomo.environ import *

model = ConcreteModel("RNA Folding 1")

#Enter your RNA sequence here:
RNA = "AUCGAUCG"

#Converting the RNA sequence to a list and creating a list with nucleotides' positions
def split(word): 
    return [char for char in word]  
nucleotides = split(RNA)
list1 = list(range(len(nucleotides)))

#Creating a dictionary where keys are the positions of each nucleotide in the sequence and the nucleotide is the corresponding value.
index_nucleotide = {} 
for key in list1: 
    for value in nucleotides: 
        index_nucleotide[key] = value 
        nucleotides.remove(value) 
        break  

#Creating a list of ordered pairs where the first coordinate is less than the second coordinate    
list2 = []    
for i in index_nucleotide:
    for j in index_nucleotide:
        if i < j:
            list2.append((i,j))

#Variables

#binary variable indicating whether two different nucleotides are paired
model.pairing = Var(list2, within = Binary)

#Objective

#maximizing the number of pairings
model.max_pairs = Objective(expr = sum(model.pairing[i,j] for (i,j) in list2), sense = maximize)

#Constraints

#Constraints indicating that noncomplementary nucleotides cannot be matched
model.non_matching = ConstraintList()
for (i,j) in list2:
    if index_nucleotide[i] == 'A' and index_nucleotide[j] != 'U':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'U' and index_nucleotide[j] != 'A':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'C' and index_nucleotide[j] != 'G':
        model.non_matching.add(model.pairing[i,j] == 0)
    elif index_nucleotide[i] == 'G' and index_nucleotide[j] != 'C':
        model.non_matching.add(model.pairing[i,j] == 0)

#Constraints indicating that each nucleotide must be paired to most one other nucleotide
model.matched_at_most_once = ConstraintList()
for i in index_nucleotide:
    model.matched_at_most_once.add(sum(model.pairing[i,j] for j in index_nucleotide if (i,j) in list2) + sum(model.pairing[j,i] for j in index_nucleotide if (j,i) in list2) <= 1)

#Constraints indicating that there cannot be any crossing between any two pairs in the secondary structure
model.no_crossing = ConstraintList()
for (i,j) in list2:
    for (k,l) in list2:
        if i < k and  k < j and j < l:
            model.no_crossing.add(model.pairing[i,j] + model.pairing[k,l] <= 1)

#Creating the solver
solver = SolverFactory('gurobi_direct')

#Applying the solver
results = solver.solve(model)

#Python Script for printing the solution in the terminal
for (i,j) in list2:
    if model.pairing[i,j] == 1:
        print("Nucleotide %s at position %s is matched to nucleotide %s at position %s" %(index_nucleotide[i], i, index_nucleotide[j], j))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in list2:
        if model.pairing[i,j] == 1:
            output.write("Nucleotide %s at position %s is matched to nucleotide %s at position %s\n\n" %(index_nucleotide[i], i, index_nucleotide[j], j))
    output.close()
