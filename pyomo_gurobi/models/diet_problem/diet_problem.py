# Given a set of foods and a set of nutritions, we wish to minimize the
# cost of the diet while satisfying the lower and upper bounds on
# nutritions value and maximum amount of food.

from pyomo.environ import *

model = AbstractModel("Diet problem")

#Sets and parameters

#set of food items
model.foods = Set()

#set of nutrients
model.nutrients = Set()

#The cost for each food item
model.costs = Param(model.foods, within = NonNegativeReals)

#minimum amount of nutrition
model.min_nutrient=Param(model.nutrients, within = NonNegativeReals)

#maximum amount of nutrition
model.max_nutrient=Param(model.nutrients, within = NonNegativeReals)

#the amount of each food item
model.volumes=Param(model.foods, within = NonNegativeReals)

#the maximum amount of food
model.max_volume=Param(within = NonNegativeReals)

#the value/calories of each nutrition for each food  
model.nutrient_value=Param(model.nutrients, model.foods, within = NonNegativeReals)

#Variables

#variable indicating the necessary amount of each food item 
model.amount=Var(model.foods, within = NonNegativeReals)

#Objective

#Minimizing the cost of the diet
def costRule(model):
    return sum(model.costs[n]*model.amount[n] for n in model.foods)
model.cost=Objective(rule=costRule)

#Constraints

#constraint on the amount of overall food
def volumeRule(model):
    return sum(model.volumes[n]*model.amount[n] for n in model.foods) <= model.max_volume
model.volume = Constraint(rule=volumeRule)

#constraints respecting the lower and upper bound on nutrition value
def nutrientRule(model, n):
    value = sum(model.nutrient_value[n,f]*model.amount[f] for f in model.foods)
    return (model.min_nutrient[n], value, model.max_nutrient[n])
model.nutrientConstraint = Constraint(model.nutrients, rule=nutrientRule)



#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("diet_problem.dat")
results = solver.solve(instance)

for i in instance.foods:
    if value(instance.amount[i]) > 0:
        print("The amount of food %s needed is: %f" %(i,value(instance.amount[i])))
print("The minimum cost of the diet is: %f" %value(instance.cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.foods:
        if value(instance.amount[i]) > 0:
            output.write("The amount of food %s needed is: %f\n\n" %(i,value(instance.amount[i])))
    output.write("The minimum cost of the diet is: %f" %value(instance.cost))
    output.close()
