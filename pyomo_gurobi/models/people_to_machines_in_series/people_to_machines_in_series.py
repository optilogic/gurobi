# A set of six workers and a set of six machines along with the productivity 
# of each worker to each machine are given. We wish to maximize the overall 
# productivity by assigning workers to machines in series.

from pyomo.environ import *

model = AbstractModel("Scheduling people to machines in series")

#Sets and parameters

#set of workers
model.workers = RangeSet(1,6)

#set of machines
model.machines = RangeSet(1,6)

#productivity in pieces per hour when worker i is assgned at machine j
model.productivity = Param(model.workers, model.machines, within = NonNegativeIntegers)

#Variables

#binary variable x which is one if and only if person i is assigned to machine j
model.x = Var(model.workers, model.machines, within = Binary)

#variable bounding the productivities on every machine from below
model.min_prod = Var(within =NonNegativeReals)

#Objective

#maximizing the lower bound on the productivity 
def max_min_prod(model):
    return model.min_prod 
model.max_min_prod = Objective(rule = max_min_prod, sense = maximize)

#Constraints

#For every machine there is exactly one person assigned
def machine2person(model,j):
    return sum(model.x[i,j] for i in model.workers) == 1
model.machine2person = Constraint(model.machines, rule = machine2person)

#For every person there is exactly one machine assigned
def person2machine(model, i):
    return sum(model.x[i,j] for j in model.machines) == 1
model.person2machine = Constraint(model.workers, rule = person2machine)

#if there are more workers than machines, then the last constraint can be replaced by:
#def person2machine(model, i):
#    return sum(model.x_ij[i,j] for j in model.machines) <= 1
#model.person2machine = Constraint(model.workers, rule = person2machine)

#for every person the corresponding productivity should satisfy the lower bound
def lower_bound(model, i):
    return sum(model.productivity[i,j]*model.x[i,j] for j in model.machines) >= model.min_prod
model.lower_bound = Constraint(model.workers, rule = lower_bound)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("people_to_machines_in_series.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.workers:
    for j in instance.machines:
        if value(instance.x[i,j]) > 0:
            print(f'Worker {i} is assigned to machine {j}')
print(f'The minimum productivity of every machine is {value(instance.min_prod)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.workers:
        for j in instance.machines:
            if value(instance.x[i,j]) > 0:
                output.write(f'Worker {i} is assigned to machine {j}\n\n')
    output.write(f'The minimum productivity of every machine is {value(instance.min_prod)}')
    output.close()


