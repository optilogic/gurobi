# A set of cities and their population is given. Also, the distance of every two 
# cities is given. The problem is to determine the location of three tax offices 
# while minimizing the average distance per person to the closest tax office.

from pyomo.environ import *

model = AbstractModel("Location of income tax offices")

#Sets and parameters

#set of all cities
model.cities = Set()

#population of each city
model.population = Param(model.cities, within = NonNegativeIntegers)

#distances between connected cities
model.distances = Param(model.cities, model.cities, within = NonNegativeIntegers)

#number of cities offices should be located
model.number_cities_required = Param(within = PositiveIntegers)

#Variables

#binary variable x which is one if and only if the office will be build in city i
model.x = Var(model.cities, within = Binary)

#binary variable indicating whether city i depends on office in city j
model.depends = Var(model.cities, model.cities, within = Binary)

#Objective

#minimizing the total distance weighted by the number of inhabitants of the cities
def min_dist(model):
    return sum(sum(model.population[i]*model.distances[i,j]*model.depends[i,j] for j in model.cities) for i in model.cities)
model.min_dist = Objective(rule = min_dist)

#Constraints

#a city can depend on office in a city only if the office is open in that city
def indicator_constraint(model, i, j):
    return model.depends[i,j] <= model.x[j]
model.indicator_constraint = Constraint(model.cities, model.cities, rule = indicator_constraint)

#every city depends on a single office
def single_office_dependence(model, i):
    return sum(model.depends[i,j] for j in model.cities) == 1
model.single_office_dependence = Constraint(model.cities, rule = single_office_dependence)

#exactly m offices should open
def number_open_offices(model):
    return sum(model.x[i] for i in model.cities) == model.number_cities_required
model.number_open_offices = Constraint(rule = number_open_offices)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("location_income_tax_offices.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.cities:
    if value(instance.x[i]) > 0:
        print("Office will be build in city %s" %i)
for i in instance.cities:
    for j in instance.cities:
        if value(instance.depends[i,j]) > 0:
            print("City %s depends on office located in office %s" %(i,j))
print("The minimum distance weighted by the number of inhabitants of the cities is: %f" %value(instance.min_dist))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.cities:
        if value(instance.x[i]) > 0:
            output.write("Office will be build in city %s\n\n" %i)
    for i in instance.cities:
        for j in instance.cities:
            if value(instance.depends[i,j]) > 0:
                output.write("City %s depends on office located in office %s\n\n" %(i,j))
    output.write("The minimum distance weighted by the number of inhabitants of the cities is: %f" %value(instance.min_dist))
    output.close()

