# Consider a set of customer, a set of time periods and a set possible
# facility locations. Each facility is assigned a cost for opening and
# there is a cost associated with serving each customer to each possible
# location. In each time period it is decided whether to open new
# facilites so as to comply with the minimum number of customers needed
# to serve at that time period. The objective is to minimize the total
# cost of customer service plus the cost of opening facilites over the
# time horizon, while setting a number of new facilities over a finite
# time horizon so as to cover dynamically the demand of a given set of
# customers.

from pyomo.environ import *

model = AbstractModel('Multi-period-incremental-service-facility-location')

#Sets and Parameters

#Set of customers 
model.I = Set()

#Set of possible locations for facilities
model.J = Set()

#Set of time periods
model.T = Set()

#Minimum number of customers that must be served at period t
model.n_t = Param( model.T, within = NonNegativeIntegers)

#Number of facilities that must be opened at period t
model.p_t = Param( model.T, within = NonNegativeIntegers)

#Arcs: Customer i is served at facility j
model.arcs = Set(within = model.I*model.J)

#Assignment value of allocating customer i to facility j at time period t
model.cost_ij_t = Param(model.I, model.J, model.T, within=NonNegativeReals)

#Total cost of facility j being established at time period t
model.f_j_t = Param(model.J, model.T, within=NonNegativeReals)

#Variables

#binary variable which is 1 if customer i is assigned at facility j at time period t
model.x_ij_t = Var(model.I, model.J, model.T, domain=Binary)

#binary variable which is 1 if facility j is opened at time period t
model.y_j_t = Var(model.J, model.T, domain=Binary)

#Objective

#objective function minimizing the total operation costs.
def min_cost(model):
    x=0
    for i in model.I:
        for j in model.J:
            for t in model.T:
                x += model.cost_ij_t[i,j,t]*model.x_ij_t[i,j,t]
    for j in model.J:
        for t in model.T:
            x += model.f_j_t[j,t]*model.y_j_t[j,t]
    return x        
model.obj = Objective(rule=min_cost)

#Constraints

#Covering constraints that impose that in each time period the minimum required number of customers are being served
def cover_custom(model, t):
    return sum(sum(model.x_ij_t[i,j,t] for i in model.I) for j in model.J) >= model.n_t[t]
model.cover = Constraint(model.T, rule=cover_custom)

#Each customer is assigned to at most one facility in each period.
def assign_custom(model, i ,t):
    return sum(model.x_ij_t[i,j,t] for j in model.J) <= 1
model.assign_custom = Constraint(model.I, model.T, rule=assign_custom)

#Guarantee that once that a customer is served at a given period he/she will be served in any subsequent period.
def conseq_served(model, i, t):
    if t == 1:
        return Constraint.Skip
    else:
        return sum(model.x_ij_t[i,j,t-1] for j in model.J) <= sum(model.x_ij_t[i,j,t] for j in model.J)
model.conseq_served = Constraint(model.I, model.T, rule=conseq_served)

#All customers are served at the end of the planning horizon. The constant 2 corresponds to the cardinality of the set of time horizons.
def all_custom_served(model, i):
    return sum(model.x_ij_t[i,j,2] for j in model.J) == 1
model.all_custom_served = Constraint(model.I, rule=all_custom_served)

#Customers are assigned only to open facilities
def custom_assign_open_facility(model, i, j, t):
    return model.x_ij_t[i,j,t] <= sum(model.y_j_t[j,k] for k in range(1,t+1))
model.custom_assign_open_facility = Constraint(model.I, model.J, model.T, rule=custom_assign_open_facility)

#Exactly p_t facilities are opened in each time period 
def numb_open_facilities_t(model, t):
    return sum(model.y_j_t[j,t] for j in model.J) == model.p_t[t]
model.numb_open_facilities_t = Constraint(model.T, rule=numb_open_facilities_t)

#A facility can be opened, at most, once.
def open_facility_once(model, j):
    return sum(model.y_j_t[j,t] for t in model.T) <= 1
model.open_facility_once = Constraint(model.J, rule = open_facility_once)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("multi_period_incremental_service_facility_location.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.J:
    for j in instance.T:
        if value(instance.y_j_t[i,j]) > 0:
            print(f'Facility {i} will open at time period {j}')
for i in instance.I:
    for j in instance.J:
        for k in instance.T:
            if value(instance.x_ij_t[i,j,k]) > 0:
                print(f'Customer {i} will be served at facility {j} during time period {k}')
print(f'The minimum total operation cost is {value(instance.obj)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.J:
        for j in instance.T:
            if value(instance.y_j_t[i,j]) > 0:
                output.write(f'Facility {i} will open at time period {j}\n\n')
    for i in instance.I:
        for j in instance.J:
            for k in instance.T:
                if value(instance.x_ij_t[i,j,k]) > 0:
                    output.write(f'Customer {i} will be served at facility {j} during time period {k}\n\n')
    output.write(f'The minimum total operation cost is {value(instance.obj)}')
    output.close()
