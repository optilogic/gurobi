# Let's consider a company that produces drinking glasses. It currently sells 
# six different types, that are produced in batches of 1000 glasses, and wishes 
# to plan its production for the next 12 weeks. The demand in thousands for the 
# 12 coming weeks and for every glass type is given. For every glass type the 
# initial stock is given, as well as the required final stock level. Per batch 
# of every glass type, the production and storage costs in $ are given, together 
# with the required working time for workers and machines, and the required storage 
# space. The number of working hours of the personnel is limited to 390 hours per week, 
# and the machines have a weekly capacity of 850 hours. Storage space for up to 1000 
# trays is available. The problem is to determine which quantities of the different glass 
# types need to be produced in every period to minimize the total cost of production and storage.

from pyomo.environ import *

model = AbstractModel("Production of drinking glasses")

#Sets and Parameters

#set of glass types to produce
model.types = Set()

#set of weeks to plan
model.weeks = Set()

#demand of glasses per week
model.demand = Param(model.types, model.weeks, within = NonNegativeIntegers)

#production cost (batch of 1000)
model.prod_cost = Param(model.types, within = NonNegativeIntegers)

#storage cost (batch of 1000)
model.storage_cost = Param(model.types, within = NonNegativeIntegers)

#initial stock per glass type (batch of 1000)
model.initial_stock = Param(model.types, within = NonNegativeIntegers)

#final stock per glass type (batch of 1000)
model.final_stock = Param(model.types, within = NonNegativeIntegers)

#worker time to produce 1000 glasses of each type
model.worker_time = Param(model.types, within = NonNegativeIntegers)

#machine time to produce 1000 glasses of each type
model.machine_time = Param(model.types, within = NonNegativeIntegers)

#storage space in number of trays
model.storage_space = Param(model.types, within = NonNegativeIntegers)

#capacity on worker time per glass type each week
model.worker_cap_week = Param(within = NonNegativeIntegers)

#capacity on machine time per glass type each week
model.machine_cap_week = Param(within = NonNegativeIntegers)

#capacity on storage space time per glass type each week
model.storage_cap_week = Param(within = NonNegativeIntegers)

#Variables

#variable indicating the number of each glass type to be produced during week t
model.prod_g_t = Var(model.types, model.weeks, within = NonNegativeReals)

#variable indicating the number of each glass type to store at the end of week t
model.store_g_t = Var(model.types, model.weeks, within = NonNegativeReals)

#Objective

#minimizing the total cost of production and storage
def min_cost(model):
    return sum(sum(model.prod_cost[g]*model.prod_g_t[g,t] + model.storage_cost[g]*model.store_g_t[g,t] for g in model.types) for t in model.weeks)
model.min_cost = Objective(rule = min_cost)

#Constraints

#inventory balance constraints on each week
def inventory_balance(model, g, t):
    if t == 1:
        return model.store_g_t[g,1] + model.demand[g,1] == model.prod_g_t[g,1] + model.initial_stock[g]
    else:
        return model.store_g_t[g,t] + model.demand[g,t] == model.prod_g_t[g,t] + model.store_g_t[g, t-1]
model.inventory_balance = Constraint(model.types, model.weeks, rule = inventory_balance)

#the storage for each glass type must be at least as the final stock for each glass type
def last_week_store(model, g):
    return model.store_g_t[g,12] >= model.final_stock[g]
model.last_week_store = Constraint(model.types, rule = last_week_store)

#limit on the worker time each week
def worker_time_capacity(model,t):
    return sum(model.worker_time[g]*model.prod_g_t[g,t] for g in model.types) <= model.worker_cap_week
model.worker_time_capacity = Constraint(model.weeks, rule = worker_time_capacity)

#limit on the machine time each week
def machine_time_capacity(model,t):
    return sum(model.machine_time[g]*model.prod_g_t[g,t] for g in model.types) <= model.machine_cap_week
model.machine_time_capacity = Constraint(model.weeks, rule = machine_time_capacity)

#limit on the storage space each week
def storage_space_capacity(model,t):
    return sum(model.storage_space[g]*model.prod_g_t[g,t] for g in model.types)<= model.storage_cap_week
model.storage_space_capacity = Constraint(model.weeks, rule = storage_space_capacity)



solver = SolverFactory('gurobi_direct')
instance = model.create_instance("drinking_glasses_production.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.types:
    for j in instance.weeks:
        if value(instance.prod_g_t[i,j]) > 0:
            print(f'{value(instance.prod_g_t[i,j])} glasses of type {i} need to be produced on week {j}')
for i in instance.types:
    for j in instance.weeks:
        if value(instance.store_g_t[i,j]) > 0:
            print(f'{value(instance.store_g_t[i,j])} glasses of type {i} need to be stored on week {j}')
print(f'The minimum total cost of production and storage is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.types:
        for j in instance.weeks:
            if value(instance.prod_g_t[i,j]) > 0:
                output.write(f'{value(instance.prod_g_t[i,j])} glasses of type {i} need to be produced on week {j}\n\n')
    for i in instance.types:
        for j in instance.weeks:
            if value(instance.store_g_t[i,j]) > 0:
                output.write(f'{value(instance.store_g_t[i,j])} glasses of type {i} need to be stored on week {j}\n\n')
    output.write(f'The minimum total cost of production and storage is {value(instance.min_cost)}')
    output.close()
