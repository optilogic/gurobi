# This model packs a set of rectangles without rotation or overlap
# within a strip of a given width, minimizing the length of the
# strip. Since, the formulation as a Linear Program is designed as a
# concrete model, the data is displayed within the .py file where the
# model is implemented.

from pyomo.environ import *
from pyomo.gdp import *

model = ConcreteModel("Strip Packing")

model.RECTANGLES = Set(ordered=True, initialize=[0, 1, 2, 3])

# Width and Length of each rectangle
model.Width = Param(model.RECTANGLES, initialize={0: 6, 1: 3, 2: 4, 3: 2})
model.Length = Param(model.RECTANGLES, initialize={0: 6, 1: 8, 2: 5, 3: 3})

#Width of strip
model.StripWidth = Param(initialize=10)

# Upperbound on length (default is sum of lengths of rectangles)
model.LengthUB = Param(initialize=sum(model.Length[i] for i in model.RECTANGLES))

# Generate the list of possible rectangle conflicts (which are any pair)
def rec_pairs_filter(model, i, j):
    return i < j
model.OVERLAP_PAIRS = Set(initialize=model.RECTANGLES*model.RECTANGLES, dimen=2, filter=rec_pairs_filter)

#Variables

# x (length) coordinate of each of the rectangles
model.x = Var(model.RECTANGLES, bounds=(0, model.LengthUB))

# y (width) coordinate of each of the rectangles
def w_bounds(model, i):
    return (0, model.StripWidth-model.Width[i])
model.y = Var(model.RECTANGLES, bounds=w_bounds)

# Length of strip
model.MaxLength = Var(within=NonNegativeReals)

#Objective

# Minimize length
model.total_length = Objective(expr=model.MaxLength)

#Constraints

# Strip length constraint
@model.Constraint(model.RECTANGLES)
def strip_ends_after_last_rec(model, i):
    return model.MaxLength >= model.x[i] + model.Length[i]

# Insert the no-overlap disjunctions here
@model.Disjunction(model.OVERLAP_PAIRS)
def noOverlap(model, i, j):
    return [
        model.x[i] + model.Length[i] <= model.x[j],
        model.x[j] + model.Length[j] <= model.x[i],
        model.y[i] + model.Width[i] <= model.y[j],
        model.y[j] + model.Width[j] <= model.y[i],
    ]

TransformationFactory('gdp.chull').apply_to(model)

# Solve and print the solution
SolverFactory('gurobi_direct').solve(model)

for i in model.RECTANGLES:
    print("Rectangle %s: (%s, %s)" % (i, value(model.x[i]), value(model.y[i])))
print("The minimum total length is: %d" %value(model.total_length))


output = open("results.txt", 'w')
for i in model.RECTANGLES:
    output.write("Rectangle %s: (%s, %s)\n\n" % (i, value(model.x[i]), value(model.y[i])))
output.write("The minimum total length is: %d" %value(model.total_length))
output.close()
