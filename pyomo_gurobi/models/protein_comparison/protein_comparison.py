# In this problem from molecular biology it is wished to measure the
# similarities of two protein sequences. In this case, the two proteins
# are seen as two undirected graphs, where nodes in each graph represent
# the acids in the respecitve protein sequences and edges of a graph
# mirror pair of matched acids in a sequence. From the graph theory
# point of view the problem can be seen as finding the largest
# isomorphic subgraphs in each graph. Since there should not be
# crossovers in each sequence, the order of acids in each sequence
# matters. Below are given the constraints needed to be formulated
# mathematically.

# Each acid in the first (second) sequence must be mapped to at
# most one other acid in the second (first) sequence.
# If two acids from two different sequences are not mapped
# together, then any two edges in the respective sequences containing
# the corresponding acids cannot be mapped together.
# There can be no crossing of two pairs of acids with endpoints on
# two different sequences.


from pyomo.environ import *

model = AbstractModel("Protein comparison")

#Sets and parameters

#set of acids in the first protein
model.acids1 = Set()

#set of acids in the second protein
model.acids2 = Set()

#set of edges between acids in the first protein sequence
model.edges1 = Set(within = model.acids1*model.acids1)

#set of edges between acids in the second protein sequence
model.edges2 = Set(within = model.acids2*model.acids2)

#set of pairs of edges with one element from each protein
def pair_edges(model):
    return ((i,j,k,l) for i in model.acids1 for j in model.acids2 for k in model.acids1 for l in model.acids2 if ((i,k) in model.edges1 and (j,l) in model.edges2))
model.pair_edges = Set(dimen = 4, initialize = pair_edges) 

#Variables

#binary variable indicating whether an acid in the first sequence is paired with an acid in the second sequence
model.acids_in_diff_seq = Var(model.acids1, model.acids2, within = Binary)

#binary variable indicating whether an edge in the first sequence is paired with an edge in the second sequence
model.edges_in_diff_seq = Var(model.pair_edges, within = Binary)

#Objective

#maximizing the number of the paired edges from two different sequences
def max_paired_edges(model):
    return sum(sum(model.edges_in_diff_seq[i,j,k,l] for (i,k) in model.edges1) for (j,l) in model.edges2)
model.max_paired_edges = Objective(rule = max_paired_edges, sense = maximize)

#Constraints

#no acid in the first sequence can be paired with more than one acid in the second sequence
def degree_at_most_1_1(model, j):
    return sum(model.acids_in_diff_seq[i,j] for i in model.acids1) <= 1
model.degree_at_most_1_1 = Constraint(model.acids2, rule = degree_at_most_1_1)

#no acid in the second sequence can be paired with more than one acid in the first sequence
def degree_at_most_1_2(model, i):
    return sum(model.acids_in_diff_seq[i,j] for j in model.acids2) <= 1
model.degree_at_most_1_2 = Constraint(model.acids1, rule = degree_at_most_1_2)

#if two edges from different sequences are paired then so are their respective acids
def imply1(model, i, j, k, l):
    return model.edges_in_diff_seq[i,j,k,l] <= model.acids_in_diff_seq[i,j] 
model.imply1 = Constraint(model.pair_edges, rule = imply1)

#if two edges from different sequences are paired then so are their respective acids
def imply2(model, i, j, k, l):
    return model.edges_in_diff_seq[i,j,k,l] <= model.acids_in_diff_seq[k,l] 
model.imply2 = Constraint(model.pair_edges, rule = imply2)

#no crossover allowed if the order of acids is not preserved
def no_crossover(model, i, j, k, l):
    if i < k and  l < j:
        return model.acids_in_diff_seq[i,j] + model.acids_in_diff_seq[k,l] <= 1
    else:
        return Constraint.Skip
model.no_crossover = Constraint(model.acids1, model.acids2, model.acids1, model.acids2, rule = no_crossover)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("protein_comparison.dat")
results = solver.solve(instance)

print("The similarity mapping between two protein sequences is: ")
for i in instance.acids1:
    for j in instance.acids2:
        if value(instance.acids_in_diff_seq[i,j]) > 0:
            print("Amino acid %s matches amino acid %s" %(i,j))
for (i,j,k,l) in instance.pair_edges:
    if value(instance.edges_in_diff_seq[i,j,k,l]) > 0:
        print("Edge %s-%s in the first sequence matches edge %s-%s in the second sequence" %(i,k,j,l))
print("The maximum number of paired edges between two protein sequences is: %d" %value(instance.max_paired_edges))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("The similarity mapping between two protein sequences is: \n\n")
    for i in instance.acids1:
        for j in instance.acids2:
            if value(instance.acids_in_diff_seq[i,j]) > 0:
                output.write("Amino acid %s matches amino acid %s \n\n" %(i,j))
    for (i,j,k,l) in instance.pair_edges:
        if value(instance.edges_in_diff_seq[i,j,k,l]) > 0:
            output.write("Edge %s-%s in the first sequence matches edge %s-%s in the second sequence \n\n" %(i,k,j,l))
    output.write("The maximum number of paired edges between two protein sequences is: %d" %value(instance.max_paired_edges))
    output.close()
