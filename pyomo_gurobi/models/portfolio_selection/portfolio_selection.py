# Suppose there are 6 different shares available for anybody who wants to 
# invest on them for the next 6 months. The shares are of two different 
# categories; three shares are in technology while the other three are 
# non-technological. Also, each share belongs to a certain country 
# (European and non-European) and has an expected return of investment given. 
# Suppose a wealthy person would like to invest $100.000 and for each share 
# the amount to be invested must fall within a given range. Furthermore, the 
# person would like to invest half of the investment in countries of his favorite 
# continent (Europe in this case) and no more than 30% of the capital in technology. 
# This problem can be modelled as a Linear Program whose solution provides the optimal 
# way to choose different number of shares maximizing the total expected return of 
# investment on the top of 6 months time period.

from pyomo.environ import *

model = AbstractModel("Portfolio selection")

#Sets and parameters

#set of shares
model.shares = Set()

#the amount of money to be invested
model.capital = Param(within = NonNegativeReals)

#set of technology type investments
model.tech = Set()

#set of shares belonging to Europe
model.eu_shares = Set()

#expected return of investment per share
model.roi_s = Param(model.shares, within = NonNegativeReals)

#minimum amount of money to be invested in each share
model.min_inv = Param(within = NonNegativeReals)

#maximum amount of money to be invested in each share
model.max_inv = Param(within = NonNegativeReals)

#Variables

#variable indicating the amount of money to be invested in share s
model.invest_s = Var(model.shares, within = NonNegativeReals)

#Objective

#maximizing the return of investments over all shares
def max_roi(model):
    return sum(model.roi_s[s]*model.invest_s[s] for s in model.shares)
model.max_roi = Objective(rule = max_roi, sense = maximize)

#Constraints

#lower bound on the investment of each share
def lower_bound(model, s):
    return model.min_inv <= model.invest_s[s]
model.lower_bound = Constraint(model.shares, rule = lower_bound)

# upper bound on the investment of each share
def upper_bound(model, s):
    return model.max_inv >= model.invest_s[s]
model.upper_bound = Constraint(model.shares, rule = upper_bound)

#all the capital will be invested
def capital_invested(model):
    return sum(model.invest_s[s] for s in model.shares) == model.capital
model.capital_invested = Constraint( rule = capital_invested)

#limit on investment on technology
def tech_invest_limit(model):
    return sum(model.invest_s[s] for s in model.tech) <= 0.3*model.capital
model.tech_invest_limit = Constraint(rule = tech_invest_limit)

#limit on investment on europian based shares
def eu_based_shares_invest(model):
    return sum(model.invest_s[s] for s in model.eu_shares) >= 0.5*model.capital
model.eu_based_shares_invest = Constraint(rule = eu_based_shares_invest)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("portfolio_selection.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.shares:
        if value(instance.invest_s[i]) > 0:
            print(f'The amount of money to be invested in share {i} is {value(instance.invest_s[i])}')
print(f'The maximum return of investments over all shares is {value(instance.max_roi)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.shares:
        if value(instance.invest_s[i]) > 0:
            output.write(f'The amount of money to be invested in share {i} is {value(instance.invest_s[i])}\n\n')
    output.write(f'The maximum return of investments over all shares is {value(instance.max_roi)}')
    output.close()
