from pyomo.environ import *
import numpy as np
import matplotlib.pyplot as plt

model = AbstractModel('MEIO-2')

# Sets and Parameters

# Sets

# set of nodes
model.nodes = Set()

# set of demand nodes
model.demand_nodes = Set(within=model.nodes)

# set of arcs
model.arcs = Set(within=model.nodes * model.nodes)

# Parameters

# lead times for each node
model.lead_times = Param(model.nodes, within=NonNegativeReals)

# maximum replenishment time for each node
model.max_replenishment_time = Param(model.nodes, within=NonNegativeReals)

# parameter indicating the service level
model.k = Param(within=NonNegativeReals)

# average demand for each node
model.average_demand = Param(model.nodes, within=NonNegativeReals)

# standard deviation of the demand for each node
model.standard_deviation = Param(model.nodes, within=NonNegativeReals)

# service time at demand nodes which is guaranteed to the customers
model.guaranteed_service_time = Param(
    model.demand_nodes, within=NonNegativeReals)

# cost of unit inventory holding for each node
model.unit_inventory_holding_cost = Param(model.nodes, within=NonNegativeReals)

# Variables

# variable representing the service time quoted by each node
model.service_time = Var(model.nodes, within=NonNegativeReals)

# variable representing the inbound service time for each node
model.inbound_service_time = Var(model.nodes, within=NonNegativeReals)


# Obtaining piecewise linear approximation of the safety stock level

inst = model.create_instance('meio1.dat')


def safety_stock(x, j):
    return inst.k * inst.standard_deviation[j] * np.sqrt(x)


# constructing the breakpoints for each node and calculating the safety stock for each breakpoint
breakpoints = {}
dictionary1 = {}

for j in inst.nodes:
    breakpoints[j] = []
    dictionary1[j] = []
    x = np.arange(1, inst.max_replenishment_time[j] + 1, 1)
    for i in x:
        breakpoints[j].append(i)
        dictionary1[j].append((i, safety_stock(i, j)))
        

# calculating the slope and y-intercept of each line for each node
dictionary2 = {}
for j in inst.nodes:
    dictionary2[j] = [dictionary1[j][0][1] / dictionary1[j][0][0]]
    for i in range(1, len(dictionary1[j])):
        dictionary2[j].append((dictionary1[j][i][1] - dictionary1[j][i - 1]
                               [1]) / (dictionary1[j][i][0] - dictionary1[j][i - 1][0]))


pairs = [(i, j) for i in inst.nodes for j in breakpoints[i]]
model.b = Var(pairs, within=Binary)
model.x = Var(pairs, within=NonNegativeReals)



# Objective
def min_cost(model):
    return sum(model.unit_inventory_holding_cost[i] * (sum(dictionary2[i][j-1] * model.b[i, j] for j in breakpoints[i])) for i in model.nodes)
model.objective = Objective(rule=min_cost)


# Constraints

# lower bound on the variable x on each linear piece
def lb(model, n, j):
    if j == breakpoints[n][0] and (n,j+1) in pairs:
        return model.x[n, 1] >= dictionary1[n][0][0] * model.b[n, 2]
    elif len(breakpoints[n]) > 1 and j in range(breakpoints[n][1], breakpoints[n][-1]):
        return model.x[n, j] >= (dictionary1[n][j-1][0] - dictionary1[n][j-2][0]) * model.b[n, j + 1]
    else:
        return Constraint.Skip
model.lb = Constraint(pairs, rule=lb)


# upper bound on the variable x on each linear piece
def ub(model, n, j):
    if j == breakpoints[n][0]:
        return model.x[n, 1] <= dictionary1[n][0][0] * model.b[n, 1]
    elif j in range(breakpoints[n][1], breakpoints[n][-1]):
        return model.x[n, j] <= (dictionary1[n][j-1][0] - dictionary1[n][j-2][0]) * model.b[n, j]
    else:
        return model.x[n, breakpoints[n][-1]] <= (dictionary1[n][breakpoints[n][-2]][0] - dictionary1[n][breakpoints[n][-3]][0]) * model.b[n, breakpoints[n][-1]]
model.ub = Constraint(pairs, rule=ub)


# cummulative lead time constraints
def at_most_one_piece(model, n, j):
    if j != breakpoints[n][-1]:
        return model.b[n, j+1] <= model.b[n, j]
    else:
        return Constraint.Skip
model.at_most_one_piece = Constraint(pairs, rule=at_most_one_piece)


# the net replenishment lead time at each node is equal to the sum of variables x over all linear pieces for that node
def NRLT_equal_to_sum_x(model, n):
    return sum(model.x[n, p] for p in breakpoints[n] if (n, p) in pairs) == model.inbound_service_time[n] + model.lead_times[n] - model.service_time[n]
model.NRLT_equal_to_sum_x = Constraint(model.nodes, rule=NRLT_equal_to_sum_x)

# the inbound service time of each node j is the maximum of all service time of the nodes i supplying j
def inbound_service_time_bounds(model, i, j):
    if (i, j) in model.arcs:
        return model.inbound_service_time[j] >= model.service_time[i]
    else:
        return Constraint.Skip
model.inbound_service_time_bounds = Constraint(model.nodes, model.nodes, rule=inbound_service_time_bounds)

# upper bound on the service time at the demand nodes
def service_time_ub_demand_nodes(model, d):
    return model.service_time[d] <= model.guaranteed_service_time[d]
model.service_time_ub_demand_nodes = Constraint(model.demand_nodes, rule=service_time_ub_demand_nodes)

# net replenishment lead time must be nonnegative
def nonnegative_nrlt(model, i):
    return model.inbound_service_time[i] + model.lead_times[i] >= model.service_time[i]
model.nonnegative_nrlt = Constraint(model.nodes, rule=nonnegative_nrlt)


# Creating and solving an instance

solver = SolverFactory('gurobi_direct')
instance1 = model.create_instance('meio1.dat')
results = solver.solve(instance1)



for i in instance1.nodes:
    if value(instance1.service_time[i]) > 0:
        print("Service time for node %s is: %d" %
              (i, value(instance1.service_time[i])))
for i in instance1.nodes:
    if value(instance1.inbound_service_time[i]) > 0:
        print("Inbound service time for node %s is: %d" %
              (i, value(instance1.inbound_service_time[i])))
for i in instance1.nodes:
    print("Net replenishment lead time for node %s is %f" % (
        i, instance1.lead_times[i] + value(instance1.inbound_service_time[i]) - value(instance1.service_time[i])))
print("The minimum holding inventory cost is: %f" % value(instance1.objective))

# Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance1.nodes:
        if value(instance1.service_time[i]) > 0:
            output.write("Service time for node %s is: %d \n" %(i,value(instance1.service_time[i])))
    for i in instance1.nodes:
        if value(instance1.inbound_service_time[i]) > 0:
            output.write("Inbound service time for node %s is: %d \n" %(i,value(instance1.inbound_service_time[i])))
    for i in instance1.nodes:
        output.write("Net replenishment lead time for node %s is %f \n" %(i, instance1.lead_times[i] + value(instance1.inbound_service_time[i]) - value(instance1.service_time[i])))
    output.write("The minimum holding inventory cost is: %f" %value(instance1.objective))
    output.close()
