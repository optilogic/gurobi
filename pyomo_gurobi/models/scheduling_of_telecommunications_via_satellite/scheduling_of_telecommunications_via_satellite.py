# A digital telecommunications system via satellite consists of a satellite 
# and a set of stations on earth which serve as interfaces with the terrestrial network. 
# Consider the transmissions from four transmitter stations in the US to four receiver 
# stations in Europe. The quantity of data transmitted from a station to another station is given. 
# The quantity is expressed in seconds of transmission duration, because all lines have the same 
# constant transmission rate. The problem is to find a schedule with minimal total duration.

from pyomo.environ import *

model = AbstractModel("Scheduling of telecommunications via satellite")

#Sets and parameters

#set of transmitter stations
model.transmit_stations = Set()

#set of receiver stations
model.receiv_stations = Set()

#set of modes
model.modes = Set()

#set of arcs between stations
def arcs(model):
    return ((i,j) for i in model.transmit_stations for j in model.transmit_stations)
model.arcs = Set(dimen = 2, initialize = arcs)

#quantity of data transmitted from a station to another station
model.quantity = Param(model.arcs, within = NonNegativeIntegers)

#Variables

#variable representing the duration of every mode
model.duration = Var(model.modes, within = NonNegativeReals)

#binary variable indicating whether quantity[t,r] is transmitted through mode m
model.flow = Var(model.transmit_stations, model.receiv_stations, model.modes, within = Binary)

#Objective

#minimizing the overall time duration over all modes
def min_dur(model):
    return sum(model.duration[m] for m in model.modes)
model.min_dur = Objective(rule = min_dur)

#Constraints

#bound every quantity[t,r] transmitted in mode m by duration of m
def quantity_limit(model, t, r, m):
    return model.quantity[t,r]*model.flow[t,r,m] <= model.duration[m]
model.quantity_limit = Constraint(model.transmit_stations, model.receiv_stations, model.modes, rule = quantity_limit)

#every quantity[t,r] must be transmitted in a single mode
def single_mode(model, t, r):
    return sum(model.flow[t,r,m] for m in model.modes) == 1
model.single_mode = Constraint(model.transmit_stations, model.receiv_stations, rule = single_mode)

#every mode is valid, with a single transmitter station
def single_transmit(model, r, m):
    return sum(model.flow[t,r,m] for t in model.transmit_stations) == 1
model.single_transmit = Constraint(model.receiv_stations, model.modes, rule = single_transmit)

#every mode is valid, with a single receiver station
def single_receiv(model, t, m):
    return sum(model.flow[t,r,m] for r in model.transmit_stations) == 1
model.single_receiv = Constraint(model.transmit_stations, model.modes, rule = single_receiv)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("scheduling_of_telecommunications_via_satellite.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.modes:
    if value(instance.duration[i]) > 0:
        print(f'The duration of largest element of mode {i} is {value(instance.duration[i])}')
for i in instance.transmit_stations:
    for j in instance.receiv_stations:
        for k in instance.modes:
            if value(instance.flow[i,j,k]) > 0:
                print(f'Quantity {value(instance.quantity[i,j])} is transmitted through mode {k}')
print(f'The minimum of the total time duration over all modes is {value(instance.min_dur)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.modes:
        if value(instance.duration[i]) > 0:
            output.write(f'The duration of largest element of mode {i} is {value(instance.duration[i])}\n\n')
    for i in instance.transmit_stations:
        for j in instance.receiv_stations:
            for k in instance.modes:
                if value(instance.flow[i,j,k]) > 0:
                    output.write(f'Quantity {value(instance.quantity[i,j])} is transmitted through mode {k}\n\n')
    output.write(f'The minimum of the total time duration over all modes is {value(instance.min_dur)}')
    output.close()
