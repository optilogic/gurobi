# A large company wishes to move some of its departments out of
# Detroit. This will derive in cost reduction. The possible locations
# are Chicago, Toronto and Detroit. There are $5$ departments considered
# to relocate. Every two departments need to communicate a certain
# number of communication units, which is given in the data file. Also,
# there is a cost per unit of communication between every two
# locations. For each department, there is a benefit if it will be
# relocated to a location.

from pyomo.environ import *

model = AbstractModel("Decentralization")

#Sets and parameters

#set of all departments
model.departments = Set()

#set of all possible locations
model.all_loc = Set()

#benefits for moving a department in one of the locations off of the headquarters city
model.benefits = Param(model.departments, model.all_loc, within = NonNegativeReals) 

#set of arcs between departments
def arcs(model):
    return((i,j) for i in model.departments for j in model.departments if i < j)
model.arcs = Set(dimen = 2, initialize = arcs)

#quantities of communication between departments
model.quant_comm = Param(model.arcs, within = NonNegativeReals)

#set of arcs between locations
def arcs_locs(model):
    return ((i,j) for i in model.all_loc for j in model.all_loc)
model.arcs_locs = Set(dimen = 2, initialize = arcs_locs)

#costs per unit of communication between locations
model.cost_comm = Param(model.arcs_locs, within = NonNegativeReals)

#Variables

#binary variables indicating whether a department is located in a location
model.dep_city = Var(model.departments, model.all_loc, within = Binary)

#binary variables
def function2(model):
    return ((i,j,k,l) for i in model.departments for j in model.all_loc for k in model.departments for l in model.all_loc if ((i,k) in model.arcs and model.quant_comm[i,k] != 0))
model.quad = Set(dimen = 4, initialize = function2)
model.gamma = Var(model.quad, within = Binary)

#Objective

#maximizing the net yearly benefit
def max_benefit(model):
    return sum(sum(model.benefits[i,j]*model.dep_city[i,j] for i in model.departments) for j in model.all_loc) - sum(model.quant_comm[i,k]*model.cost_comm[j,l]*model.gamma[i,j,k,l] for i in model.departments for j in model.all_loc for k in model.departments for l in model.all_loc if (i,j,k,l) in model.quad)
model.max_benefit = Objective(rule = max_benefit, sense = maximize)

#Constraints

#each department must be located in exactly one city
def each_dep_one_city(model, i):
    return sum(model.dep_city[i,j] for j in model.all_loc) == 1
model.each_dep_one_city = Constraint(model.departments, rule = each_dep_one_city)

#implication constraints
def con(model, i, j, k, l):
    return model.gamma[i,j,k,l] <= model.dep_city[i,j]
model.con = Constraint(model.quad, rule = con)

#implication constraints
def con2(model, i, j, k, l):
    return model.gamma[i,j,k,l] <= model.dep_city[k,l]
model.con2 = Constraint(model.quad, rule = con2)

#implication constraints
def con3(model, i, j, k, l):
    return model.dep_city[i,j] + model.dep_city[k,l] - model.gamma[i,j,k,l] <= 1
model.con3 = Constraint(model.quad, rule = con3)

#each city cannot locate more than 3 departments
def con4(model, j):
    return sum(model.dep_city[i,j] for i in model.departments) <= 3
model.con4 = Constraint(model.all_loc, rule = con4)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("decentralization.dat")
results = solver.solve(instance)
print("Departments' optimal locations: ")
for i in instance.departments:
    for j in instance.all_loc:
        if value(instance.dep_city[i,j]) > 0:
            print("Department %s will be located in location %s" %(i,j))
print("The net yearly benefit is %f" %value(instance.max_benefit))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("Departments' optimal locations: \n\n ")
    for i in instance.departments:
        for j in instance.all_loc:
            if value(instance.dep_city[i,j]) > 0:
                output.write("Department %s will be located in location %s \n\n" %(i,j))
    output.write("The net yearly benefit is %f" %value(instance.max_benefit))
    output.close()
