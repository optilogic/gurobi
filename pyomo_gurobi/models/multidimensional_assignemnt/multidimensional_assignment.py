# Given a set of jobs, a set of people, a set of time periods and a cost
# for assigning a person to a job at a time period, we wish to assign
# people to jobs while minimizing the total cost.

from pyomo.environ import *

model = AbstractModel("Multidimensional-assignment")

#Sets and parameters

#set of jobs
model.jobs = Set()

#set of people
model.people = Set()

#set of time periods
model.time_periods = Set()

#cost of assigning a person to a job at an interval
model.cost = Param(model.jobs, model.people, model.time_periods, within = NonNegativeReals)

#Variables

#binary variable indicating whether a person is assigned at a job at a certain time period
model.assign = Var(model.jobs, model.people, model.time_periods, within = Binary)

#Objective

#minimizing the total cost of assigning people to jobs at all time periods
def min_cost(model):
    return sum(sum(sum(model.cost[i,j,t]*model.assign[i,j,t] for t in model.time_periods) for j in model.people) for i in model.jobs)
model.min_cost = Objective(rule = min_cost)

#Constraints

#each job must be assigned to only one person at one time period
def unique_pers_time(model, i):
    return sum(sum(model.assign[i,j,t] for j in model.people) for t in model.time_periods) == 1
model.unique_pers_time = Constraint(model.jobs, rule = unique_pers_time)

#each person must be assigned to only one job at one time period
def unique_job_time(model, j):
    return sum(sum(model.assign[i,j,t] for i in model.jobs) for t in model.time_periods) == 1
model.unique_job_time = Constraint(model.people, rule = unique_job_time)

#for each time period there must be exactly one person to each job
def unique_pers_job(model, t):
    return sum(sum(model.assign[i,j,t] for i in model.jobs) for j in model.people) == 1
model.unique_pers_job = Constraint(model.time_periods, rule = unique_pers_job)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("multidimensional_assignment.dat")
results = solver.solve(instance)

for i in instance.jobs:
    for j in instance.people:
        for t in instance.time_periods:
            if value(instance.assign[i,j,t]) > 0:
                print("Person %s is assigned to job %s at time period %s" %(j,i,t))
print("The minimum cost of all assignments is: %f" %value(instance.min_cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.jobs:
        for j in instance.people:
            for t in instance.time_periods:
                if value(instance.assign[i,j,t]) > 0:
                    output.write("Person %s is assigned to job %s at time period %s\n\n" %(j,i,t))
    output.write("The minimum cost of all assignments is: %f" %value(instance.min_cost))
    output.close()



