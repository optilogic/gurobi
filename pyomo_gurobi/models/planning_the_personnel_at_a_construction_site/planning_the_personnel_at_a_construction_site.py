# The requirements for steel erectors for each month over a period of six 
# months are given. Transfers from other sites to this one are possible on 
# the first day of every month and cost $100 per person. At the end of every 
# month workers may leave to other sites at a transfer cost of $160 per person. 
# It is estimated that understaffing as well as overstaffing cost $200 per 
# month per post. While satisfying several constraints on arrival and leaving 
# of workers it is wished to determine the number of arrivals, leavings each 
# month by minimizing the total cost.

from pyomo.environ import *

model = AbstractModel("Planning the personnel at a construction site")

#Sets and parameters

#set of months
model.months = Set()

#requirements for each month
model.requirements = Param(model.months, within = NonNegativeReals)

#cost per person of transferring to this site at the beginning of each month
model.cost_begin = Param(within = NonNegativeReals)

#cost per person of transferring to other sites at the end of each month
model.cost_end = Param(within = NonNegativeReals)

#cost of undestaffing
model.cost_under = Param(within = NonNegativeReals)

#cost of overstaffing
model.cost_over = Param(within = NonNegativeReals)

#number of workers on-site at the beginning of the planning period
model.workers_begin = Param(within = NonNegativeReals)

#number of workers on-site at the end of the planning period
model.workers_end = Param(within = NonNegativeReals)

#Variables

#number of steel erectors present
model.onsite = Var(model.months, within = NonNegativeIntegers)

#number of workers transferred at the beginning of each month
model.arrive = Var(model.months, within = NonNegativeIntegers)

#number of workers leaving at the end of each month
model.leave = Var(model.months, within = NonNegativeIntegers)

#number of persons more than the required personnel
model.over = Var(model.months, within = NonNegativeIntegers)

#number of persons less than the required personnel
model.under = Var(model.months, within = NonNegativeIntegers)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(model.cost_begin*model.arrive[m] + model.cost_end*model.leave[m] + model.cost_under*model.under[m] + model.cost_over*model.over[m] for m in model.months) + model.workers_begin*model.cost_begin + model.workers_end*model.cost_end
model.min_cost = Objective(rule = min_cost) 

#Constraints

#For the first month, the total staff equals the initial staff plus the arrivals at the beginning of the month
def first_month_staff(model):
    return model.onsite[1] == model.workers_begin + model.arrive[1]
model.first_month_staff = Constraint(rule = first_month_staff)

#After the last month, the remaining personnel onsite equals the personnel present in the last month minus the departures at the end of this month
def after_last_month_staff(model):
    return model.workers_end == model.onsite[6] - model.leave[6]
model.after_last_month_staff = Constraint(rule = after_last_month_staff)

#For the intermediate months, the personnel on-site, the arrivals and departures are linked by the following relation
def balance(model, m):
    if m != 1 and m != 6:
        return model.onsite[m] == model.onsite[m-1] - model.leave[m-1] + model.arrive[m]
    else:
        return Constraint.Skip
model.balance = Constraint(model.months, rule = balance)

#relation between the required number and the actual number of personnel
def over_under_contribution(model, m):
    return model.onsite[m] - model.over[m] + model.under[m] == model.requirements[m] 
model.over_under_contribution = Constraint(model.months, rule = over_under_contribution)

#limit on the workers worked overtime
def limit_hours(model, m):
    return 4*model.under[m] <= model.onsite[m]
model.limit_hours = Constraint(model.months, rule = limit_hours)

#limits the number of persons leaving the construction site each month
def limit_numb_leave(model, m):
    return 3*model.leave[m] <= model.onsite[m] 
model.limit_numb_leave = Constraint(model.months, rule = limit_numb_leave)

#upper bound on the number of arrivals
def ub_arrive(model, m):
	return model.arrive[m] <= 3
model.ub_arrive = Constraint(model.months, rule = ub_arrive)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("planning_the_personnel_at_a_construction_site.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.months:
    if value(instance.onsite[i]) > 0:
        print(f'Number of onsite personnel at the beginning of month {i} is {value(instance.onsite[i])}')
for i in instance.months:
    if value(instance.arrive[i]) > 0:
        print(f'Number of personnel arriving at the beginning of month {i} is {value(instance.arrive[i])}')
for i in instance.months:
    if value(instance.leave[i]) > 0:
        print(f'Number of personnel leaving at the beginning of month {i} is {value(instance.leave[i])}')
for i in instance.months:
    if value(instance.over[i]) > 0:
        print(f'Number of personnel over the required number at the beginning of month {i} is {value(instance.over[i])}')
for i in instance.months:
    if value(instance.under[i]) > 0:
        print(f'Number of personnel under the number required at the beginning of month {i} is {value(instance.under[i])}')     
print(f'The minimum total cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.months:
        if value(instance.onsite[i]) > 0:
            output.write(f'Number of onsite personnel at the beginning of month {i} is {value(instance.onsite[i])}\n\n')
    for i in instance.months:
        if value(instance.arrive[i]) > 0:
            output.write(f'Number of personnel arriving at the beginning of month {i} is {value(instance.arrive[i])}\n\n')
    for i in instance.months:
        if value(instance.leave[i]) > 0:
            output.write(f'Number of personnel leaving at the beginning of month {i} is {value(instance.leave[i])}\n\n')
    for i in instance.months:
        if value(instance.over[i]) > 0:
            output.write(f'Number of personnel over the required number at the beginning of month {i} is {value(instance.over[i])}\n\n')
    for i in instance.months:
        if value(instance.under[i]) > 0:
            output.write(f'Number of personnel under the number required at the beginning of month {i} is {value(instance.under[i])}\n\n')     
    output.write(f'The minimum total cost is {value(instance.min_cost)}')
    output.close()
