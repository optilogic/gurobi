# A set of teachers and subjects for two classes of a college are given. Also, a set 
# of time slots available throughout the week is given. While satisfying several 
# restrictions on preferences that teachers have on the schedule, we wish to assign 
# teachers to classes on each time slot by minimizing the holes in the timetable.

from pyomo.environ import *

model = AbstractModel("Establishing a college timetable")

#Sets and parameters

#set of weekdays
model.weekdays = RangeSet(1,5)

#set of teachers
model.teachers = RangeSet(1,9)

#set of classes
model.classes = RangeSet(1,2)

#number of two-hours lessons per week for each teacher in each class
model.courses = Param(model.teachers, model.classes, within = NonNegativeIntegers)

#set of time slots per week
model.slots = RangeSet(1,20)

#number of slots per day
model.numb_slots_day = Param(within = NonNegativeIntegers)

#Variables

#binary variable indicating whether a teacher gives a lesson to a class at a time slot
model.teach = Var(model.teachers, model.classes, model.slots, within = Binary)

#Objective

#minimizing the sum of courses taught during slots 1 and 4 of every day
def min_penalties(model):
    return sum(sum(sum(model.teach[t, c, d*model.numb_slots_day + 1] + model.teach[t, c, d*model.numb_slots_day + 4] for d in range(5)) for c in model.classes) for t in model.teachers)
model.min_penalties = Objective(rule = min_penalties)

#Constraints

#all lessons taught by the teacher t to class c must be scheduled
def sched_lessons_tc(model, t, c):
    return sum(model.teach[t,c,l] for l in model.slots) == model.courses[t,c]
model.sched_lessons_tc = Constraint(model.teachers, model.classes, rule = sched_lessons_tc)

#a class has at most one course at any time
def no_two_simultan_courses_in_class(model, c, l):
    return sum(model.teach[t,c,l] for t in model.teachers) <= 1
model.no_two_simultan_courses_in_class = Constraint(model.classes, model.slots, rule = no_two_simultan_courses_in_class)

#a teacher must not teach more than one course at a time
def no_two_simultan_courses_per_teacher(model, t, l):
    return sum(model.teach[t,c,l] for c in model.classes) <= 1
model.no_two_simultan_courses_per_teacher = Constraint(model.teachers, model.slots, rule = no_two_simultan_courses_per_teacher)

#at most one two-hour lesson per subject is taught on the same day
def subject_per_day(model, t, c):
    for d in range(5):
        return sum(model.teach[t,c,l] for l in range(d*model.numb_slots_day + 1, model.numb_slots_day*(d+1) + 1)) <= 1 
model.subject_per_day = Constraint(model.teachers, model.classes, rule = subject_per_day)

#the sport lessons have to take place on Thursday at 2:00pm
def sport_1(model):
    return model.teach[8,1,15] == 1
model.sport_1 = Constraint(rule = sport_1)

#the sport lessons have to take place on Thursday at 2:00pm
def sport_2(model):
    return model.teach[9,2,15] == 1
model.sport_2 = Constraint(rule = sport_2)

#no course may be scheduled during Monday morning
def no_teaching_on_slot_1(model, t, c):
    return model.teach[t,c,1] == 0
model.no_teaching_on_slot_1 = Constraint(model.teachers, model.classes, rule = no_teaching_on_slot_1)

#The fourth teacher cannot teach on Monday mornings
def absent_slots_12(model):
    for l in range(1,3):
        return model.teach[4,2,l] == 0
model.absent_slots_12 = Constraint(rule = absent_slots_12)

#The second teacher cannot teach on Wednesday
def absent_slots_9_thru_12(model, c):
    for l in range(2*model.numb_slots_day + 1, 3*model.numb_slots_day + 1):
        return model.teach[2,c,l] == 0
model.absent_slots_9_thru_12 = Constraint(model.classes, rule = absent_slots_9_thru_12)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("establishing_a_college_timetable.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.teachers:
    for j in instance.classes:
        for k in instance.slots:
            if value(instance.teach[i,j,k]) > 0:
                print(f'Teacher {i} is assigned to class {j} at time slot {k}')
print(f'The minimum number of courses taught during time slots 1-4 every day is {value(instance.min_penalties)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.teachers:
        for j in instance.classes:
            for k in instance.slots:
                if value(instance.teach[i,j,k]) > 0:
                    output.write(f'Teacher {i} is assigned to class {j} at time slot {k}\n\n')
    output.write(f'The minimum number of courses taught during time slots 1-4 every day is {value(instance.min_penalties)}\n\n')
    output.close()
