# A company wishes to determine the optimal locations to build
# warehouses so as the delivery demands are met at a minimum cost. Let's
# assume that a set of possible warehouse locations and a set of
# customers are given. Nevertheless, costs for serving each customer to
# each location are given. The company wishes to built at most 2
# warehouses. Facilities can be hospitals, fire stations, ambulances,
# restaurants, schools, etc.

from pyomo.environ import *
import pandas

#reading the first row and first column of excel data
df = pandas.read_excel('warehouse_data.xlsx', header = 0, index_col = 0)

#Candidate warehouse locations
N = df.index.map(str).tolist()

#Customer  locations
M = df.columns.map(str).tolist()

#cost of serving a customer at a location
d ={(r,c) : df.at[r,c] for r in N for c in M}

#number of warehouses that can be build
P = 2

model = ConcreteModel("Warehouse Location Problem")

#variables
model.assign = Var(N, M, bounds = (0,1))
model.build = Var(N, within = Binary)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(d[n,m]*model.assign[n,m] for m in M for n in N)
model.min_cost = Objective(rule = min_cost)

#Constraints

#assign each customer to exactly one warehouse
def cust_warehouse(model, m):
    return sum(model.assign[n,m] for n in N) == 1
model.cust_warehouse = Constraint(M, rule = cust_warehouse)

#if a warehouse is not build, then there should no customers assigned to that warehouse
def implic_con(model, n, m):
    return model.assign[n,m] <= model.build[n]
model.implic_con = Constraint(N, M, rule = implic_con)

#no more than 2 warehouses should be built
def numb_warehouses(model):
    return sum(model.build[n] for n in N) <= P
model.numb_warehouses = Constraint(rule = numb_warehouses)

opt = SolverFactory('gurobi_direct')
results = opt.solve(model)

output = open('results.txt', 'w')
for n in N:
    if value(model.build[n]) > 0:
        output.write('\n\nWarehouse %s will serve the following customers:' % n) 
        for m in M:
            if value(model.assign[n,m]) > 0:
                output.write('\n%s' % m)
output.close()

for n in N:
    if value(model.build[n]) > 0:
        print('Warehouse %s will serve the following customers:' % n) 
        for m in M:
            if value(model.assign[n,m]) > 0:
                print('%s' % m)
