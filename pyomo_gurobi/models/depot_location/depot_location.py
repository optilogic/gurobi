# A company wishes to open new depots, each of which has a fixed cost for opening up. 
# The products of the company are delivered from depots to the customers closest to 
# each site. Depending on the distance of depot-customer pair there is a transportation 
# cost associated to delivering from a depot to a customer. There is a capacity limit 
# for each depot location and a demand for each customer given. The problem is to determine 
# which depots should be opened to minimize the total cost of construction and of delivery, 
# whilst satisfying all demands. 

from pyomo.environ import *

model = AbstractModel("Depot location")

#Sets and parameters

#set of depots
model.depots = Set()

#set of customers
model.customers = Set()

#distance between a customer and a depot
model.distance_cost = Param(model.depots, model.customers, within = NonNegativeReals)

#capacity for each depot
model.capacity_depot = Param(model.depots, within = NonNegativeIntegers)

#fixed cost for each depot
model.fixed_cost = Param(model.depots, within = NonNegativeIntegers)

#demand of each customer
model.demand = Param(model.customers, within = NonNegativeIntegers)

#Variables

#binary variable indicating whether a depot should be build
model.build = Var(model.depots, within = Binary)

#variable indicating the fraction of the demand of a customer delivered by a depot
model.frac_demand = Var(model.depots, model.customers, bounds = (0,1))

#Objective

#minimize the total cost
def min_cost(model):
    return sum(model.fixed_cost[d]*model.build[d] for d in model.depots) + sum(sum(model.distance_cost[d,c]*model.frac_demand[d,c] for c in model.customers) for d in model.depots)
model.min_cost = Objective(rule = min_cost)

#Constraints

#sum of all the supplies from different depots must meet the demand of a customer
def demand_met(model, c):
    return sum(model.frac_demand[d,c] for d in model.depots) == 1
model.demand_met = Constraint(model.customers, rule = demand_met)

#the amount of delivery to all customers from each depot should not exceed the capacity of each depot
def capacity_satisfied(model, d):
    return sum(model.demand[c]*model.frac_demand[d,c] for c in model.customers) <= model.capacity_depot[d]*model.build[d]
model.capacity_satisfied = Constraint(model.depots, rule = capacity_satisfied)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("depot_location.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.depots:
    if value(instance.build[i]) > 0:
        print(f'Depot {i} should be build')
for i in instance.depots:
    for j in instance.customers:
        if value(instance.frac_demand[i,j]) > 0:
            print(f'{100*value(instance.frac_demand[i,j])} percent of the demand of customer {j} should be delivered by depot {i}')
print(f'The minimum total cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.depots:
        if value(instance.build[i]) > 0:
            output.write(f'Depot {i} should be build\n\n')
    for i in instance.depots:
        for j in instance.customers:
            if value(instance.frac_demand[i,j]) > 0:
                output.write(f'{100*value(instance.frac_demand[i,j])} percent of the demand of customer {j} should be delivered by depot {i}\n\n')
    output.write(f'The minimum total cost is {value(instance.min_cost)}')
    output.close()
