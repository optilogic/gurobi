# Suppose a city through its local authorities wants to survey all the streets by using 
# Closed Circuit TV (CCTV) cameras which can be directed and pivoted through 360 degrees. 
# There are a certain number (49 in this case) of possible locations for installing these 
# type of cameras. The problem is find the minimum possible number of cameras and their 
# locations so that all the streets of the city are covered. This problem can be modelled 
# as a Mixed Integer Linear Program whose solution yields the optimum number of cameras needed.

from pyomo.environ import *

model = AbstractModel("CCTV surveillance")

#Sets and parameters

#set of possible locations to put a CCTV camera
model.locations = Set()

#set of streets
model.streets = Set(within = model.locations*model.locations)

#Variables

#binary variable install indicating whether or not to install a CCTV camera at location i
model.install = Var(model.locations, within = Binary)

#Objective

#minimize the number of cameras to be installed
def number_cameras(model):
    return sum(model.install[i] for i in model.locations)
model.number_cameras = Objective(rule = number_cameras)

#Constraints

#every street must be covered by at least one CCTV camera
def each_street_covered(model, i, j):
    return model.install[i] + model.install[j] >= 1
model.each_street_covered = Constraint(model.streets, rule = each_street_covered)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("cctv_surveillance.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.locations:
    if value(instance.install[i]) > 0:
        print("A CCTV camera should be installed at location %s" %i)
print("The mminimum amount of cameras needed is: %f" %value(instance.number_cameras))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.locations:
        if value(instance.install[i]) > 0:
            output.write("A CCTV camera should be installed at location %s\n\n" %i)
    output.write("The mminimum amount of cameras needed is: %f" %value(instance.number_cameras))
    output.close()
