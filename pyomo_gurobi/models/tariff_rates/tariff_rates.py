# A number of power stations are needed to satisfy electricity demands
# over a day. The day (24-hour) is divided into 5 different time
# periods. Three type of generating unit are available at the following
# quantities: 12 of type 1, 10 of type 2 and 5 of type 3. A
# minimum and maximum work level is given for each generator. Also, a
# hourly cost of using each generator at minimum level is provided. In
# addition, there is an extra hourly cost for each mw at which a unit is
# used above the minimum level. There is a cost associated to starting a
# generator. If there is an increase by 15% in demand, there should be
# sufficient generators working to make it possible.

from pyomo.environ import *

model = AbstractModel("Tariff rates")

#Sets and parameters

#set of time periods
model.times = Set()

#amount of hours in each period of time
model.hours_period = Param(model.times, within = NonNegativeReals)

#electricity demands for each time period
model.elec_demand = Param(model.times, within = NonNegativeReals)

#set of type of generators
model.gen_types = Set()

#minimum level each generator type works
model.min_level = Param(model.gen_types, within = NonNegativeReals)

#maximum level each generator type works
model.max_level = Param(model.gen_types, within = NonNegativeReals)

#cost per hour working at minimum
model.cost_at_min = Param(model.gen_types, within = NonNegativeReals)

#cost per hour per megawatt above minimum
model.cost_above_min = Param(model.gen_types, within = NonNegativeReals)

#cost for starting a generator of each type
model.start_cost = Param(model.gen_types, within = NonNegativeReals)

#cost per hour per megawatt above minimum level multiplied by the number of hours in the period
def c(model, i, j):
    return model.cost_above_min[i]*model.hours_period[j]
model.c = Param(model.gen_types, model.times, initialize = c)

#costs per hour operating at minimum level multiplied by the number of hours in the period
def total_cost_at_min_per_period(model, i, j):
    return model.cost_at_min[i]*model.hours_period[j]
model.total_cost_at_min_per_period = Param(model.gen_types, model.times, initialize = total_cost_at_min_per_period)

#Variables

#number of generators of a type working in a period
def ub(model, i, j):
    if i == 1:
        return RangeSet(0,12)
    elif i == 2:
        return RangeSet(0,10)
    else:
        return RangeSet(0,5)
model.n = Var(model.gen_types, model.times, within = ub)

#number of generators of a type started up in a period
model.s = Var(model.gen_types, model.times, within = ub)

#total output rate from generators of a type in a period
model.x = Var(model.gen_types, model.times, within = NonNegativeReals)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(sum(model.c[i,j]*(model.x[i,j] - model.min_level[i]*model.n[i,j]) for i in model.gen_types) for j in model.times) + sum(sum(model.total_cost_at_min_per_period[i,j]*model.n[i,j] for i in model.gen_types) for j in model.times) + sum(sum(model.start_cost[i]*model.s[i,j] for i in model.gen_types) for j in model.times)
model.min_cost = Objective(rule = min_cost)

#Constraints

#Demand must be met in each period
def demand_sat(model, j):
    return sum(model.x[i,j] for i in model.gen_types) >= model.elec_demand[j]
model.demand_sat = Constraint(model.times, rule = demand_sat)

#Constraints satisfying lower bound on the output 
def lb_output(model, i, j):
    return model.x[i,j] >= model.min_level[i]*model.n[i,j]
model.lb_output = Constraint(model.gen_types, model.times, rule = lb_output)

#Constraints satisfying upper bound on the output 
def ub_output(model, i, j):
    return model.x[i,j] <= model.max_level[i]*model.n[i,j]
model.ub_output = Constraint(model.gen_types, model.times, rule = ub_output)

#The extra guaranteed load requirement(15%) must be able to be met without starting up any more generators
def extra_load_sat(model, j):
    return sum(model.max_level[i]*model.n[i,j] for i in model.gen_types) >= 1.15*model.elec_demand[j]
model.extra_load_sat = Constraint(model.times, rule = extra_load_sat)

#The number of generators started in period j must equal the increase in number
def numb_gens_started(model, i, j):
    if j == 1:
        return model.s[i,1] >= model.n[i,1] - model.n[i,5]
    else:
        return model.s[i,j] >= model.n[i,j] - model.n[i,j-1]
model.numb_gens_started = Constraint(model.gen_types, model.times, rule = numb_gens_started)



#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("tariff_rates.dat")
results = solver.solve(instance)

for i in instance.gen_types:
    for j in instance.times:
        if value(instance.n[i,j]) > 0:
            print("Number of generating units of type %s working in period %s is: %d " %(i,j,value(instance.n[i,j])))
for i in instance.gen_types:
    for j in instance.times:
        if value(instance.s[i,j]) > 0:
            print("Number of generators of type %s started up in period %s is: %d " %(i,j, value(instance.s[i,j])))
for i in instance.gen_types:
    for j in instance.times:
        if value(instance.x[i,j]) > 0:
            print("Total output rate from generators of type %s in period %s is: %f " %(i,j, value(instance.x[i,j])))
print("The minimum total cost is: %f" %value(instance.min_cost))


#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.n[i,j]) > 0:
                output.write("Number of generating units of type %s working in period %s is: %d \n\n" %(i,j,value(instance.n[i,j])))
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.s[i,j]) > 0:
                output.write("Number of generators of type %s started up in period %s is: %d \n\n" %(i,j, value(instance.s[i,j])))
    for i in instance.gen_types:
        for j in instance.times:
            if value(instance.x[i,j]) > 0:
                output.write("Total output rate from generators of type %s in period %s is: %f \n\n" %(i,j, value(instance.x[i,j])))
    output.write("The minimum total cost is: %f" %value(instance.min_cost))
    output.close()
