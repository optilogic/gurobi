# An automotive company wishes to evaluate efficiences of different
# dealers, who have the right to sell company's cars. Each dealer has
# several measurable inputs and outputs. Inputs can be staff, showroom
# space, population covered etc., while outputs can be alpha sales, beta
# sales and profit. There are 28 auto dealers considered and the inputs
# and outputs for all the dealers are given below in the data file. A
# dealer is deemed to be efficient if it is not possible to find a
# mixture of proportions of other dealers, whose combined inputs do not
# exceed those of the dealer being considered, but whose outputs are
# equal to or exceed those of the dealer, otherwise the dealer is
# considered inefficient. This method is also referred to as Data
# Envelopment Analysis (DEA).

from pyomo.environ import *

model = AbstractModel("Efficiency Analysis")

#Sets and parameters

#set of garages/dealers
model.garages = Set()

#set of inputs
model.inputs = Set()

#set of outputs
model.outputs = Set()

#inputs for each garage/dealer
model.garage_input = Param(model.garages, model.inputs,  within = NonNegativeReals)

#outputs for each garage/dealer
model.garage_output = Param(model.garages, model.outputs, within = NonNegativeReals)

#Variables

#variable indicating the efficiency of a garage/dealer
model.efficiency = Var(within = NonNegativeReals)

#variable indicating the units of each of the garages/dealers
model.garage_units = Var(model.garages, within = NonNegativeReals)

#Objective

#maximizing the efficiency of a garage/dealer
def max_efficiency(model):
    return model.efficiency
model.max_efficiency = Objective(rule = max_efficiency, sense = maximize)

#Constraints

#combined inputs of all garages do not exceed the inputs of garage k
def combined_inputs(model, j):
    return sum(model.garage_input[i,j]*model.garage_units[i] for i in model.garages) <= model.garage_input[k,j]
model.combined_inputs = Constraint(model.inputs, rule = combined_inputs)

#combined outputs of all garages exceed the outputs of garage k
def combined_outputs(model, j):
    return sum(model.garage_output[i,j]*model.garage_units[i] for i in model.garages) >= model.garage_output[k,j]*model.efficiency
model.combined_outputs = Constraint(model.outputs, rule = combined_outputs)

#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
print("Efficiency Analysis: ")
for k in range(1,29):
    instance = model.create_instance("efficiency_analysis.dat")
    results = solver.solve(instance)
    if value(instance.max_efficiency) <= 1:
        print("Auto dealer %s is efficient" %k)
    else:
        print("Auto dealer %s is inefficient" %k)

#Python Script for writing the solution while checking the termination condition of the solver 
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("Efficient Analysis: \n\n ")
    for k in range(1,29):
        instance = model.create_instance("efficiency_analysis.dat")
        results = solver.solve(instance)
        if value(instance.max_efficiency) <= 1:
            output.write("Auto dealer %s is efficient \n\n" %k)
        else:
            output.write("Auto dealer %s is inefficient\n\n" %k)
    output.close()









