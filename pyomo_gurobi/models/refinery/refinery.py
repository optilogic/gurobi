# A refinery produces butane, petrol, diesel oil, and heating oil from two crudes. 
# Four types of operations are necessary to obtain these products: separation, 
# conversion, upgrading, and blending. The separation phase consists of distilling 
# the raw product into, among others, butane, naphtha, gasoil, and a residue. 
# The residue subsequently undergoes a conversion phase (catalytic cracking) to obtain 
# lighter products. The different products that come out of the distillation are purified 
# (desulfurization or sweetening) or upgraded by a reforming operation that augments their 
# octane value. Finally, to obtain the products that will be sold, the refinery blends 
# several of the intermediate products in order to fulfill the prescribed characteristics 
# of the commercial products. Demand of the final products and characteristics of the 
# intermediate and final products are given. The problem is to determine the amount of 
# crudes used so as to minimize the total production cost.

from pyomo.environ import *

model = AbstractModel("Refinery")

#Sets and parameters

#set of crudes
model.crudes = Set()

#set of final products
model.final = Set()

#set of intermediate products in production of petrol
model.ipetrol = Set()

#set of intermediate products in production of diesel
model.idiesel = Set()

#set of intermediate products in production of heating-oil
model.iheating_oil = Set()

#set of products resulting from distilation
model.prod_gen_distilation = Set()

#percentage of production for each product from each crude after distilation process
model.dist = Param(model.crudes, model.prod_gen_distilation, within = NonNegativeReals)

#set of products resulting from reforming
model.prod_gen_reforming = Set()

#percentage of production for each product generated from naphtha
model.ref = Param(model.prod_gen_reforming, within = NonNegativeReals)

#set of products resulting from cracking
model.prod_gen_cracking = Set()

#percentage of production for each product generated from cracking naphtha
model.crack = Param(model.prod_gen_cracking, within = NonNegativeReals)

#set of all final and intermediate products
model.all_products = model.final | model.ipetrol | model.idiesel | model.iheating_oil | model.prod_gen_distilation | model.prod_gen_reforming | model.prod_gen_cracking

#demand per final product
model.demand = Param(model.final, within = NonNegativeReals)

#available amount per crude
model.avail = Param(model.crudes, within = NonNegativeReals)

#cost per process
model.cost = Param(model.prod_gen_distilation, within = NonNegativeReals)

#cost per crude
model.cost_crude = Param(model.crudes, within = NonNegativeReals)

#octane value per products in set ipetrol
model.oct_val = Param(model.ipetrol, within = NonNegativeReals)

#vapor pressure per products in set ipetrol
model.vapor_pressure = Param(model.ipetrol, within = NonNegativeReals)

#volatility per product in set ipetrol
model.volatility = Param(model.ipetrol, within = NonNegativeReals)

#percentage of sulfur in each product in set idiesel
model.sulf = Param(model.idiesel, within = NonNegativeReals)

#Variables

#variables representing quantities to produce from all products set
model.produce = Var(model.all_products, within = NonNegativeReals)

#variable representing the quantity of each crude used in production
model.use = Var(model.crudes, within = NonNegativeReals)

#Objective

#minimize the production costs
def min_cost(model):
    return sum(model.cost_crude[c]*model.use[c] for c in model.crudes) + sum(model.cost[p]*model.produce[p] for p in model.prod_gen_distilation)
model.min_cost = Objective(rule = min_cost)

#Constraints

#maximum quantities of every component of the raw materials after distilation
def max_quant_dist(model, p):
    return model.produce[p] <= sum(model.dist[c,p]*model.use[c] for c in model.crudes)
model.max_quant_dist = Constraint(model.prod_gen_distilation, rule = max_quant_dist)

#maximum quantities of every component of the raw materials after reforming
def max_quant_ref(model, r):
    return model.produce[r] <= model.ref[r]*model.produce["naphtha"]
model.max_quant_ref = Constraint(model.prod_gen_reforming, rule = max_quant_ref)

#maximum quantities of every component of the raw materials after cracking
def max_quant_crack(model, c):
    return model.produce[c] <= model.crack[c]*model.produce["residue"]
model.max_quant_crack = Constraint(model.prod_gen_cracking, rule = max_quant_crack)

#Amount of cracked-naphtha is used in the production of petrol, heating oil, and diesel oil is equal to the total amount of cracked-naphtha
def conserv_cracked_naphtha(model):
    return model.produce["crknaphtha"] >= model.produce["petcrknaphtha"] + model.produce["dslcrknaphtha"] + model.produce["hocrknaphtha"]
model.conserv_cracked_naphtha = Constraint(rule = conserv_cracked_naphtha)

#Amount of cracked-gasoil is used in the production of heating oil, and diesel oil is equal to the total amount of cracked-gasoil
def conserv_cracked_gasoil(model):
    return model.produce["crkgasoil"] >= model.produce["dslcrkgasoil"] + model.produce["hocrkgasoil"]
model.conserv_cracked_gasoil = Constraint(rule = conserv_cracked_gasoil)

#Amount of sweetened-gasoil is used in the production of heating oil, and diesel oil is equal to the total amount of gasoil
def conserv_gasoil(model):
    return model.produce["gasoil"] >= model.produce["dslgasoil"] + model.produce["hogasoil"]
model.conserv_gasoil = Constraint(rule = conserv_gasoil)

#constraint on the way the quantity of butane is obtained from the intermediate products
def butane(model):
    return model.produce["butane"] == model.produce["distbutane"] + model.produce["refbutane"] - model.produce["petbutane"] 
model.butane = Constraint(rule = butane)

#constraint on the way the quantity of petrol is obtained from the intermediate products
def petrol(model):
    return model.produce["petrol"] == sum(model.produce[p] for p in model.ipetrol) 
model.petrol = Constraint(rule = petrol)

#constraint on the way the quantity of diesel is obtained from the intermediate products
def diesel(model):
    return model.produce["diesel"] == sum(model.produce[p] for p in model.idiesel)  
model.diesel = Constraint(rule = diesel)

#constraint on the way the quantity of heating oil is obtained from the intermediate products
def heating_oil(model):
    return model.produce["heating"] == sum(model.produce[p] for p in model.iheating_oil) 
model.heating_oil = Constraint(rule = heating_oil)

#the octane value of petrol must be better than 94
def min_val_oct(model):
    return sum(model.oct_val[p] * model.produce[p] for p in model.ipetrol) >= 94 * model.produce["petrol"]
model.min_val_oct = Constraint(rule = min_val_oct)

#the vapor pressure of petrol must be at most 12.7
def max_vap_pressure(model):
    return sum(model.vapor_pressure[p] * model.produce[p] for p in model.ipetrol) <= 12.7 * model.produce["petrol"]
model.max_vap_pressure = Constraint(rule = max_vap_pressure)

#volatility of petrol must be better than 17
def min_val_volatility(model):
    return sum(model.volatility[p] * model.produce[p] for p in model.ipetrol) >= 17 * model.produce["petrol"]
model.min_val_volatility = Constraint(rule = min_val_volatility)

#the sulfur of diesel must be at most 0.05 
def max_val_sulfur(model):
    return sum(model.sulf[p] * model.produce[p] for p in model.idiesel) <= 0.05 * model.produce["diesel"]
model.max_val_sulfur = Constraint(rule = max_val_sulfur)

#capacity on the production of naphtha
def cap_naphtha(model):
    return model.produce["naphtha"] <= 30000
model.cap_naphtha = Constraint(rule = cap_naphtha)

#capacity on the production of gasoil
def cap_gasoil(model):
    return model.produce["gasoil"] <= 50000
model.cap_gasoil = Constraint(rule = cap_gasoil)

#capacity on the production of residue
def cap_residue(model):
    return model.produce["residue"] <= 40000
model.cap_residue = Constraint(rule = cap_residue)

#demand_satisfied
def demand_satisfied(model, p):
    return model.produce[p] >= model.demand[p]
model.demand_satisfied = Constraint(model.final, rule = demand_satisfied)

#limits the use of each crude to its availability
def crude_use_limit(model, c):
    return model.use[c] <= model.avail[c]
model.crude_use_limit = Constraint(model.crudes, rule = crude_use_limit)



solver = SolverFactory('gurobi_direct')
instance = model.create_instance("refinery.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.all_products:
    if value(instance.produce[i]) > 0:
        print(f'{value(instance.produce[i])} units of {i} will be produced')
for i in instance.crudes:
    if value(instance.use[i]) > 0:
        print(f'{value(instance.use[i])} units of {i} are used')
print(f'The minimum production cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.all_products:
        if value(instance.produce[i]) > 0:
            output.write(f'{value(instance.produce[i])} units of {i} will be produced\n\n')
    for i in instance.crudes:
        if value(instance.use[i]) > 0:
            output.write(f'{value(instance.use[i])} units of {i} are used\n\n')
    output.write(f'The minimum production cost is {value(instance.min_cost)}')
    output.close()
