# Assume an architecture of a mobile phone network is given. Every geographical zone 
# is served by a transmitter-receiver/relay. Every relay is connected by cable or 
# electro-magnetic waves to a transit node. Mobile Telephone Switching Office controls 
# the network. A very reliable ring of fiber optic cable connects the hubs and the MTSO 
# with high capacity links. Connection costs, traffic and number of connections per cell 
# are given. The objective is to define the connections of the cells to the ring that 
# minimize the connection costs whilst remaining within the capacity limits and satisfying 
# the constraints on the number of connections.

from pyomo.environ import *

model = AbstractModel("Dimensioning of a mobile phone network")

#Sets and parameters

#set of cells
model.cells = Set()

#number of possible connections for every cell
model.connections = Param(model.cells, within = NonNegativeIntegers) 

#capacity of simultaneous phone calls during peak periods
model.capacity = Param(within = NonNegativeIntegers)

#set of hubs; in this case, the 5-th hub is the Mobile Telephone Switching Office (MTSO)
model.hubs = Set()

#Mobile Telephone Switching Office (MTSO)
model.MTSO = Set()

#set of all hubs
model.nodes = model.hubs | model.MTSO

#traffic from every cell
model.traffic = Param(model.cells, within = NonNegativeIntegers)

#arcs between cells and hubs
model.arcs = Set(within = model.cells*model.nodes)

#cost of connecting a cell to a hub
model.cell2hubcost = Param(model.arcs, within = NonNegativeReals)

#Variables

#binary variable indicating whether a cell is connected to a hub
model.connect = Var(model.cells, model.nodes, within = Binary)

#Objective

#minimizing the total connection cost
def min_cost(model):
    return sum(sum(model.cell2hubcost[c,h]*model.connect[c,h] for h in model.nodes) for c in model.cells)
model.min_cost = Objective(rule = min_cost)

#Constraints

#every cell is connected to the required number of hubs
def numb_hubs_per_cell(model, c):
    return sum(model.connect[c,h] for h in model.nodes) == model.connections[c]
model.numb_hubs_per_cell = Constraint(model.cells, rule = numb_hubs_per_cell)

#satisfying capacity limit of all hubs
def sat_cap_limit(model):
    return sum(sum((model.traffic[c]/ model.connections[c])*model.connect[c,h] for h in model.hubs) for c in model.cells) <= 2*model.capacity
model.sat_cap_limit = Constraint(rule = sat_cap_limit)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("dimensioning_of_a_mobile_phone_network.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.cells:
    for j in instance.nodes:
        if value(instance.connect[i,j]) > 0:
            print(f'Cell {i} is connected to hub {j}')
print(f'The minimum of the total connection cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.cells:
        for j in instance.nodes:
            if value(instance.connect[i,j]) > 0:
                output.write(f'Cell {i} is connected to hub {j}\n\n')
    output.write(f'The minimum of the total connection cost is {value(instance.min_cost)}')
    output.close()
