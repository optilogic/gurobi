# Premier League authorities wish to obtain the schedule for 2020-2021
# year in at most 10 days. Due to the limited amount of time, the
# authorities contact OptiLogic and request the most convenient schedule
# by satisfying several constraints. There are 20 teams playing in the
# league every year. Throughout the season, from August 2020 to May
# 2021, every team plays with every other team twice, once in each
# respective city. Since during the season, the best 4 teams of the
# previous season play in Champions League as well, the schedule for
# these teams will have several restrictions. For instance, the teams
# which play in Champions League would prefer to have "easy" matches on
# the weeks after. For instance, Manchester Utd requests that on week
# 8 to play away against Everton and the broadcasters have requested
# that the "big" match Arsenal-Chelsea to be played in the first four
# weeks of the season. This problem is formulated below as a binary
# program.

from pyomo.environ import *

model = AbstractModel("Sport Scheduling")


#Sets and parameters

#set of teams in a league
model.teams = Set()

#set of weeks
model.no_teams = Param(within = NonNegativeIntegers)
def wk(model):
    w = 2*(model.no_teams - 1) + 1
    return range(1, w)
model.weeks = Set(initialize = wk)


#Variables

#binary variable indicating whether team i is playing against team j in week k
model.play = Var(model.teams, model.teams, model.weeks, within = Binary)



#Objective

#maximizing the total value
model.maxi = Objective(expr = 0)



#Constraints

#each pair should play twice throughout the season
def games_pair(model, i, j):
    if i != j:
        return sum(model.play[i,j,k] for k in model.weeks) == 1
    else:
        return Constraint.Skip
model.games_pair = Constraint(model.teams, model.teams, rule = games_pair)

#every team must play once each week
def once_week(model, i, k):
    return sum(model.play[i,j,k] for j in model.teams if j != i) + sum(model.play[j,i,k] for j in model.teams if j != i) == 1
model.once_week = Constraint(model.teams, model.weeks, rule = once_week)

#Arsenal-Chelsea has to be played within the first four weeks, Everton-Man Utd will be played on 8th week.
def fix_match(model, i, j):
    if i == 'Arsenal' and j == 'Chelsea':
        return sum(model.play[i,j,k] for k in range(1,5)) == 1
    elif i == 'Everton' and j == 'Manchester Utd':
        return model.play[i,j,8] == 1
    else:
        return Constraint.Skip
model.fix_match = Constraint(model.teams, model.teams, rule = fix_match)


#linking the data with the model
instance = model.create_instance("sport_scheduling.dat")

#Solver
solver = SolverFactory('gurobi_direct')

#Calling the solver to solve the instance and displaying the solving information(log file)
results = solver.solve(instance)


#Python Script for printing the solution while checking the termination condition of the solver       
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('schedule.txt', 'w')
    output.write('Simple Schedule for Premier League \n')
    for k in instance.weeks:
        output.write('\nWeek ' + str(k) + ': \n\n')
        for i in instance.teams:
            for j in instance.teams:
                if i != j:
                    if value(instance.play[i,j,k]) > 0:
                        output.write(str(i) + ' - ' + str(j) + '\n')
    output.write('\nObjective Value is: ' + str(value(instance.maxi)))
    output.close()



#Script for displaying the solution on the terminal
for k in instance.weeks:
    print('\nWeek ' + str(k) + ': \n\n')
    for i in instance.teams:
        for j in instance.teams:
            if i != j:
                if value(instance.play[i,j,k]) > 0:
                    print(str(i) + ' - ' + str(j) + '\n')
print('\nObjective Value is: ' + str(value(instance.maxi)))
