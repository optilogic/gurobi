# A set of ten batches to be produced in a set of 5 machines in the next period are given. 
# For each batch the time to be produced by each machine is given. Also, each machine has 
# a certain capacity. There is a production cost associated to each pair batch-machine. 
# The problem is to determine on which machine should each batch be executed if it is wished 
# to minimize the total cost of production.

from pyomo.environ import *

model = AbstractModel("Assignment of production batches to machines")

#Sets and parameters

#set of batches
model.batches = Set()

#set of machines
model.machines = Set()

#duration of processing batch p by machine m
model.duration = Param(model.machines, model.batches, within = NonNegativeReals)

#capacity for every machine
model.capacity = Param(model.machines, within = NonNegativeIntegers)

#production cost of batch p by machine m
model.prod_cost = Param(model.machines, model.batches, within = NonNegativeIntegers)

#Variables

#binary variable deciding whether a batch will be produced by a certain machine
model.use_mp = Var(model.machines, model.batches, within = Binary)

#Objective

#minimizing is the total production cost
def min_cost(model):
    return sum(sum(model.prod_cost[m,p]*model.use_mp[m,p] for m in model.machines) for p in model.batches)
model.min_cost = Objective(rule = min_cost)

#Constraints

#every batch is assigned to a single machine
def every_batch_2_each_machine(model, p):
    return sum(model.use_mp[m,p] for m in model.machines) == 1
model.every_batch_2_each_machine = Constraint(model.batches, rule = every_batch_2_each_machine)

#Every machine may only work within its capacity limits
def capacity_per_machine(model, m):
    return sum(model.duration[m,p]*model.use_mp[m,p] for p in model.batches) <= model.capacity[m]
model.capacity_per_machine = Constraint(model.machines, rule = capacity_per_machine)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("assignment_of_production_batches_to_machines.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.machines:
    for j in instance.batches:
        if value(instance.use_mp[i,j]) > 0:
            print(f'Batch {j} will be produced by machine {i}')
print(f'The minimum total production cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.machines:
        for j in instance.batches:
            if value(instance.use_mp[i,j]) > 0:
                output.write(f'Batch {j} will be produced by machine {i}\n\n')
    output.write(f'The minimum total production cost is {value(instance.min_cost)}')
    output.close()
