# An electronics factory produces an amplifier on an assembly line with four workstations. 
# An amplifier is assembled in twelve operations between which there are certain precedence 
# constraints. The duration of every task and the list of its immediate predecessors are given. 
# The production manager would like to distribute the tasks among the four workstations, 
# subject to the precedence constraints, in order to balance the line to obtain the shortest 
# possible cycle time, that is, the total time required for assembling an amplifier. 
# Every task needs to be assigned to a single workstation.

from pyomo.environ import *

model = AbstractModel("Assembly line balancing")

#Sets and parameters

#set of tasks
model.tasks = Set()

#duration of each task
model.duration = Param(model.tasks, within = NonNegativeIntegers)

#set of workstations
model.workstations = Set()

#set of arcs
model.arcs = Set(within = model.tasks*model.tasks)

#Variables

#binary variable indicating whether task i is assigned to workstation j
model.assign = Var(model.tasks, model.workstations, within = Binary)

#variable representing the cycle time
model.cycle = Var(within = NonNegativeReals)

#Objective

#minimize the cycle
def min_cycle(model):
    return model.cycle
model.min_cycle = Objective(rule = min_cycle)

#Constraints

#each task is assigned to exactly one workstation
def each_task_one_workstation(model, i):
    return sum(model.assign[i,j] for j in model.workstations) == 1
model.each_task_one_workstation = Constraint(model.tasks, rule = each_task_one_workstation)

#variable cycle is an upper bound on the workload assigned to every workstation
def cycle_up_bound(model, w):
    return sum(model.duration[i]*model.assign[i,w] for i in model.tasks) <= model.cycle
model.cycle_up_bound = Constraint(model.workstations, rule = cycle_up_bound)

#An assignment is valid only if it fulfills the precedence constraints
def valid_assignment(model, i, j):
    return sum(m * model.assign[i,m] for m in model.workstations) <= sum(m * model.assign[j,m] for m in model.workstations)
model.valid_assignment = Constraint(model.arcs, rule = valid_assignment)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("assembly_line_balancing.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.tasks:
    for j in instance.workstations:
        if value(instance.assign[i,j]) > 0:
            print(f'Task {i} is assigned to work station {j}')
print(f'The minimum cycle time is {value(instance.min_cycle)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.tasks:
        for j in instance.workstations:
            if value(instance.assign[i,j]) > 0:
                output.write(f'Task {i} is assigned to work station {j}\n\n')
    output.write(f'The minimum cycle time is {value(instance.min_cycle)}')
    output.close()
