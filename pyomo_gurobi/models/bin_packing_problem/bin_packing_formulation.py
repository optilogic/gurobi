# Bin packing problem:

# There are n items to be packed and an infinite number of available bins of size B. 
# The sizes 0<=s_i <= B of individual items are assumed to be known. The problem is 
# to determine how to pack these n items in bins of size B so that the number of 
# required bins is minimum.

from pyomo.environ import *

model=AbstractModel("Bin Packing")

#Sets and Parameters

#set of possible bins
model.bins = Set()

#set of items to be packed
model.packs = Set()

#weight of each item
model.item_weight = Param(model.packs, within = NonNegativeReals)

#weight of each bin
model.bin_weight = Param(within = NonNegativeReals)

#Variables

#binary variable x_ij which is 1 if and only if item i is packed in bin j
model.x = Var(model.packs, model.bins, within = Binary)

#binary variable y_j which is 1 if and only if bin j is used
model.y = Var(model.bins, within = Binary)

#Objective

#objective minimizes the number of bins needed to pack all given items
def min_numb_bins(model):
    return sum(model.y[j] for j in model.bins)
model.obj = Objective(rule=min_numb_bins)

#Constraints

# The following constraints force the placement of each item in one bin.
def item_one_bin(model, i):
    return sum(model.x[i,j] for j in model.bins) == 1
model.item_one_bin = Constraint(model.packs, rule=item_one_bin)

#The following constraints represent the upper limit on the bins contents, as well as the fact that items cannot be packed in a bin that is not in use. 
def bin_capacity(model, j):
    return sum(model.item_weight[i]*model.x[i,j] for i in model.packs) <= model.bin_weight*model.y[j]
model.bin_capacity = Constraint(model.bins, rule=bin_capacity)

#The following constraints provide an enhanced formulation, indicating that if a bin is not used items cannot be placed there. 
def no_item_in_unused_bins(model, i ,j):
    return model.x[i,j] <= model.y[j]
model.no_item_in_unused_bins = Constraint(model.packs, model.bins, rule = no_item_in_unused_bins)



#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("bin_packing_formulation.dat")
results = solver.solve(instance)

for i in instance.packs:
    for j in instance.bins:
        if value(instance.x[i,j]) > 0:
            print("Item %s will be packed in bin %s" %(i,j))
print("The minimum number of bins needed is %f" %value(instance.obj))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.packs:
        for j in instance.bins:
            if value(instance.x[i,j]) > 0:
                output.write("Item %s will be packed in bin %s\n\n" %(i,j))
    output.write("The minimum number of bins needed is %f" %value(instance.obj))
    output.close()

