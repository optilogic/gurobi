# A company needs to transport 180 tonnes of chemical products stored in four depots 
# to three recycling centers. The depots contain respectively 50, 40, 35, and 65 tonnes. 
# There are two modes of transport are available, road and rail. The first depot only 
# delivers to the first two centers and that by road at a cost of $12k/t and $11k/t. 
# The second depot only delivers to the second center, by rail or road at $12k/t and $14k/t, 
# respectively. The third depot delivers to the second center by road ($9k/t) and to the third 
# center by rail or road for $4k/t and $5k/t respectively. The fourth depot delivers to the second 
# center by rail or road at a cost of $11k/t and $14k/t, and to the third center by rail or road 
# at $10k/t and $14k/t respectively. The company needs to transport at least 10 tonnes and at 
# most 50 tonnes for any single delivery. The problem is to determine how should the company 
# transport the 180 tonnes of chemicals to minimize the total cost of transport.

from pyomo.environ import *

model = AbstractModel("Choosing the mode of transport")

#Sets and parameters

#sources
model.sources = Set()

#sinks
model.sinks = Set()

#set of depots
model.depots = Set()

#set of recycling centers
model.centers = Set()

#capacity of each depot
model.capacity_depot = Param(model.depots, within = NonNegativeIntegers)

#set of transportation modes according to the center
model.transp_modes = Set()

#set of all nodes but sinks
model.all_but_sinks = model.sources | model.depots | model.centers | model.transp_modes

#set of all nodes but sources
model.all_but_sources = model.sinks | model.depots | model.centers | model.transp_modes

#set of all nodes but sources and sinks
model.all_but_sources_and_sinks = model.depots | model.centers | model.transp_modes

#set of arcs
model.arcs = Set(within = model.all_but_sinks*model.all_but_sources)

#minimum amount to be transported by each depot through rails
model.min_cap_transp_rail = Param(model.arcs, within = NonNegativeReals)

#maximum amount to be transported by each depot through rails
model.max_cap_transp_rail = Param(model.arcs, within = NonNegativeReals)

#cost for each transportation type from each depot
model.costs = Param(model.arcs, within = NonNegativeIntegers)

#total amount of chemicals to be transported
model.tot_amount = Param(within = NonNegativeReals)

#Variables

#variable indicating the amount of flow transport on an arc
model.flow = Var(model.arcs, within = NonNegativeIntegers)

#Objective

#minimizing the total cost of transportation
def min_cost(model):
    return sum(model.costs[i,j]*model.flow[i,j] for (i,j) in model.arcs)
model.min_cost = Objective(rule = min_cost)

#Constraint

#conservation constraints
def inflow_equal_outflow(model, k):
    return sum(model.flow[i,j] for (i,j) in model.arcs if j == k) == sum(model.flow[i,j] for (i,j) in model.arcs if i == k)
model.inflow_equal_outflow = Constraint(model.all_but_sources_and_sinks, rule = inflow_equal_outflow)

#the total flow must be 180
def total_flow(model):
    return sum(model.flow[i,j] for (i,j) in model.arcs if i == 0) == model.tot_amount
model.total_flow = Constraint(rule = total_flow)

#constraints satisfying the minimum flow on each arc
def min_cap(model, i, j):
    return model.flow[i,j] >= model.min_cap_transp_rail[i,j]
model.min_cap = Constraint(model.arcs, rule = min_cap)

#constraints satisfying the maximum flow on each arc
def max_cap(model, i, j):
    return model.flow[i,j] <= model.max_cap_transp_rail[i,j]
model.max_cap = Constraint(model.arcs, rule = max_cap)

solver = SolverFactory('gurobi_direct')
instance = model.create_instance("chossing_mode_of_transport.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.arcs:
    if value(instance.flow[i,j]) > 0:
        print(f'Transportation flow on arc ({i},{j}) is {value(instance.flow[i,j])}')
print(f'The minimum total transportation cost is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for (i,j) in instance.arcs:
        if value(instance.flow[i,j]) > 0:
            output.write(f'Transportation flow on arc ({i},{j}) is {value(instance.flow[i,j])}\n\n')
    output.write(f'The minimum total transportation cost is {value(instance.min_cost)}')
    output.close()
