# A company produces food for farm animals that is sold in two forms: powder and granules. 
# The raw materials used for the production of the food are: oat, maize and molasses. 
# The raw materials (with the exception of molasses) first need to be ground, and then all 
# raw materials that will form a product are blended. In the last step of the production 
# process the product mix is either transformed to granules or sieved to obtain food in the 
# form of powder. Every food product needs to fulfill certain nutritional requirements. 
# The percentages of proteins, lipids and fibers contained in the raw materials and the 
# required percentages in the final products are given. The amount of raw material that is 
# available every day and the respective prices are given. The cost of the different production 
# steps are given as well. With a daily demand of nine tonnes of granules and twelve tonnes of 
# powder, the problem is to determine which quantities of raw materials are required and how 
# should they be blended to minimize the total cost.

from pyomo.environ import *

model = AbstractModel()

#Sets and parameters

# set of raw materials
model.raw = Set()

# set of food product types that are produced
model.food = Set()

# set of nutritional components
model.comp = Set()

# the price per kg of raw material r
model.COST_r = Param(model.raw, within = NonNegativeReals)

# the maximum available quantity of raw material r
model.AVAIL_r = Param(model.raw, within = NonNegativeReals)

# the daily demand of food product f
model.DEM_f = Param(model.food, within = NonNegativeReals)

# the required content of nutritional component c
model.REQ_c = Param(model.comp, within = NonNegativeReals)

# the content of c in raw material r
model.P_rc = Param(model.raw, model.comp, within = NonNegativeReals)

#Variables

# representing the quantity of raw material r used for the production of food type f
model.use = Var(model.raw, model.food, domain=NonNegativeReals)

# the amount of food f produced
model.prod = Var(model.food, domain=NonNegativeReals) 

#Objective

#minimizing the total cost
def obj_rule(model):
    x =0
    for r in model.raw:
        for f in model.food:
            x += model.COST_r[r]*model.use[r,f]
    for f in model.food:
        for r in model.raw:
            if r != 3:
                x += 0.25*model.use[r,f]
    for r in model.raw:
        for f in model.food:
            x+=0.05 * model.use[r,f]
    for r in model.raw:
        x+=0.42*model.use[r,1] + 0.17* model.use[r, 2]
    return x
model.obj = Objective(rule=obj_rule)

#Constraints

#the produced quantity of every food type corresponds to the sum of the raw material used for its production
def use_eq_prod(model, f):
    return sum(model.use[r,f] for r in model.raw) == model.prod[f]
model.use_eq_prod = Constraint(model.food, rule=use_eq_prod)

#demand satisfied
def demand_met(model, f):
    return model.prod[f] >= model.DEM_f[f]
model.meet_demand = Constraint(model.food, rule=demand_met)

#limit on the use of raw material r on all foods
def sat_availability(model, r):
    return sum(model.use[r,f] for f in model.food) <= model.AVAIL_r[r]
model.avail_sat = Constraint(model.raw, rule=sat_availability)

#required content of nutritional component 3
def sat_required_content_comp3(model, f):
    return sum(model.P_rc[r,3] * model.use[r,f] for r in model.raw) <= model.REQ_c[3] * model.prod[f] 
model.requir_cont_sat = Constraint(model.food, rule=sat_required_content_comp3)

#required content of nutritional components
def sat_required_content_comp1_2(model, f, c):
    if c == 3:
        return Constraint.Skip
    else:
        return sum(model.P_rc[r,c] * model.use[r,f] for r in model.raw) >= model.REQ_c[c] * model.prod[f]
model.required_content_comp1_2_sat = Constraint(model.food, model.comp, rule=sat_required_content_comp1_2)

solver = SolverFactory('gurobi_direct')
instance = model.create_instance("animal_food_production.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.raw:
    for j in instance.food:
        if value(instance.use[i,j]) > 0:
            print(f'The amount of raw material {i} used to produce food {j} is {value(instance.use[i,j])}')
for i in instance.food:
    if value(instance.prod[i]) > 0:
        print(f'The amount of food {i} produced is {value(instance.prod[i])}')
print(f'The minimum of the total cost is {value(instance.obj)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.raw:
        for j in instance.food:
            if value(instance.use[i,j]) > 0:
                output.write(f'The amount of raw material {i} used to produce food {j} is {value(instance.use[i,j])}\n\n')
    for i in instance.food:
        if value(instance.prod[i]) > 0:
            output.write(f'The amount of food {i} produced is {value(instance.prod[i])}\n\n')
    output.write(f'The minimum of the total cost is {value(instance.obj)}')
    output.close()
