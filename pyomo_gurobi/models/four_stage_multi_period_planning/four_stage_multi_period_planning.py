# Minimizing the total transportation, inventory holding and backorder 
# costs for each time period t, while planning the production and distribution 
# on a four stage multi-period problem.

from pyomo.environ import *

model = AbstractModel("Four-stage multi-period planning")

#Sets

#set of raw materials
model.n = Set()

#set of manufactored in-plant items i.e., MIP
model.p = Set()

#set of suppliers
model.h = Set()

#set of manufacturing plants
model.i = Set()

#set of manufactoring plants for shipment of MIP items, subset of i
model.s = Set(within = model.i)

#set of warehouses
model.j = Set()

#set of retailers
model.k = Set()

#set of time periods
model.t = Set()

#set of transport modes
model.m = Set()

#Parameters

#Supplier Stage

#Quality standard set by manufacturer for raw material 
model.Qstdset = Param(model.n, within = NonNegativeReals)

#Quality of raw material n from supplier h in time period t
model.Qstd = Param(model.n, model.h, model.t, within = NonNegativeReals)

#Cost per unit of raw material n from supplier h for time period t
model.cpu = Param(model.n, model.h, model.t, within = NonNegativeReals)

#Capacity limit for raw material n provided by supplier h
model.SCap = Param(model.n, model.h, within = NonNegativeReals)

#Minimum quantity supplied by supplier h for raw material n
model.SMin = Param(model.n, model.h, within = NonNegativeReals)

#Unit transportation cost of shipping raw material n from supplier h to plant i
model.TSUP = Param(model.n, model.h, model.i, within = NonNegativeReals)

#Manufacturer Stage

#Capacity of plant i to manufacture the finished product
model.PCap = Param(model.i, within = NonNegativeReals)

#Capacity of plant i to manufacture the p-th MIP item
model.MIPCap = Param(model.p, model.i, within = NonNegativeReals)

#Unit cost of manufacturing the finished product at plant i
model.Pprod = Param(model.i, within = NonNegativeReals)

#Unit transportation cost of shipping the p-th MIP item from plant i to plant s
model.TMIP = Param(model.p, model.i, model.s, within = NonNegativeReals)

#Unit transportation cost for the finished product from plant i to warehouse j
model.TPROD = Param(model.i, model.j, within = NonNegativeReals)

#Unit production cost of the p-th MIP item in plant i for time period t
model.CMIP = Param(model.p, model.i, model.t, within = NonNegativeReals)

#Number of raw material of type n required to produce one finished product
model.alpha = Param(model.n, within = NonNegativeReals)

#Number of p-th MIP item required to produce one finished product
model.beta = Param(model.p, within = NonNegativeReals)

#Warehouse Stage

#Capacity limit for warehouse j for storing finished product
model.WCap = Param(model.j, within = NonNegativeReals)

#Initial Inventory of finished product at warehouse j
model.WInitialInv = Param(model.j, within = NonNegativeReals)

#Inventory holding cost per unit of finished product per unit time at warehouse j
model.Hw = Param(model.j, within = NonNegativeReals)

#Unit  transportation  cost  of  the  finished  product  from  warehouse j  to retailer k using shipment mode m
model.TWAR = Param(model.j, model.k, model.m, within = NonNegativeReals)

#Maximum capacity limit for the number of units transported by each mode
model.cap_transp_mode = Param(model.m, within = NonNegativeReals)

#Retailer Stage

#Demand for the finished product at retailer k in time period t
model.R = Param(model.k, model.t, within = NonNegativeReals)

#Inventory holding cost per unit of finished product per unit time at retailer k
model.Hr = Param(model.k, within = NonNegativeReals)

#Maximum inventory capacity at retailer k
model.RCap = Param(model.k, within = NonNegativeReals)

#Unit backorder cost per unit time at retailer k
model.RBackcost = Param(model.k, within = NonNegativeReals)

#Initial Inventory of finished product at retailer k
model.RInitialInv = Param(model.k, within = NonNegativeReals)

#Variables

#Supplier Stage

#Binary variable denoting whether supplier h is selected in time period t for raw material n
model.Sa = Var(model.n, model.h, model.t, within = Binary)

#Quantity of raw material n ordered from selected supplier h for time period t
model.w = Var(model.n, model.h, model.t, within = NonNegativeReals)

#Quantity of raw material n transported from supplier h to plant i during time period t
model.u = Var(model.n, model.h, model.i, model.t, within = NonNegativeReals)

#Manufacturer Stage

#Quantity of raw material n reaching plant i in time period t
model.v = Var(model.n, model.i, model.t, within = NonNegativeReals)

#Number of finished products manufactured at plant i in time period t
model.X = Var(model.i, model.t, within = NonNegativeReals)

#Number of p-th MIP item produced in plant i in time period t
model.XMIP = Var(model.p, model.i, model.t, within = NonNegativeReals)

#Number of p-th MIP item shipped from plant i to plant s in time period t
model.XMIPS = Var(model.p, model.i, model.s, model.t, within = NonNegativeReals)

#Requirement of the p-th MIP item in plant s in each time period t
model.MIPReq = Var(model.p, model.s, model.t, within = NonNegativeReals)

#Number of finished products shipped from plant i to warehouse j during time period t
model.y = Var(model.i, model.j, model.t, within = NonNegativeReals)

#Warehouse Stage

#Cummulative inventory in warehouse j at the end of time period t
model.phi = Var(model.j, model.t, within = NonNegativeReals)

#Number of finished products shipped from warehouse j to retailer k in time period t using transport mode m
model.z = Var(model.j, model.k, model.t, model.m, within = NonNegativeReals)

#Retailer Stage

#Cummulative backorders at retailer k at end of time period t
model.RBO = Var(model.k, model.t, within = NonNegativeReals)

#Cummulative inventory at retailer k at the end of time period t
model.RInv = Var(model.k, model.t, within = NonNegativeReals)

#Objective

#Minimizing the total cost
def min_cost(model):
    return sum(sum(sum(model.cpu[n,h,t]*model.w[n,h,t] for t in model.t) for h in model.h) for n in model.n) + sum(sum(sum(sum(model.TSUP[n,h,i]*model.u[n,h,i,t] for t in model.t) for i in model.i) for h in model.h) for n in model.n) + sum(sum(model.Pprod[i]*model.X[i,t] for t in model.t) for i in model.i) + sum(sum(sum(model.CMIP[p,i,t]*model.XMIP[p,i,t] for t in model.t) for i in model.i) for p in model.p) + sum(sum(sum(sum(model.TMIP[p,i,s]*model.XMIPS[p,i,s,t] for t in model.t) for s in model.s) for i in model.i) for p in model.p) + sum(sum(sum(model.TPROD[i,j]*model.y[i,j,t] for t in model.t) for j in model.j) for i in model.i) + sum(sum(sum(sum(model.TWAR[j,k,m]*model.z[j,k,t,m] for m in model.m) for t in model.t) for k in model.k) for j in model.j) + sum(sum(model.Hw[j]*model.phi[j,t] for t in model.t) for j in model.j) + sum(sum(model.Hr[k]*model.RInv[k,t] for t in model.t) for k in model.k) + sum(sum(model.RBackcost[k]*model.RBO[k,t] for t in model.t) for k in model.k)
model.min_cost = Objective(rule = min_cost)

#Constraints

#The quality of raw material purchased Qstd(n,h,t) must meet the specific standard set for that material n by the manufacturer Qstdset(n) for every supplier h in each time period t:
def con1(model, n, h, t):
    return model.Qstd[n,h,t] >= model.Qstdset[n]*model.Sa[n,h,t]
model.con1 = Constraint(model.n, model.h, model.t, rule = con1)

#The capacity of the supplier h for raw material n must be high enough to meet the quantity of raw material ordered from the supplier in each time period t
def con2(model, n, h, t):
    return model.w[n,h,t] <= model.SCap[n,h]*model.Sa[n,h,t]
model.con2 = Constraint(model.n, model.h, model.t, rule = con2)

#The order quantity of each raw material n must meet the minimum purchase requirement of supplier h in every time period t
def con3(model, n, h, t):
    return model.w[n,h,t] >= model.SMin[n,h]*model.Sa[n,h,t]
model.con3 = Constraint(model.n, model.h, model.t, rule = con3)

#For every time period t, only one supplier is finally selected, but the same supplier can supply more than one raw material
def con4(model, n, t):
    return sum(model.Sa[n,h,t] for h in model.h) == 1
model.con4 = Constraint(model.n, model.t, rule = con4)

#The raw materials purchased from the selected suppliers are shipped to all the plants as follows
def con5(model, n, h, t):
    return model.w[n,h,t] == sum(model.u[n,h,i,t] for i in model.i)
model.con5 = Constraint(model.n, model.h, model.t, rule = con5)

#Quantity of raw material n reaching plant i in time period t after the first period of time
def con6(model, n, i, t):
    if t == 1:
        return Constraint.Skip
    else:
        return model.v[n,i,t] == sum(model.u[n,h,i,t-1] for h in model.h)
model.con6 = Constraint(model.n, model.i, model.t, rule = con6)

#The plant\'s production for both the finished products and MIP items must satisfy their respective capacity limits
def con7(model, i, t):
    return model.X[i,t] <= model.PCap[i]
model.con7 = Constraint(model.i, model.t, rule = con7)

def con8(model, p, i, t):
    return model.XMIP[p,i,t] <= model.MIPCap[p,i]
model.con8 = Constraint(model.p, model.i, model.t, rule = con8)

#for producing one finished product X(i,t) in each plant i for every time period t we require alpha(n) number of raw materials of type n and beta(p) number of  p-th MIP items.
def con9(model, n, i, t):
    return model.v[n,i,t] == model.alpha[n]*model.X[i,t]
model.con9 = Constraint(model.n, model.i, model.t, rule = con9)

def con10(model, p, s, t):
    return model.MIPReq[p,s,t] == model.beta[p]*model.X[s,t]
model.con10 = Constraint(model.p, model.s, model.t, rule = con10)

#the number of MIP items shipped from plant i to all other plants s to meet their requirement levels plus their own in-plant use must be less than the MIP item production at that plant.
def con11(model, p, i, t):
    return sum(model.XMIPS[p,i,s,t] for s in model.s) <= model.XMIP[p,i,t]
model.con11 = Constraint(model.p, model.i, model.t, rule = con11)

# the internal use of MIP items in any plant s plus the MIP items received from all other plants i must meet the MIP item requirement level of that plant s for every MIP item p in each time period t
def con12(model, p, s, t):
    return sum(model.XMIPS[p,i,s,t] for i in model.i) >= model.MIPReq[p,s,t]
model.con12 = Constraint(model.p, model.s, model.t, rule = con12)

#The finished products assembled in each plant i are shipped to the different warehouses jfor every time period t
def con13(model, i, t):
    return model.X[i,t] == sum(model.y[i,j,t] for j in model.j)
model.con13 = Constraint(model.i, model.t, rule = con13)

#The inventory stored in the warehouses must be within their capacity limits for each time period t
def con14(model, j, t):
    return model.phi[j,t] <= model.WCap[j]
model.con14 = Constraint(model.j, model.t, rule = con14)

#inventory at the end of time period t-1 + Shipment received from manufacturing plants in time period t = Inventory at the end of time period t + Shipment sent to retailers in time period t
def con15(model, j, t):
    if t == 1:
        return model.WInitialInv[j] == model.phi[j,1] + sum(sum(model.z[j,k,1,m] for k in model.k) for m in model.m)
    else:
        return model.phi[j, t- 1] + sum(model.y[i,j,t-1] for i in model.i) == model.phi[j,t] + sum(sum(model.z[j,k,t,m] for k in model.k) for m in model.m)
model.con15 = Constraint(model.j, model.t, rule = con15)

#The products shipped to the all retailers z(j,k,t,m) using the various modes of transportation must meet their capacity limits
def con16(model, j, k, t, m):
    if m == 1: 
        return model.z[j,k,t,1] <= model.cap_transp_mode[1]
    elif m == 2:
        return model.z[j,k,t,2] <= model.cap_transp_mode[2]
    elif m == 3:
        return model.z[j,k,t,3] <= model.cap_transp_mode[3]
    else:
        return Constraint.Skip
model.con16 = Constraint(model.j, model.k, model.t, model.m, rule = con16)

#The inventory stored at the retailer must meet the capacity limit for every retailer k in each time period t
def con17(model, k, t):
    return model.RInv[k,t] <= model.RCap[k]
model.con17 = Constraint(model.k, model.t, rule = con17)

#Initial  Inventory  +  Shipment  received  from  warehouse  +  Backorders  at  the  end  of  time period t = Retailer demand + Final inventory + Backorders at the end of time period t-1 
def con18(model, k, t):
    if t == 1:
        return model.RInitialInv[k] + model.RBO[k,1] == model.R[k,1] + model.RInv[k,1]
    elif t == 2:
        return model.RInv[k,1] + sum(model.z[j,k,1,1] for j in model.j) + model.RBO[k,2] == model.R[k,2] + model.RInv[k,2] + model.RBO[k,1]
    elif t == 3:
        return model.RInv[k,2] + sum(model.z[j,k,2,1] + model.z[j,k,1,2] for j in model.j) + model.RBO[k,3] == model.R[k,3] + model.RInv[k,3] + model.RBO[k,2]
    else:
        return model.RInv[k,t-1] + sum(sum(model.z[j,k,t-m,m] for m in model.m) for j in model.j) + model.RBO[k,t] == model.R[k,t] + model.RInv[k,t] + model.RBO[k,t-1]
model.con18 = Constraint(model.k, model.t, rule = con18)

#The backorders at the retailers for the last time period T is assumed to be zero since all retailer demands have to be met by the end of time period T
def con19(model, k):
    return model.RBO[k, 10] == 0
model.con19 = Constraint(model.k, rule = con19)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("four_stage_multi_period_planning.dat")
results = solver.solve(instance)

for i in instance.n:
    for j in instance.h:
        for k in instance.t:
            if value(instance.Sa[i,j,k]) > 0:
                print("Supplier %s is selcted on time period %s for raw material %s" %(j,k,i))
for i in instance.n:
    for j in instance.h:
        for k in instance.t:
            if value(instance.w[i,j,k]) > 0:
                print("Quality of raw material %s ordered from supplier %s on time period %s is: %f" %(i,j,k,value(instance.w[i,j,k])))
for i in instance.n:
    for j in instance.h:
        for m in instance.i:
            for k in instance.t:
                if value(instance.u[i,j,m,k]) > 0:
                    print("Quality of raw material %s transported from supplier %s to plant %s during time period %s is: %f" %(i,j,m,k,value(instance.u[i,j,m,k])))
for i in instance.n:
    for m in instance.i:
        for k in instance.t:
            if value(instance.v[i,m,k]) > 0:
                print("Quantity of raw material %s reaching plant %s in time period %s is: %f" %(i,m,k,value(instance.v[i,m,k])))
for m in instance.i:
    for k in instance.t:
        if value(instance.X[m,k]) > 0:
            print("Number of finished products manufactured at plant %s in time period %s is: %f" %(m,k,value(instance.X[m,k])))
for q in instance.p:
    for m in instance.i:
        for k in instance.t:
            if value(instance.XMIP[q,m,k]) > 0:
                print("Number of MIP item %s produced in plant %s in time period %s is: %f" %(q,m,k,value(instance.XMIP[q,m,k])))
for q in instance.p:
    for m in instance.i:
        for l in instance.s:
            for k in instance.t:
                if value(instance.XMIPS[q,m,l,k]) > 0:
                    print("Number of MIP item %s shipped from plant %s to plant %s in time period %s is: %f" %(q,m,l,k,value(instance.XMIPS[q,m,l,k])))
for q in instance.p:
    for l in instance.s:
        for k in instance.t:
            if value(instance.MIPReq[q,l,k]) > 0:
                print("Requirement of the MIP item %s in plant %s in time period %s is: %f" %(q,l,k,value(instance.MIPReq[q,l,k])))
for m in instance.i:
    for g in instance.j:
        for k in instance.t:
            if value(instance.y[m,g,k]) > 0:
                print("Number of finished products shipped from plant %s to warehouse %s during time period %s is: %f" %(m,g,k,value(instance.y[m,g,k])))
for g in instance.j:
    for k in instance.t:
        if value(instance.phi[g,k]) > 0:
            print("Cummulative inventory in warehouse %s at the end of time period %s is: %f" %(g,k,value(instance.phi[g,k])))
for g in instance.j:
    for m in instance.k:
        for k in instance.t:
            for l in instance.m:
                if value(instance.z[g,m,k,l]) > 0:
                    print("Number of finished products shipped from warehouse %s to retailer %s in time period %s using transport mode %s is: %f " %(g,m,k,l,value(instance.z[g,m,k,l])))
for m in instance.k:
    for k in instance.t:
        if value(instance.RBO[m,k]) > 0:
            print("Cummulative backorders at retailer %s at end of time period %s is: %f" %(m,k,value(instance.RBO[m,k])))
for m in instance.k:
    for k in instance.t:
        if value(instance.RInv[m,k]) > 0:
            print("Cummulative inventory at retailer %s at the end of time period %s is: %f" %(m,k,value(instance.RInv[m,k])))
print("The minimum total cost is: %f" %value(instance.min_cost))
        
            
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.n:
        for j in instance.h:
            for k in instance.t:
                if value(instance.Sa[i,j,k]) > 0:
                    output.write("Supplier %s is selcted on time period %s for raw material %s\n\n" %(j,k,i))
    for i in instance.n:
        for j in instance.h:
            for k in instance.t:
                if value(instance.w[i,j,k]) > 0:
                    output.write("Quality of raw material %s ordered from supplier %s on time period %s is: %f\n\n" %(i,j,k,value(instance.w[i,j,k])))
    for i in instance.n:
        for j in instance.h:
            for m in instance.i:
                for k in instance.t:
                    if value(instance.u[i,j,m,k]) > 0:
                        output.write("Quality of raw material %s transported from supplier %s to plant %s during time period %s is: %f\n\n" %(i,j,m,k,value(instance.u[i,j,m,k])))
    for i in instance.n:
        for m in instance.i:
            for k in instance.t:
                if value(instance.v[i,m,k]) > 0:
                    output.write("Quantity of raw material %s reaching plant %s in time period %s is: %f\n\n" %(i,m,k,value(instance.v[i,m,k])))
    for m in instance.i:
        for k in instance.t:
            if value(instance.X[m,k]) > 0:
                output.write("Number of finished products manufactured at plant %s in time period %s is: %f\n\n" %(m,k,value(instance.X[m,k])))
    for q in instance.p:
        for m in instance.i:
            for k in instance.t:
                if value(instance.XMIP[q,m,k]) > 0:
                    output.write("Number of MIP item %s produced in plant %s in time period %s is: %f\n\n" %(q,m,k,value(instance.XMIP[q,m,k])))
    for q in instance.p:
        for m in instance.i:
            for l in instance.s:
                for k in instance.t:
                    if value(instance.XMIPS[q,m,l,k]) > 0:
                        output.write("Number of MIP item %s shipped from plant %s to plant %s in time period %s is: %f\n\n" %(q,m,l,k,value(instance.XMIPS[q,m,l,k])))
    for q in instance.p:
        for l in instance.s:
            for k in instance.t:
                if value(instance.MIPReq[q,l,k]) > 0:
                    output.write("Requirement of the MIP item %s in plant %s in time period %s is: %f\n\n" %(q,l,k,value(instance.MIPReq[q,l,k])))
    for m in instance.i:
        for g in instance.j:
            for k in instance.t:
                if value(instance.y[m,g,k]) > 0:
                    output.write("Number of finished products shipped from plant %s to warehouse %s during time period %s is: %f\n\n" %(m,g,k,value(instance.y[m,g,k])))
    for g in instance.j:
        for k in instance.t:
            if value(instance.phi[g,k]) > 0:
                output.write("Cummulative inventory in warehouse %s at the end of time period %s is: %f\n\n" %(g,k,value(instance.phi[g,k])))
    for g in instance.j:
        for m in instance.k:
            for k in instance.t:
                for l in instance.m:
                    if value(instance.z[g,m,k,l]) > 0:
                        output.write("Number of finished products shipped from warehouse %s to retailer %s in time period %s using transport mode %s is: %f\n\n" %(g,m,k,l,value(instance.z[g,m,k,l])))
    for m in instance.k:
        for k in instance.t:
            if value(instance.RBO[m,k]) > 0:
                output.write("Cummulative backorders at retailer %s at end of time period %s is: %f\n\n" %(m,k,value(instance.RBO[m,k])))
    for m in instance.k:
        for k in instance.t:
            if value(instance.RInv[m,k]) > 0:
                output.write("Cummulative inventory at retailer %s at the end of time period %s is: %f\n\n" %(m,k,value(instance.RInv[m,k])))
    output.write("The minimum total cost is: %f" %value(instance.min_cost))
    output.close()
