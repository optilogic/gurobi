# Three banks and three projects are considered. Each bank has a rate for a loan on 
# each project. Determine the amount to borrow from each bank for financing each 
# project in order to minimize the total expenses.

from pyomo.environ import *

model = AbstractModel("Choice of loans")

#Sets and parameters

#set of banks
model.banks = Set()

#set of shops
model.shops = Set()

#rates for each shop on each bank
model.rates = Param(model.banks, model.shops, within = NonNegativeReals)

#maximum amount to be invested
model.max_invest = Param(within = NonNegativeReals)

#number of years to invest
model.numb_years = Param(within = NonNegativeReals)

#cost to open each shop
model.cost = Param(model.shops, within = NonNegativeReals)

#Variables

#variable representing the amount of money to be borrowed by each bank for a shop
model.borrow = Var(model.banks, model.shops, within = NonNegativeReals)

#Objective

#minimizing the sum of annual payments to make
def min_payments(model):
    return sum(sum(model.borrow[b,s]*(model.rates[b,s] / (1 - (1 + model.rates[b,s])**(- model.numb_years))) for s in model.shops) for b in model.banks)
model.min_payments = Objective(rule = min_payments)

#Constraints

#Every shop has to be completely financed
def each_shop_financed(model, s):
    return sum(model.borrow[b,s] for b in model.banks) == model.cost[s]
model.each_shop_financed = Constraint(model.shops, rule = each_shop_financed)

#every bank must not finance more than maximum amount allowed
def upper_bound_invest(model, b):
    return sum(model.borrow[b,s] for s in model.shops) <= model.max_invest
model.upper_bound_invest = Constraint(model.banks, rule = upper_bound_invest)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("choice_of_loans.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.banks:
    for j in instance.shops:
        if value(instance.borrow[i,j]) > 0:
            print(f'Amount of money to be borrowed by bank {i} for shop {j} is {value(instance.borrow[i,j])}')
print(f'The minimum amount of annual payments is {value(instance.min_payments)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.banks:
        for j in instance.shops:
            if value(instance.borrow[i,j]) > 0:
                output.write(f'Amount of money to be borrowed by bank {i} for shop {j} is {value(instance.borrow[i,j])}\n\n')
    output.write(f'The minimum amount of annual payments is {value(instance.min_payments)}')
    output.close()
