# A company wishes to determine the optimal locations to build
# warehouses so as the delivery demands are met at a minimum cost. Let's
# assume that a set of possible warehouse locations and a set of
# customers are given. Nevertheless, costs for serving each customer to
# each location are given. The company wishes to built at most 2
# warehouses. Facilities can be hospitals, fire stations, ambulances,
# restaurants, schools, etc.

from pyomo.environ import *

model = AbstractModel("Warehouse Location Problem")

#Sets and parameters

#set of possible warehouse locations
model.locations = Set()

#set of customer locations
model.customers = Set()

#cost of serving a customer to a location
model.cost = Param(model.locations, model.customers, within = NonNegativeReals)

#number of warehouses to built
model.numb_wh = Param(within = NonNegativeIntegers)

#Variables
model.assign = Var(model.locations, model.customers, within = Binary)
model.build = Var(model.locations, within = Binary)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(model.cost[n,m]*model.assign[n,m] for m in model.customers for n in model.locations)
model.min_cost = Objective(rule = min_cost)

#Constraints

#assign each customer to exactly one warehouse
def cust_warehouse(model, m):
    return sum(model.assign[n,m] for n in model.locations) == 1
model.cust_warehouse = Constraint(model.customers, rule = cust_warehouse)

#if a warehouse is not build, then there should no customers assigned to that warehouse
def implic_con(model, n, m):
    return model.assign[n,m] <= model.build[n]
model.implic_con = Constraint(model.locations, model.customers, rule = implic_con)

#no more than 2 warehouses should be build
def numb_warehouses(model):
    return sum(model.build[n] for n in model.locations) <= model.numb_wh
model.numb_warehouses = Constraint(rule = numb_warehouses)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("warehouse_location_1.dat")
results = solver.solve(instance)

print("Warehouses will be build in the following locations:")
for i in instance.locations:
    if value(instance.build[i]) > 0:
        print("%s" %i)
for i in instance.locations:
    for j in instance.customers:
        if value(instance.assign[i,j]) > 0:
            print("Customers in %s are served by warehouse %s" %(j,i))
print("The minimum cost is: %f" %value(instance.min_cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("Wrehouses will be build in the following locations:\n\n")
    for i in instance.locations:
        if value(instance.build[i]) > 0:
            output.write("%s\n\n" %i)
    for i in instance.locations:
        for j in instance.customers:
            if value(instance.assign[i,j]) > 0:
                output.write("Customers in %s are served by warehouse %s\n\n" %(j,i))
    output.write("The minimum cost is: %f" %value(instance.min_cost))
    output.close()
