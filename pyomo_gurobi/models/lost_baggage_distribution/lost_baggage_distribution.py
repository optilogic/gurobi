# Several airlines contract a small company with six vans to pick up
# lost or delayed baggage in an airport in London at 6 p.m. each
# evening. Every customer must have their baggage delivered by 8
# p.m. same day. Travel time between any two locations is given. The
# company needs to figure out a way every day to distribute all baggages
# while minimizing the number of vans used. Vans have no capacity
# limitation.

from pyomo.environ import *

model = AbstractModel("Lost baggage distribution")

#Sets and parameters

#set of vans
model.vans = Set()

#set of locations
model.locations = Set()

#set of arcs between locations
def arcs(model):
    return ((i,j) for i in model.locations for j in model.locations if i < j)
model.arcs = Set(dimen = 2, initialize = arcs)

#time travelling from location i to location j
model.time = Param(model.arcs, within = NonNegativeReals)

#Variables

#binary variable indicating whether van i is used
model.used = Var(model.vans, within = Binary)

#binary variable indicating whether location i is visited by van j
model.van_visit_loc = Var(model.locations, model.vans, within = Binary)

#binary variable indicating whether van k visits and goes from i to j
model.van_path = Var(model.vans, model.locations, model.locations, within = Binary)

#auxiliary variables used in subtour elimination constraints
model.y = Var(model.locations, within = NonNegativeReals)

#Objective

#minimizing the number of vans needed to complete the distribution of lost baggage
def minimize_number_of_vans(model):
    return sum(model.used[i] for i in model.vans)
model.minimize_number_of_vans = Objective(rule = minimize_number_of_vans)

#Constraints

#symmetry breaking constraints
def symm_break(model, k):
    if k == 6:
        return Constraint.Skip
    else:
        return sum(model.van_visit_loc[i,k] for i in model.locations) >= sum(model.van_visit_loc[i,k+1] for i in model.vans)
model.symm_break = Constraint(model.vans, rule = symm_break)
                                                                             
#subtour elimination constraint
def subtour_elimination(model, i, j, k):
    if i == j:
        return Constraint.Skip
    elif i == 1:
        return Constraint.Skip
    else:
        return model.y[j] >= model.y[i] + 1 - 14*(1 - model.van_path[k,i,j])
model.subtour_elimination = Constraint(model.locations, model.locations, model.vans, rule = subtour_elimination)

#each location must be visited by exactly one van
def con1(model, i):
    if i == 1:
        return Constraint.Skip
    else:   
        return sum(model.van_visit_loc[i,j] for j in model.vans) == 1
model.con1 = Constraint(model.locations, rule = con1)

#if location i is visited by van j, then van j is used
def con2(model, i, j):
    if i == 1:
        return Constraint.Skip
    else:
        return model.van_visit_loc[i,j] <= model.used[j]
model.con2 = Constraint(model.locations, model.vans, rule = con2)

#No van travels for more than 2 hours
def con3(model, k):
    return sum(model.time[i,j]*model.van_path[k,i,j] for i in model.locations for j in model.locations \
    if (i,j) in model.arcs) \
    + sum(model.time[j,i]*model.van_path[k,i,j]for i in model.locations for j in model.locations if ((j,i) in model.arcs and j != 1)) <= 120
model.con3 = Constraint(model.vans, rule = con3)    

#Heathrow(origin) is visited by every van
def con4(model, k):
    return sum(model.used[k] for k in model.vans) <= sum(model.van_visit_loc[1,k] for k in model.vans)
model.con4 = Constraint(model.vans, rule = con4)

#there is only one arc going in for every location
def con5(model, j, k):
    return model.van_visit_loc[j,k] == \
    sum(model.van_path[k,i,j] for i in model.locations if i != j)
model.con5 = Constraint(model.locations, model.vans, rule = con5)    

#there is only one arc going out for every location
def con6(model, j, k):
    return model.van_visit_loc[j,k] == \
    sum(model.van_path[k,j,i] for i in model.locations if i != j)
model.con6 = Constraint(model.locations, model.vans, rule = con6)                        


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("lost_baggage_distribution.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.vans:
    if value(instance.used[i]) > 0:
        print(f'Van {i} is used')
for i in instance.vans:
    for j in instance.locations:
        for k in instance.locations:
            if ((j,k) in instance.arcs or (k,j) in instance.arcs) and value(instance.van_path[i,j,k]) > 0:
                print(f'Van {i} travels from location {j} to location {k}')     
for i in instance.locations:
    for k in instance.vans:
        if value(instance.van_visit_loc[i,k]) > 0:
            print(f'Location {i} is visited by van {k}')
print(f'The minimum number of vans used is {value(instance.minimize_number_of_vans)}')

            
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.vans:
        if value(instance.used[i]) > 0:
            output.write(f'Van {i} is used\n\n')
    for i in instance.vans:
        for j in instance.locations:
            for k in instance.locations:
                if ((j,k) in instance.arcs or (k,j) in instance.arcs) and value(instance.van_path[i,j,k]) > 0:
                    output.write(f'Van {i} travels from location {j} to location {k}\n\n')     
    for i in instance.locations:
        for k in instance.vans:
            if value(instance.van_visit_loc[i,k]) > 0:
                output.write(f'Location {i} is visited by van {k}\n\n')
    output.write(f'The minimum number of vans used is {value(instance.minimize_number_of_vans)}')
    output.close()
