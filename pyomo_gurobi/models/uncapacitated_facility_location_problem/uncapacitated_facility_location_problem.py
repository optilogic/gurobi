# A set of possible warehouse locations along with a set of customers
# are given. Also, there is a cost associated with serving each
# customer to each possible location. It is wished to determine the
# optimal capacitated warehouse locations that will minimize the total
# cost of product delivery.

from pyomo.environ import *

model=AbstractModel("Uncapacitated Facility Location")

#Sets and Parameters

#set of customers
model.C = Set()

#set of facilities
model.F = Set()

#cost of opening facility j
model.f = Param(model.F, within=NonNegativeReals)

#cost of serving customer i at facility j
model.c = Param(model.C, model.F, within = NonNegativeReals)

#Variables

#binary variable y which is 1 if and only if facility j is open.
model.y = Var(model.F, within=Binary)

#binary variable x which is 1 if and only if customer i is assigned/served at facility j
model.x = Var(model.C, model.F, within=Binary)

#Objective

#objective function that minimizes the overall cost
def min_cost(model):
    x=0
    for i in model.C:
        for j in model.F:
            x += model.c[i,j]*model.x[i,j]
    for j in model.F:
        x += model.f[j]*model.y[j]
    return x
model.obj = Objective(rule=min_cost)

# Costraints

#every customer is assigned/served at exactly one facility
def customer_assigned_1_facility(model, i):
    return sum(model.x[i,j] for j in model.F) == 1
model.customer_assigned_1_facility = Constraint(model.C, rule=customer_assigned_1_facility)

#no customer is assigned at a closed facility
def no_customer_closed_facility(model, i, j):
    return model.x[i,j] <= model.y[j]
model.no_customer_closed_facility = Constraint(model.C, model.F, rule=no_customer_closed_facility)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("uncapacitated_facility_location_problem.dat")
results = solver.solve(instance)

print("The following facilities will open:")
for j in instance.F:
    if value(instance.y[j]) > 0:
        print("%s" %j)
for i in instance.C:
    for j in instance.F:
        if value(instance.x[i,j]) > 0:
            print("Customer %s is served at facility %s" %(i,j))
print("The minimum cost is: %f" %value(instance.obj))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    output.write("The following facilities will open:\n")
    for j in instance.F:
        if value(instance.y[j]) > 0:
            output.write("%s\n" %j)
    for i in instance.C:
        for j in instance.F:
            if value(instance.x[i,j]) > 0:
                output.write("\nCustomer %s is served at facility %s\n" %(i,j))
    output.write("\nThe minimum cost is: %f" %value(instance.obj))
    output.close()
