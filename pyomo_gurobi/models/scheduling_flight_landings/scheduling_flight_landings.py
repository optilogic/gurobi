# Ten planes are due to arrive at a single runway in an airport. 
# Every plane has an earliest arrival time and a latest arrival time. 
# Within this time window the airlines choose a target time, communicated 
# to the public as the flight arrival time. To take into account these cost 
# and to compare them a penalty per minute of early arrival and a second 
# penalty per minute of late arrival are associated with every plane. 
# The time windows and the penalties per plane are given. The problem is to 
# determine which landing schedule minimizes the total penalty subject to arrivals 
# within the given time windows and the required intervals separating any two landings.

from pyomo.environ import *

model = AbstractModel("Scheduling flight landings")

#Sets and parameters

#set of planes due to arrive at the airport
model.planes = Set()

#earliest time for each plane
model.earliest = Param(model.planes, within = NonNegativeReals)

#latest time for each plane
model.latest = Param(model.planes, within = NonNegativeReals)

#target time to land
model.target = Param(model.planes, within = NonNegativeReals)

#cost per minute for arriving early for each plane
model.early_cost = Param(model.planes, within = NonNegativeReals)

#cost per minute for arriving late for each plane
model.late_cost = Param(model.planes, within = NonNegativeReals)

#set of arcs
def arcs(model):
    return ((i,j) for i in model.planes for j in model.planes)
model.arcs = Set(dimen = 2, initialize = arcs)

#minimum distance between landing of every two planes
model.distances = Param(model.arcs, within = NonNegativeReals)

#subset of the set of arcs
model.subset_arcs = Set(within = model.planes*model.planes)

#big M value
model.M =Param(within = NonNegativeReals)

#Variables

#integer variable indicating the landing time for each plane
model.landing = Var(model.planes, within = NonNegativeIntegers)

#integer variable indicating the amount of time being early for each plane
model.early = Var(model.planes, within = NonNegativeIntegers)

#integer variable indicating the amount of time being late for each plane
model.late = Var(model.planes, within = NonNegativeIntegers)

#binary variable indicating if the landing of aircraft p precedes the landing of q
model.preceding = Var(model.subset_arcs, within = Binary)

#Objective

#minimizing the total cost for not landing on the targeted time
def min_cost(model):
    return sum(model.early_cost[p]*model.early[p] + model.late_cost[p]*model.late[p] for p in model.planes)
model.min_cost = Objective(rule = min_cost)

#Constraints

#landing time should be less than latest time
def landing_upper_bound(model, p):
    return  model.landing[p] <= model.latest[p]
model.landing__upper_bound = Constraint(model.planes, rule = landing_upper_bound)

#landing time should be bigger than earliest time
def landing_lower_bound(model, p):
    return  model.landing[p] >= model.earliest[p]
model.landing_lower_bound = Constraint(model.planes, rule = landing_lower_bound)

#separation of the landings
def sep_landings(model, p, q):
    if p > q:
        return model.landing[p] + model.distances[p,q] <= model.landing[q] + model.M * model.preceding[q,p]
    elif p < q:
        return model.landing[p] + model.distances[p,q] <= model.landing[q] + model.M * (1 - model.preceding[p,q])
    else:
        return Constraint.Skip
model.sep_landings = Constraint(model.planes, model.planes, rule = sep_landings)

#upper bound on the variable early
def early_ub(model, p):
    return model.early[p] <= model.target[p] - model.earliest[p]
model.early_ub = Constraint(model.planes, rule = early_ub)

#upper bound on the variable late
def late_ub(model, p):
    return model.late[p] <= model.latest[p] - model.target[p]
model.late_ub = Constraint(model.planes, rule = late_ub)

#link between the landing, early and late times
def time_links(model, p):
    return model.landing[p] == model.target[p] - model.early[p] + model.late[p]
model.time_links = Constraint(model.planes, rule = time_links)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("scheduling_flight_landings.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.planes:
    if value(instance.landing[i]) > 0:
        print(f'Plane {i} will be landing {value(instance.landing[i])} minutes after the start of the day')
print(f'The minimum total cost for not landing on targeted time is {value(instance.min_cost)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.planes:
        if value(instance.landing[i]) > 0:
            output.write(f'Plane {i} will be landing {value(instance.landing[i])} minutes after the start of the day\n\n')
    output.write(f'The minimum total cost for not landing on targeted time is {value(instance.min_cost)}')
    output.close()
