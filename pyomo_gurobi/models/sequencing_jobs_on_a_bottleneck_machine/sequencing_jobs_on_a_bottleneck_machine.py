# In workshops it frequently happens that a single machine determines the throughput 
# of the entire production. This machine is called the critical machine or the bottleneck. 
# A set of tasks is to be processed on a single machine. For every task i its release date 
# and duration are given. The problem is to determine the sequence of jobs so as to minimize 
# the total duration of the schedule.

from pyomo.environ import *

model = AbstractModel("Sequencing jobs on a bottleneck machine")

#Sets and parameters

#set of jobs
model.jobs = Set()

#duration of each job
model.duration = Param(model.jobs, within = NonNegativeIntegers)

#release date for each job
model.release_date = Param(model.jobs, within = NonNegativeIntegers)

#due date for each job
model.due_date = Param(model.jobs, within = NonNegativeIntegers)

#Variables

#binary variable indicating whether a job has a certain position
model.rank = Var(model.jobs, model.jobs, within = Binary)

#start time of jobs at their respective positions
model.y = Var(model.jobs, within = NonNegativeReals)

#Objective

#minimize the total time
def min_time(model):
    return model.y[7] + sum(model.duration[i]*model.rank[i,7] for i in model.jobs)
model.min_time = Objective(rule = min_time)

#Constraints

#for every position there is only one job
def one_job_per_position(model, k):
    return sum(model.rank[i,k] for i in model.jobs) == 1
model.one_job_per_position = Constraint(model.jobs, rule = one_job_per_position)

#for every job there is only one position
def one_position_per_job(model, i):
    return sum(model.rank[i,k] for k in model.jobs) == 1
model.one_position_per_job = Constraint(model.jobs, rule = one_position_per_job)

#lower bound on the release date of the job
def lower_bound_start(model, k):
    return model.y[k] >= sum(model.release_date[i]*model.rank[i,k] for i in model.jobs)
model.lower_bound_start = Constraint(model.jobs, rule = lower_bound_start)

#lower bound on the start time of the next job
def next_job_start_time(model, k):
    if k == 7:
        return Constraint.Skip
    else:
        return model.y[k+1] >= model.y[k] + sum(model.duration[i]*model.rank[i,k] for i in model.jobs)
model.next_job_start_time = Constraint(model.jobs, rule = next_job_start_time)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("sequencing_jobs_on_a_bottleneck_machine.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.jobs:
    for j in instance.jobs:
        if value(instance.rank[i,j]) > 0:
            print(f'Job {i} is ranked on the {j} position')
for i in instance.jobs:
    if value(instance.y[i]) > 0:
        print(f'Start time of the job at position {i} is {value(instance.y[i])}')
print(f'The minimum of the total time is {value(instance.min_time)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.txt', 'w')
    for i in instance.jobs:
        for j in instance.jobs:
            if value(instance.rank[i,j]) > 0:
                output.write(f'Job {i} is ranked on the {j} position\n\n')
    for i in instance.jobs:
        if value(instance.y[i]) > 0:
            output.write(f'Start time of the job at position {i} is {value(instance.y[i])}\n\n')
    output.write(f'The minimum of the total time is {value(instance.min_time)}')
    output.close()
