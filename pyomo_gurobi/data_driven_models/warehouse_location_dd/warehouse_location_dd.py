# A company wishes to determine the optimal locations to build
# warehouses so as the delivery demands are met at a minimum cost. Let's
# assume that a set of possible warehouse locations and a set of
# customers are given. Nevertheless, costs for serving each customer to
# each location are given. The company wishes to built at most 2
# warehouses. Facilities can be hospitals, fire stations, ambulances,
# restaurants, schools, etc.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['locations.txt', 'costs.txt', 'customers.txt', 'number_warehouses.txt']

FileHeaders = {}
StaticFileHeaders = {'locations.txt': ['locations'], 'customers.txt': ['customers'], 'costs.txt': ['locations', 'customers', 'cost'], 'number_warehouses.txt': ['number_of_warehouses']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('warehouse_location_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('warehouse_location_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['locations.txt', 'customers.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0].split('.')[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		datafile.write(readfiles.readline().split()[-1])
        		datafile.write(' := \n')
        		for line in readfiles:
        			line = line.rstrip('\n')
        			for i in line.split('\t'):
        				if i != line.split('\t')[-1]:
        					datafile.write("'")
        					datafile.write(i)
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(i)
        					datafile.write('\n')
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')


model = AbstractModel("Warehouse Location Problem")

#Sets and parameters

#set of possible warehouse locations
model.locations = Set()

#set of customer locations
model.customers = Set()

#cost of serving a customer to a location
model.cost = Param(model.locations, model.customers, within = NonNegativeReals)

#number of warehouses to built
model.number_of_warehouses = Param(within = NonNegativeIntegers)

#Variables
model.assign = Var(model.locations, model.customers, within = Binary)
model.build = Var(model.locations, within = Binary)

#Objective

#minimizing the total cost
def min_cost(model):
    return sum(model.cost[n,m]*model.assign[n,m] for m in model.customers for n in model.locations)
model.min_cost = Objective(rule = min_cost)

#Constraints

#assign each customer to exactly one warehouse
def cust_warehouse(model, m):
    return sum(model.assign[n,m] for n in model.locations) == 1
model.cust_warehouse = Constraint(model.customers, rule = cust_warehouse)

#if a warehouse is not build, then there should no customers assigned to that warehouse
def implic_con(model, n, m):
    return model.assign[n,m] <= model.build[n]
model.implic_con = Constraint(model.locations, model.customers, rule = implic_con)

#no more than 2 warehouses should be build
def numb_warehouses(model):
    return sum(model.build[n] for n in model.locations) <= model.number_of_warehouses
model.numb_warehouses = Constraint(rule = numb_warehouses)


#Python script for printing the solution in the terminal
solver = SolverFactory('gurobi_direct')
instance = model.create_instance("warehouse_location_dd.dat")
results = solver.solve(instance)

print("Warehouses will be build in the following locations:")
for i in instance.locations:
    if value(instance.build[i]) > 0:
        print("%s" %i)
for i in instance.locations:
    for j in instance.customers:
        if value(instance.assign[i,j]) > 0:
            print("Customers in %s are served by warehouse %s" %(j,i))
print("The minimum cost is: %f" %value(instance.min_cost))

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    output.write("Wrehouses will be build in the following locations:\n\n")
    for i in instance.locations:
        if value(instance.build[i]) > 0:
            output.write("%s\n\n" %i)
    for i in instance.locations:
        for j in instance.customers:
            if value(instance.assign[i,j]) > 0:
                output.write("Customers in %s are served by warehouse %s\n\n" %(j,i))
    output.write("The minimum cost is: %f" %value(instance.min_cost))
    output.close()
