# A set of possible warehouse locations along with a set of customers
# are given. Also, there is a cost associated with serving each
# customer to each possible location. It is wished to determine the
# optimal capacitated warehouse locations that will minimize the total
# cost of product delivery.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['customers.txt', 'facilities.txt', 'demand.txt', 'facility_cost.txt', 'transportation_cost.txt', 'flow_capacities.txt']

FileHeaders = {}
StaticFileHeaders = {'customers.txt': ['customers'], 'facilities.txt': ['facilities'], 'demand.txt': ['customers', 'demand'], 'facility_cost.txt': ['facilities', 'facility_cost'], 'transportation_cost.txt': ['customers', 'facilities', 'transportation_cost'], 'flow_capacities.txt': ['facilities', 'flow_capacities']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('capacitated_facility_location_problem_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('capacitated_facility_location_problem_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['customers.txt', 'facilities.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		paramnames = txtfile.rsplit('.', 1)[0]
        		datafile.write(paramnames)
        		datafile.write(' := \n')
        		next(readfiles)
        		for line in readfiles:
        			line = line.rstrip('\n')
        			for j in range(0, len(line.split('\t'))):
        				if j != len(line.split('\t')) - 1:
        					datafile.write("'")
        					datafile.write(line.split('\t')[j])
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(line.split('\t')[len(line.split('\t')) - 1])
        					datafile.write('\n')	
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')

model = AbstractModel("Capacitated facility location problem")

#Sets and parameters

#Set of customers
model.customers = Set()

#Set of facilities
model.facilities = Set()

#demand of each customer
model.demand = Param(model.customers, within=NonNegativeReals)

#activation cost f_j
model.facility_cost = Param(model.facilities, within=NonNegativeReals)

#transportation cost per unit from j to i
model.transportation_cost = Param(model.facilities, model.customers, within = NonNegativeReals)

#maximum amount that can flow from facility j
model.flow_capacities = Param(model.facilities, within=NonNegativeReals)

#Variables

#The variable representing the amount serviced from facility j to demand point i
model.x_ij = Var(model.facilities, model.customers, within=NonNegativeIntegers)

#Binary variable y_j which is 1 if and only if facility j is established
model.y_j = Var(model.facilities, within=Binary)

#Objective
#The objective of the problem is to minimize the sum of facility activation costs and transportation costs. 
def min_cost(model):
    x=0
    for i in model.customers:
        for j in model.facilities:
            x += model.transportation_cost[j,i]*model.x_ij[j,i]
    for j in model.facilities:
        x += model.facility_cost[j]*model.y_j[j]
    return x
model.obj=Objective(rule=min_cost)

#Constraints

#The first constraints require that each customer's demand must be satisfied
def demand_satisfied(model, i): 
    return sum(model.x_ij[j,i] for j in model.facilities) == model.demand[i]
model.demand_satisfied = Constraint(model.customers, rule=demand_satisfied)

#The capacity of each facility j is limited by the second constraints: if facility j is activated, its capacity restriction is observed; if it is not activated, the demand satisfied by j is zero
def big_M(model, j):
    return sum(model.x_ij[j,i] for i in model.customers) <= model.flow_capacities[j]*model.y_j[j]
model.big_M = Constraint(model.facilities, rule = big_M)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("capacitated_facility_location_problem_dd.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.facilities:
    if value(instance.y_j[i]) > 0:
        print(f'Facility {i} will be established')
for i in instance.facilities:
    for j in instance.customers:
        if value(instance.x_ij[i,j]) > 0:
            print(f'{value(instance.x_ij[i,j])} customers are serviced from facility {i} to demand point {j}')
print(f'The minimum total cost is {value(instance.obj)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.facilities:
        if value(instance.y_j[i]) > 0:
            output.write(f'Facility {i} will be established\n\n')
    for i in instance.facilities:
        for j in instance.customers:
            if value(instance.x_ij[i,j]) > 0:
                output.write(f'{value(instance.x_ij[i,j])} customers are serviced from facility {i} to demand point {j}\n\n')
    output.write(f'The minimum total cost is {value(instance.obj)}')
    output.close()
