from pyomo.environ import *
import numpy as np
import matplotlib.pyplot as plt
import os

Files = []
StaticFilenames = ['nodes.txt', 'demand_nodes.txt', 'arcs.txt', 'average_demand.txt', 'guaranteed_service_time.txt', 'lead_times.txt', 'max_replenishment_time.txt', 'service_level.txt', 'standard_deviation.txt', 'unit_inventory_holding_cost.txt']

FileHeaders = {}
StaticFileHeaders = {'nodes.txt': ['nodes'], 'demand_nodes.txt': ['demand_nodes'], 'arcs.txt': ['nodes', 'nodes'], 'average_demand.txt': ['nodes', 'average_demand'], 'guaranteed_service_time.txt': ['demand_nodes', 'guaranteed_service_time'], 'lead_times.txt': ['nodes', 'lead_times'], 'max_replenishment_time.txt': ['nodes', 'max_replenishment_time'], 'service_level.txt': ['service_level'], 'standard_deviation.txt': ['nodes', 'standard_deviation'], 'unit_inventory_holding_cost.txt': ['nodes', 'unit_inventory_holding_cost']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
        print('The file names inputted do not match the ones listed in StaticFilenames.')
        quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('meio_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('meio_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['nodes.txt', 'demand_nodes.txt', 'arcs.txt']:
            with open(txtfile, 'r') as readfiles:
                datafile.write('set ')
                setnames = txtfile.rsplit('.', 1)[0]
                datafile.write(setnames)
                datafile.write(' := \n')
                next(readfiles)
                for i in readfiles.read().splitlines():
                    for j in i.split():
                        datafile.write("'")
                        datafile.write(j)
                        datafile.write("'")
                        datafile.write(' ')
                    datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')
        else:
            datafile.write('param ')
            with open(txtfile, 'r') as readfiles:
                paramnames = txtfile.rsplit('.', 1)[0]
                datafile.write(paramnames)
                datafile.write(' := \n')
                next(readfiles)
                for line in readfiles:
                    line = line.rstrip('\n')
                    for j in range(0, len(line.split('\t'))):
                        if j != len(line.split('\t')) - 1:
                            datafile.write("'")
                            datafile.write(line.split('\t')[j])
                            datafile.write("'")
                            datafile.write(' ')
                        else:
                            datafile.write(line.split('\t')[
                                           len(line.split('\t')) - 1])
                            datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')



model = AbstractModel('MEIO')

# Sets and Parameters

# Sets

# set of nodes
model.nodes = Set()

# set of demand nodes
model.demand_nodes = Set(within = model.nodes)

# set of arcs
model.arcs = Set(within = model.nodes * model.nodes)

# Parameters

# lead times for each node
model.lead_times = Param(model.nodes, within = NonNegativeReals)

# maximum replenishment time for each node
model.max_replenishment_time = Param(model.nodes, within = NonNegativeReals)

# parameter indicating the service level
model.service_level = Param(within = NonNegativeReals)

# average demand for each node
model.average_demand = Param(model.nodes, within = NonNegativeReals)

# standard deviation of the demand for each node
model.standard_deviation = Param(model.nodes, within = NonNegativeReals)

# service time at demand nodes which is guaranteed to the customers
model.guaranteed_service_time = Param(model.demand_nodes, within = NonNegativeReals)

# cost of unit inventory holding for each node
model.unit_inventory_holding_cost = Param(model.nodes, within = NonNegativeReals)

# Variables

# variable representing the service time quoted by each node
model.service_time = Var(model.nodes, within = NonNegativeReals)

# variable representing the inbound service time for each node
model.inbound_service_time = Var(model.nodes, within = NonNegativeReals)


# Obtaining piecewise linear approximation of the safety stock level

inst = model.create_instance('meio_dd.dat')

def safety_stock(x, j):
    return inst.service_level * inst.standard_deviation[j] * np.sqrt(x)

# constructing the breakpoints for each node
breakpoints = {}
dictionary1 = {}

for j in inst.nodes:
    breakpoints[j] = []
    dictionary1[j] = []
    x = np.arange(0, inst.max_replenishment_time[j] + 1, 1)
    for i in x:
        breakpoints[j].append(i)
        dictionary1[j].append((i, safety_stock(i,j)))       
    plt.plot(x, safety_stock(x,j))
plt.xlabel('Net Replenishment Lead Time')
plt.ylabel('Safety Stock Level')
plt.title('Inventory')
plt.savefig('Safety Stock Placement.pdf')

# calculating the slope and y-intercept of each line for each node
dictionary2 = {}
dictionary3 = {}
for j in inst.nodes:
    dictionary2[j] = []
    dictionary3[j] = []
    for i in range(len(dictionary1[j]) - 1):
        dictionary2[j].append((dictionary1[j][i+1][1] - dictionary1[j][i][1]) / (dictionary1[j][i+1][0] - dictionary1[j][i][0]))
        dictionary3[j].append(dictionary1[j][i][1] - dictionary1[j][i][0] * dictionary2[j][i])


# setting maximum replenishment time in a list        
max_repl_time = []
for i in inst.nodes:
    max_repl_time.append(inst.max_replenishment_time[i])    
max_repl_time.sort()

# pairs consisting of nodes and breakpoints
pairs = [(i,j) for i in inst.nodes for j in breakpoints[i] if j != breakpoints[i][-1]]

model.u = Var(pairs, within = Binary)
model.z = Var(pairs, within = NonNegativeReals)
        
# Objective
def min_cost(model):
    return sum(model.unit_inventory_holding_cost[i] * (sum(dictionary3[i][j] * model.u[i,j] + dictionary2[i][j] * model.z[i,j] for j in breakpoints[i] if (i,j) in pairs)) for i in model.nodes)
model.objective = Objective(rule = min_cost)

# Constraints

# lower bound on the variable z on each linear piece
def lb(model, n, p):
    return model.z[n,p] >= dictionary1[n][p][0] * model.u[n,p]
model.lb = Constraint(pairs, rule = lb)

# upper bound on the variable z on each linear piece
def ub(model, n, p):
    return model.z[n,p] <= dictionary1[n][p][1] * model.u[n,p]
model.ub = Constraint(pairs, rule = ub)

# for each node at most one linear piece is chosen
def at_most_one_piece(model, n):
    return sum(model.u[n,j] for j in breakpoints[i] if (n,j) in pairs) <= 1
model.at_most_one_piece = Constraint(model.nodes, rule = at_most_one_piece)

# the net replenishment lead time at each node is equal to the sum of variables z over all linear pieces for that node
def NRLT_equal_to_sum_z(model, n):
    return sum(model.z[n,p] for p in breakpoints[n] if (n,p) in pairs) == model.inbound_service_time[n] + model.lead_times[n] - model.service_time[n]
model.NRLT_equal_to_sum_z = Constraint(model.nodes, rule = NRLT_equal_to_sum_z)

# the inbound service time of each node j is the maximum of all service time of the nodes i supplying j
def inbound_service_time_bounds(model, i, j):
    if (i,j) in model.arcs:
        return model.inbound_service_time[j] >= model.service_time[i]
    else:
        return Constraint.Skip
model.inbound_service_time_bounds = Constraint(model.nodes, model.nodes, rule = inbound_service_time_bounds)

# upper bound on the service time at the demand nodes
def service_time_ub_demand_nodes(model, d):
    return model.service_time[d] <= model.guaranteed_service_time[d]
model.service_time_ub_demand_nodes = Constraint(model.demand_nodes, rule = service_time_ub_demand_nodes)

# net replenishment lead time must be nonnegative
def nonnegative_nrlt(model, i):
    return model.inbound_service_time[i] + model.lead_times[i] >= model.service_time[i]
model.nonnegative_nrlt = Constraint(model.nodes, rule = nonnegative_nrlt)

# Creating and solving an instance

solver = SolverFactory('gurobi_direct')
instance = model.create_instance('meio_dd.dat')
results = solver.solve(instance)


for i in instance.nodes:
    if value(instance.service_time[i]) > 0:
        print("Service time for node %s is: %d" %(i, value(instance.service_time[i])))
for i in instance.nodes:
    if value(instance.inbound_service_time[i]) > 0:
        print("Inbound service time for node %s is: %d" %(i,value(instance.inbound_service_time[i])))
for i in instance.nodes:
    print("Net replenishment lead time for node %s is %f" %(i, instance.lead_times[i] + value(instance.inbound_service_time[i]) - value(instance.service_time[i])))
print("The minimum holding inventory cost is: %f" %value(instance.objective))
            
#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.nodes:
        if value(instance.service_time[i]) > 0:
            output.write("Service time for node %s is: %d \n" %(i,value(instance.service_time[i])))
    for i in instance.nodes:
        if value(instance.inbound_service_time[i]) > 0:
            output.write("Inbound service time for node %s is: %d \n" %(i,value(instance.inbound_service_time[i])))
    for i in instance.nodes:
        output.write("Net replenishment lead time for node %s is %f \n" %(i, instance.lead_times[i] + value(instance.inbound_service_time[i]) - value(instance.service_time[i])))
    output.write("The minimum holding inventory cost is: %f" %value(instance.objective))
    output.close()
