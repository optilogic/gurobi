# Suppose there are 6 different shares available for anybody who wants to 
# invest on them for the next 6 months. The shares are of two different 
# categories; three shares are in technology while the other three are 
# non-technological. Also, each share belongs to a certain country 
# (European and non-European) and has an expected return of investment given. 
# Suppose a wealthy person would like to invest $100.000 and for each share 
# the amount to be invested must fall within a given range. Furthermore, the 
# person would like to invest half of the investment in countries of his favorite 
# continent (Europe in this case) and no more than 30% of the capital in technology. 
# This problem can be modelled as a Linear Program whose solution provides the optimal 
# way to choose different number of shares maximizing the total expected return of 
# investment on the top of 6 months time period.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['shares.txt', 'technology_type_investments.txt', 'european_shares.txt', 'capital.txt', 'minimum_investment.txt', 'maximum_investment.txt', 'return_of_investment.txt']

FileHeaders = {}
StaticFileHeaders = {'shares.txt': ['shares'], 'technology_type_investments.txt': ['technology_type_investments'], 'european_shares.txt': ['european_shares'], 'capital.txt': ['capital'], 'minimum_investment.txt': ['minimum_investment'], 'maximum_investment.txt': ['maximum_investment'], 'return_of_investment.txt': ['shares', 'return_of_investment']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
    	print('The file names inputted do not match the ones listed in StaticFilenames.')
    	quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('portfolio_selection_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('portfolio_selection_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['shares.txt', 'technology_type_investments.txt', 'european_shares.txt']:
        	with open(txtfile, 'r') as readfiles:
            		datafile.write('set ')
            		setnames = txtfile.rsplit('.', 1)[0]
            		datafile.write(setnames)
            		datafile.write(' := \n')
            		next(readfiles)
            		for i in readfiles.read().splitlines():
            			for j in i.split():
            				datafile.write("'")
            				datafile.write(j)
            				datafile.write("'")
            				datafile.write(' ')
            			datafile.write('\n')
            		datafile.write(';')
            		for i in range(2):
            			datafile.write('\n')
        else:
        	datafile.write('param ')
        	with open(txtfile, 'r') as readfiles:
        		paramnames = txtfile.rsplit('.', 1)[0]
        		datafile.write(paramnames)
        		datafile.write(' := \n')
        		next(readfiles)
        		for line in readfiles:
        			line = line.rstrip('\n')
        			for j in range(0, len(line.split('\t'))):
        				if j != len(line.split('\t')) - 1:
        					datafile.write("'")
        					datafile.write(line.split('\t')[j])
        					datafile.write("'")
        					datafile.write(' ')
        				else:
        					datafile.write(line.split('\t')[len(line.split('\t')) - 1])
        					datafile.write('\n')	
        		datafile.write(';')
        		for i in range(2):
        			datafile.write('\n')

model = AbstractModel("Portfolio selection")

#Sets and parameters

#set of shares
model.shares = Set()

#the amount of money to be invested
model.capital = Param(within = NonNegativeReals)

#set of technology type investments
model.technology_type_investments = Set()

#set of shares belonging to Europe
model.european_shares = Set()

#expected return of investment per share
model.return_of_investment = Param(model.shares, within = NonNegativeReals)

#minimum amount of money to be invested in each share
model.minimum_investment = Param(within = NonNegativeReals)

#maximum amount of money to be invested in each share
model.maximum_investment = Param(within = NonNegativeReals)

#Variables

#variable indicating the amount of money to be invested in share s
model.invest_s = Var(model.shares, within = NonNegativeReals)

#Objective

#maximizing the return of investments over all shares
def max_roi(model):
    return sum(model.return_of_investment[s]*model.invest_s[s] for s in model.shares)
model.max_roi = Objective(rule = max_roi, sense = maximize)

#Constraints

#lower bound on the investment of each share
def lower_bound(model, s):
    return model.minimum_investment <= model.invest_s[s]
model.lower_bound = Constraint(model.shares, rule = lower_bound)

# upper bound on the investment of each share
def upper_bound(model, s):
    return model.maximum_investment >= model.invest_s[s]
model.upper_bound = Constraint(model.shares, rule = upper_bound)

#all the capital will be invested
def capital_invested(model):
    return sum(model.invest_s[s] for s in model.shares) == model.capital
model.capital_invested = Constraint( rule = capital_invested)

#limit on investment on technology
def tech_invest_limit(model):
    return sum(model.invest_s[s] for s in model.technology_type_investments) <= 0.3*model.capital
model.tech_invest_limit = Constraint(rule = tech_invest_limit)

#limit on investment on europian based shares
def eu_based_shares_invest(model):
    return sum(model.invest_s[s] for s in model.european_shares) >= 0.5*model.capital
model.eu_based_shares_invest = Constraint(rule = eu_based_shares_invest)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("portfolio_selection_dd.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.shares:
        if value(instance.invest_s[i]) > 0:
            print(f'The amount of money to be invested in share {i} is {value(instance.invest_s[i])}')
print(f'The maximum return of investments over all shares is {value(instance.max_roi)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.shares:
        if value(instance.invest_s[i]) > 0:
            output.write(f'The amount of money to be invested in share {i} is {value(instance.invest_s[i])}\n\n')
    output.write(f'The maximum return of investments over all shares is {value(instance.max_roi)}')
    output.close()
