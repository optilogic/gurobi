# A sheet metal workshop cuts pieces of sheet metal from large rectangular sheets 
# of 48 decimeters × 96 decimeters. It has received an order for 8 rectangular pieces 
# of 36 dm × 50 dm, 13 sheets of 24 dm × 36 dm, 5 sheets of 20 dm × 60 dm, and 15 sheets 
# of 18 dm × 30 dm. The problem is to determine how this order can be satisfied by using 
# the least number of large sheets.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['patterns.txt', 'sizes.txt', 'cost.txt', 'demand.txt', 'amounts.txt']

FileHeaders = {}
StaticFileHeaders = {'patterns.txt': ['patterns'], 'sizes.txt': ['sizes'], 'cost.txt': ['cost'], 'demand.txt': ['sizes', 'demand'], 'amounts.txt': ['sizes', 'patterns', 'amounts']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
        print('The file names inputted do not match the ones listed in StaticFilenames.')
        quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('cutting_sheet_metal_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('cutting_sheet_metal_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['patterns.txt', 'sizes.txt']:
            with open(txtfile, 'r') as readfiles:
                datafile.write('set ')
                setnames = txtfile.rsplit('.', 1)[0]
                datafile.write(setnames)
                datafile.write(' := \n')
                next(readfiles)
                for i in readfiles.read().splitlines():
                    for j in i.split():
                        datafile.write("'")
                        datafile.write(j)
                        datafile.write("'")
                        datafile.write(' ')
                    datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')
        else:
            datafile.write('param ')
            with open(txtfile, 'r') as readfiles:
                paramnames = txtfile.rsplit('.', 1)[0]
                datafile.write(paramnames)
                datafile.write(' := \n')
                next(readfiles)
                for line in readfiles:
                    line = line.rstrip('\n')
                    for j in range(0, len(line.split('\t'))):
                        if j != len(line.split('\t')) - 1:
                            datafile.write("'")
                            datafile.write(line.split('\t')[j])
                            datafile.write("'")
                            datafile.write(' ')
                        else:
                            datafile.write(line.split('\t')[
                                           len(line.split('\t')) - 1])
                            datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')

model = AbstractModel("Cutting Sheet Metal")

#Sets and parameters

#set of patterns needed
model.patterns = Set()

#set of different sizes
model.sizes = Set()

#demand for each size
model.demand = Param(model.sizes, within = NonNegativeIntegers)

#cost for using pattern p
model.cost = Param(within = NonNegativeIntegers)

#number of size s in pattern p
model.amounts = Param(model.sizes, model.patterns, within = NonNegativeIntegers)

#Variables

#integer variable indicating the number of times a sheet metal is cut using pattern p
model.number_cut_p = Var(model.patterns, within = NonNegativeIntegers)

#Objective

#minimizing the number of large sheets needed
def min_sheets(model):
    return sum(model.cost*model.number_cut_p[p] for p in model.patterns)
model.min_sheets = Objective(rule = min_sheets)

#Constraints

#demand satisfaction constraints
def demand_satisfied(model, s):
    return sum(model.amounts[s,p]*model.number_cut_p[p] for p in model.patterns) >= model.demand[s]
model.demand_satisfied = Constraint(model.sizes, rule = demand_satisfied)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("cutting_sheet_metal_dd.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for i in instance.patterns:
        if value(instance.number_cut_p[i]) > 0:
            print(f'Pattern {i} is used {value(instance.number_cut_p[i])} times to cut a large sheet metal')
for i in instance.sizes:
    for j in instance.patterns:
        if value(instance.amounts[i,j]) > 0:
            print(f'Size {i} appears {value(instance.amounts[i,j])} times in pattern {j}')
print(f'The minimum number of large sheets needed is {value(instance.min_sheets)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.patterns:
        if value(instance.number_cut_p[i]) > 0:
            output.write(f'Pattern {i} is used {value(instance.number_cut_p[i])} times to cut a large sheet metal\n\n')
    for i in instance.sizes:
        for j in instance.patterns:
            if value(instance.amounts[i,j]) > 0:
                output.write(f'Size {i} appears {value(instance.amounts[i,j])} times in pattern {j}\n\n')
    output.write(f'The minimum number of large sheets needed is {value(instance.min_sheets)}')
    output.close()
