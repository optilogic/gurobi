# Consider a set of customer, a set of time periods and a set possible
# facility locations. Each facility is assigned a cost for opening and
# there is a cost associated with serving each customer to each possible
# location. In each time period it is decided whether to open new
# facilites so as to comply with the minimum number of customers needed
# to serve at that time period. The objective is to minimize the total
# cost of customer service plus the cost of opening facilites over the
# time horizon, while setting a number of new facilities over a finite
# time horizon so as to cover dynamically the demand of a given set of
# customers.

from pyomo.environ import *
import os

Files = []
StaticFilenames = ['customers.txt', 'facilities.txt', 'time_periods.txt', 'arcs.txt', 'costs.txt', 'facility_cost.txt', 'customers_number.txt', 'number_facilities.txt']

FileHeaders = {}
StaticFileHeaders = {'customers.txt': ['customers'], 'facilities.txt': ['facilities'], 'time_periods.txt': ['time_periods'], 'arcs.txt': ['customers', 'facilities'], 'costs.txt': ['customers', 'facilities', 'time_periods', 'costs'], 'facility_cost.txt': ['facilities', 'time_periods', 'facility_cost'], 'customers_number.txt': ['time_periods', 'customers_number'], 'number_facilities.txt': ['time_periods', 'number_facilities']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation():
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
        print('The file names inputted do not match the ones listed in StaticFilenames.')
        quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('multi_period_incremental_service_facility_location_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('multi_period_incremental_service_facility_location_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['customers.txt', 'facilities.txt', 'time_periods.txt', 'arcs.txt']:
            with open(txtfile, 'r') as readfiles:
                datafile.write('set ')
                setnames = txtfile.rsplit('.', 1)[0]
                datafile.write(setnames)
                datafile.write(' := \n')
                next(readfiles)
                for i in readfiles.read().splitlines():
                    for j in i.split():
                        datafile.write("'")
                        datafile.write(j)
                        datafile.write("'")
                        datafile.write(' ')
                    datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')
        else:
            datafile.write('param ')
            with open(txtfile, 'r') as readfiles:
                paramnames = txtfile.rsplit('.', 1)[0]
                datafile.write(paramnames)
                datafile.write(' := \n')
                next(readfiles)
                for line in readfiles:
                    line = line.rstrip('\n')
                    for j in range(0, len(line.split('\t'))):
                        if j != len(line.split('\t')) - 1:
                            datafile.write("'")
                            datafile.write(line.split('\t')[j])
                            datafile.write("'")
                            datafile.write(' ')
                        else:
                            datafile.write(line.split('\t')[
                                           len(line.split('\t')) - 1])
                            datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')

model = AbstractModel('Multi-period-incremental-service-facility-location')

#Sets and Parameters

#Set of customers 
model.customers = Set()

#Set of possible locations for facilities
model.facilities = Set()

#Set of time periods
model.time_periods = Set(ordered = True)

#Minimum number of customers that must be served at period t
model.customers_number = Param( model.time_periods, within = NonNegativeIntegers)

#Number of facilities that must be opened at period t
model.number_facilities = Param( model.time_periods, within = NonNegativeIntegers)

#Arcs: Customer i is served at facility j
model.arcs = Set(within = model.customers * model.facilities)

#Assignment value of allocating customer i to facility j at time period t
model.costs = Param(model.customers, model.facilities, model.time_periods, within=NonNegativeReals)

#Total cost of facility j being established at time period t
model.facility_cost = Param(model.facilities, model.time_periods, within=NonNegativeReals)

#Variables

#binary variable which is 1 if customer i is assigned at facility j at time period t
model.x_ij_t = Var(model.customers, model.facilities, model.time_periods, domain=Binary)

#binary variable which is 1 if facility j is opened at time period t
model.y_j_t = Var(model.facilities, model.time_periods, domain=Binary)

#Objective

#objective function minimizing the total operation costs.
def min_cost(model):
    x=0
    for i in model.customers:
        for j in model.facilities:
            for t in model.time_periods:
                x += model.costs[i,j,t]*model.x_ij_t[i,j,t]
    for j in model.facilities:
        for t in model.time_periods:
            x += model.facility_cost[j,t]*model.y_j_t[j,t]
    return x        
model.obj = Objective(rule=min_cost)

#Constraints

#Covering constraints that impose that in each time period the minimum required number of customers are being served
def cover_custom(model, t):
    return sum(sum(model.x_ij_t[i,j,t] for i in model.customers) for j in model.facilities) >= model.customers_number[t]
model.cover = Constraint(model.time_periods, rule=cover_custom)

#Each customer is assigned to at most one facility in each period.
def assign_custom(model, i ,t):
    return sum(model.x_ij_t[i,j,t] for j in model.facilities) <= 1
model.assign_custom = Constraint(model.customers, model.time_periods, rule=assign_custom)

#Guarantee that once that a customer is served at a given period he/she will be served in any subsequent period.
def conseq_served(model, i, t):
    if t == model.time_periods[1]:
        return Constraint.Skip
    else:
        return sum(model.x_ij_t[i,j,model.time_periods[int(t)-1]] for j in model.facilities) <= sum(model.x_ij_t[i,j,model.time_periods[int(t)]] for j in model.facilities)
model.conseq_served = Constraint(model.customers, model.time_periods, rule=conseq_served)

#All customers are served at the end of the planning horizon. The constant 2 corresponds to the cardinality of the set of time horizons.
def all_custom_served(model, i):
    return sum(model.x_ij_t[i,j,model.time_periods[2]] for j in model.facilities) == 1
model.all_custom_served = Constraint(model.customers, rule=all_custom_served)

#Customers are assigned only to open facilities
def custom_assign_open_facility(model, i, j, t):
    return model.x_ij_t[i,j,model.time_periods[int(t)]] <= sum(model.y_j_t[j,model.time_periods[k]] for k in range(1,int(t)+1))
model.custom_assign_open_facility = Constraint(model.customers, model.facilities, model.time_periods, rule=custom_assign_open_facility)

#Exactly p_t facilities are opened in each time period 
def numb_open_facilities_t(model, t):
    return sum(model.y_j_t[j,t] for j in model.facilities) == model.number_facilities[t]
model.numb_open_facilities_t = Constraint(model.time_periods, rule=numb_open_facilities_t)

#A facility can be opened, at most, once.
def open_facility_once(model, j):
    return sum(model.y_j_t[j,t] for t in model.time_periods) <= 1
model.open_facility_once = Constraint(model.facilities, rule = open_facility_once)

solver = SolverFactory('gurobi_direct')
instance = model.create_instance("multi_period_incremental_service_facility_location_dd.dat")
results = solver.solve(instance)


#Python Script for printing the solution in the terminal
for i in instance.facilities:
    for j in instance.time_periods:
        if value(instance.y_j_t[i,j]) > 0:
            print(f'Facility {i} will open at time period {j}')
for i in instance.customers:
    for j in instance.facilities:
        for k in instance.time_periods:
            if value(instance.x_ij_t[i,j,k]) > 0:
                print(f'Customer {i} will be served at facility {j} during time period {k}')
print(f'The minimum total operation cost is {value(instance.obj)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for i in instance.facilities:
        for j in instance.time_periods:
            if value(instance.y_j_t[i,j]) > 0:
                output.write(f'Facility {i} will open at time period {j}\n\n')
    for i in instance.customers:
        for j in instance.facilities:
            for k in instance.time_periods:
                if value(instance.x_ij_t[i,j,k]) > 0:
                    output.write(f'Customer {i} will be served at facility {j} during time period {k}\n\n')
    output.write(f'The minimum total operation cost is {value(instance.obj)}')
    output.close()
