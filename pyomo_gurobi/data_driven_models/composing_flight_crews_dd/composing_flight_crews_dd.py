# An airline has several pilots speaking different languages and each pilot is trained 
# for some aircraft types. In order to fly a plane a pilot and a co-pilot compatible 
# in terms of language and aircraft knowledge are needed. Rating of pilots in terms of 
# language knowledge and aircraft type knowledge are given. A valid flight crew consists 
# of two pilots that both have each at least 10/20 for the same language and 10/20 on the 
# same aircraft type. The problem is to determine whether all pilots can fly simultaneously.

from pyomo.environ import *
import os
import os.path

Files = []
StaticFilenames = ['pilots.txt', 'compatible_pairs_of_pilots.txt', 'maximum_score.txt']

FileHeaders = {}
StaticFileHeaders = {'pilots.txt': ['pilots'], 'compatible_pairs_of_pilots.txt': ['pilots', 'pilots'], 'maximum_score.txt': ['pilots', 'pilots', 'maximum_score']}

file_path = os.path.abspath(__file__)
directory = os.path.split(file_path)[0]

def file_creation(): 	
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            Files.append(filename)
    count = 0
    for i in Files:
        if i in StaticFilenames:
            count += 1
    if len(StaticFilenames) == count:
        return True
    else:
        print('The file names inputted do not match the ones listed in StaticFilenames.')
        quit()


def headers_creation():
    for filename in Files:
        with open(filename, 'r') as f:
            FileHeaders[filename] = []
            for i in f.readline().split():
                FileHeaders[filename].append(i)
    count = 0
    for files in Files:
        count1 = 0
        for i in FileHeaders[files]:
            if i in StaticFileHeaders[files]:
                count1 += 1
        if len(StaticFileHeaders[files]) == count1:
            count += 1
    if count == len(StaticFileHeaders):
        return True
    else:
        print('The headers of inputted files do not match the ones listed in StaticFileHeaders.')
        quit()


file_creation()
headers_creation()


with open('composing_flight_crews_dd.dat', 'w') as createfile:
    createfile.write("#Data File \n\n")

with open('composing_flight_crews_dd.dat', 'a') as datafile:
    for txtfile in Files:
        if txtfile in ['pilots.txt', 'compatible_pairs_of_pilots.txt']:
            with open(txtfile, 'r') as readfiles:
                datafile.write('set ')
                setnames = txtfile.rsplit('.', 1)[0]
                datafile.write(setnames)
                datafile.write(' := \n')
                next(readfiles)
                for i in readfiles.read().splitlines():
                    for j in i.split():
                        datafile.write("'")
                        datafile.write(j)
                        datafile.write("'")
                        datafile.write(' ')
                    datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')
        else:
            datafile.write('param ')
            with open(txtfile, 'r') as readfiles:
                paramnames = txtfile.rsplit('.', 1)[0]
                datafile.write(paramnames)
                datafile.write(' := \n')
                next(readfiles)
                for line in readfiles:
                    line = line.rstrip('\n')
                    for j in range(0, len(line.split('\t'))):
                        if j != len(line.split('\t')) - 1:
                            datafile.write("'")
                            datafile.write(line.split('\t')[j])
                            datafile.write("'")
                            datafile.write(' ')
                        else:
                            datafile.write(line.split('\t')[
                                           len(line.split('\t')) - 1])
                            datafile.write('\n')
                datafile.write(';')
                for i in range(2):
                    datafile.write('\n')

model = AbstractModel("Composing flight crews")

#Sets and parameters

#set of pilots
model.pilots = Set()

#set of arcs connecting compatible pilots
model.compatible_pairs_of_pilots = Set(within = model.pilots*model.pilots)

#maximum score for each compatible pair
model.maximum_score = Param(model.compatible_pairs_of_pilots, within = NonNegativeIntegers)

#Variables

#binary variable indicating whether two compatible pilots will fly together
model.fly = Var(model.compatible_pairs_of_pilots, within = Binary)

#Objective

#maximizing the total score
def max_total_score(model):
    return sum(model.maximum_score[i,j]*model.fly[i,j] for i,j in model.compatible_pairs_of_pilots)
model.max_total_score = Objective(rule = max_total_score, sense = maximize)

#Constraints

#every pilot i is part of at most one pair
def pilot_in_at_most_1_pair(model, k):
        return sum(model.fly[i,j] for (i,j) in model.compatible_pairs_of_pilots if i == k or j ==k ) <= 1 
model.pilot_in_at_most_1_pair = Constraint(model.pilots, rule = pilot_in_at_most_1_pair)


solver = SolverFactory('gurobi_direct')
instance = model.create_instance("composing_flight_crews_dd.dat")
results = solver.solve(instance)

#Python Script for printing the solution in the terminal
for (i,j) in instance.compatible_pairs_of_pilots:
    if value(instance.fly[i,j]) > 0:
        print(f'Pilot {i} and pilot {j} will fly together')
print(f'The maximum total score is {value(instance.max_total_score)}')

#Python Script for writing the solution while checking the termination condition of the solver
if results.solver.termination_condition == TerminationCondition.infeasible:
    print('The model is infeasible: No solution available')
elif results.solver.termination_condition == TerminationCondition.unbounded:
    print('The model has an unbounded solution')
elif results.solver.termination_condition == TerminationCondition.optimal:
    output = open('results.text', 'w')
    for (i,j) in instance.compatible_pairs_of_pilots:
        if value(instance.fly[i,j]) > 0:
            output.write(f'Pilot {i} and pilot {j} will fly together\n\n')
    output.write(f'The maximum total score is {value(instance.max_total_score)}')
    output.close()
